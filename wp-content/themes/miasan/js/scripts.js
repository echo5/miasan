jQuery(function($) {
	jQuery('.reviews_tab a').unbind();

	jQuery('body').on('click', '.reviews_tab a', function(b) {
		        b.preventDefault();
		        var c = $(this),
		            d = c.closest(".wc-tabs-wrapper, .woocommerce-tabs"),
		            e = d.find(".wc-tabs, ul.tabs");
		        e.find("li").removeClass("active"), d.find(".wc-tab, .panel:not(.panel .panel)").hide(), c.closest("li").addClass("active"), d.find(c.attr("href")).show()
		jQuery( '.gglcptch_v1, .gglcptch_v2' ).each( function() {
			var container = jQuery( this ).find( '.gglcptch_recaptcha' );
			if (
				container.is( ':empty' ) &&
				( gglcptch.vars.visibility || jQuery( this ).is( ':visible' ) )
			) {
				var containerId = container.attr( 'id' )
				gglcptch.display( containerId );
			}
		} );
	});
});