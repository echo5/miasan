<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

include_once 'includes/woocommerce-fxns.php';
include_once 'includes/mail-fxns.php';
include_once 'includes/attendees-fxns.php';

/**
 * Captcha form for spam
 */
function miasan_add_my_forms( $forms ) {
    $forms['form_slug']   = "Form Display Name";
    $forms['form_2_slug'] = "Form 2 Display Name";
    return $forms;
}
add_filter( 'cptch_add_form', 'miasan_add_my_forms' );

/**
 * Add theme language support
 */
add_action( 'after_setup_theme', 'miasan_add_language_support' );
function miasan_add_language_support(){
    load_theme_textdomain( 'miasan', get_stylesheet_directory() . '/lang' );
}

/**
 * Theme scripts and styles
 */
function miasan_scripts() {
    wp_enqueue_script("miasan_scripts", get_stylesheet_directory_uri() . "/js/scripts.js", true, "1.0", "all");
    // wp_dequeue_script( 'wc-single-product' );
}
add_action( 'wp_enqueue_scripts', 'miasan_scripts', 1000 );

/**
 * Admin styles
 */
function miasan_admin_styles() {
    if ( is_admin() ) {
        
        wp_enqueue_style("miasan_admin_styles", get_stylesheet_directory_uri() . "/admin-style.css", false, "1.0", "all");
        wp_enqueue_script("miasan_admin_scripts", get_stylesheet_directory_uri() . "/js/admin-scripts.js", false, "1.0", "all");

    }
}
add_action( 'admin_enqueue_scripts', 'miasan_admin_styles' );

/**
 * Remove "protected" from page title
 */
add_filter('protected_title_format', 'miasan_blank');
function miasan_blank($title) {
       return '%s';
}

/**
 * Allow order editing during server transition
 * @TODO remove when Miasan confirms
 */
function miasan_order_statuses_to_editable () { 
    return TRUE; 
}
add_filter ( 'wc_order_is_editable', 'miasan_order_statuses_to_editable' ); 


/**
 * Filter out open and new events
 */
add_filter('tribe_events_pre_get_posts', 'miasan_filter_tribe_all_occurences', 100);
function miasan_filter_tribe_all_occurences ($wp_query) {
 
        if ( !is_admin() )  {
 
                $new_meta = array();
                $today = new DateTime();
 
                // Join with existing meta_query
                if(is_array($wp_query->meta_query))
                        $new_meta = $wp_query->meta_query;
 
                // Add new meta_query, select events ending from now forward
                $new_meta[] = array(
                        'key' => '_EventEndDate',
                        'type' => 'DATETIME',
                        'compare' => '>=',
                        'value' => $today->format('Y-m-d H:i:s')
                );
 
                $wp_query->set( 'meta_query', $new_meta );
        }
 
        return $wp_query;
}

/**
 * Add soldout/instock labels
 */
function miasan_add_stock_to_events_list() {
    $post_id = get_the_ID();
    echo '<div class="tribe-events-stock">';
    if ( tribe_events_has_soldout( $post_id ) ) {
        echo "<span class='tickets_nostock'>" . __( 'Sold Out!', 'miasan' ) . "</span>";
    } else {
        echo "<span class='tickets_instock'>" . __( 'In stock!', 'miasan' ) . "</span>";
    }
    echo '</div>';
}
add_action('tribe_events_before_the_event_title', 'miasan_add_stock_to_events_list');
