<?php

/**
 * Change default from email
 */
function miasan_change_from_email() {
    return "inquiry@miasan.com";
}
add_filter( 'wp_mail_from', 'miasan_change_from_email' );

/**
 * Change default from name
 */
function miasan_from_name() {
    return "Miasan";
}
add_filter( 'wp_mail_from_name', 'miasan_from_name' );

/**
 * Send order notes after checkout
 */
function miasan_send_attendee_details($order_id){
    //send email  wp_mail( $to, $subject, $message, $headers, $attachments ); ex:
    $headers = array('Content-Type: text/html; charset=UTF-8');
    // Get attendees and event info
    $attendees = array();
    $order = new WC_Order( $order_id );
    $items = $order->get_items();
    $n = 0;
    foreach ( $items as $item ) {
        $has_event = get_post_meta($item['product_id'], '_tribe_wooticket_for_event');
        if (!empty($has_event)) {
            while($n < $item['qty']) {
                $attendees[$n]['order_id'] = $order_id;
                $n++;
            }
            $event_title = $item['name'];
            $product_id = $item['product_id'];
            $event_startdate = date_format(date_create(get_post_meta($product_id, '_EventStartDate', true)), 'm/d/y');
        }

    }

    // Create mail for each attendee
    if (!empty($attendees)) {

        $n = 1;
        foreach($attendees as $attendee) {

            $mail_content = "<style>.attendee-label {
                                font-weight: bold;
                            }
                            .attendee-info-container {
                                width: 20%;
                                float: left;
                                margin-bottom: 5px;
                                padding: 15px;
                                box-sizing: border-box;
                            }

                            .attendee-container {
                                width: 100%;
                                background: #fff;
                                display: inline-block;
                                border-bottom: 1px solid rgb(229, 229, 229);
                            }
                            .line-separater {
                                height: 1px;
                                width: 100%;
                                background: rgb(218, 218, 218);
                                display: block;
                                clear: both;
                            }</style>";
            ob_start();
                print_attendees(array($attendee), $n, false, false);
            $mail_content .= ob_get_clean();
            $mail_content = str_replace('<div class="attendee-info-container">', '<br/><br/><div class="attendee-info-container">', $mail_content);
            $mail_content = preg_replace('/<div class=\"attendee\-label\">(.*?)<\/div>/s', '<div class="attendee-label"><strong>$1</strong></div>', $mail_content);

            // Get attendee and event info
            $order = get_post_meta( $order_id );
            $subject = '報名完成 - '.$order[$n.' Attendee Chinese Name'][0] . ' ('.$event_title.', '.$event_startdate.')';
            $attendee_email = $order[$n.' Attendee Email'][0];

            // Send mail to attendee and miasan
            wp_mail($attendee_email,'報名完成 - '.$subject, $mail_content, $headers);
            wp_mail('inquiry@miasan.com','報名完成 - ',$mail_content, $headers);

            $n++;
        }            
    }
    
}
add_action('woocommerce_checkout_order_processed', 'miasan_send_attendee_details');


/**
 * Send order notes without change
 */
function miasan_send_order_notes_auto(){
    //send email  wp_mail( $to, $subject, $message, $headers, $attachments ); ex:
        global $current_user;
        global $order_id;
        get_currentuserinfo();
        $order_notes = '';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $order = new WC_order($order_id);

        if ( $notes = $order->get_customer_order_notes() ) :
            foreach ( $notes as $note ) :
                $order_notes .= wpautop( wptexturize( $note->comment_content ) );
            endforeach;
        endif;

        wp_mail($current_user->user_email,'您的訂單匯款資訊',$order_notes, $headers);
}

/**
 * Send order notes
 */
function miasan_send_order_notes(){
    check_ajax_referer('my_email_ajax_nonce');
    if (isset($_POST['action']) && $_POST['action'] =="mail_before_submit" && isset($_POST['order_id'])){

    //send email  wp_mail( $to, $subject, $message, $headers, $attachments ); ex:
        global $current_user;
        get_currentuserinfo();
        $order_notes = '';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $order = new WC_order($_POST['order_id']);

        if ( $notes = $order->get_customer_order_notes() ) :
            foreach ( $notes as $note ) :
                $order_notes .= wpautop( wptexturize( $note->comment_content ) );
            endforeach;
        endif;

        wp_mail($current_user->user_email,'您的訂單匯款資訊',$order_notes, $headers);
        global $order_id;
        echo '您的郵件已寄出';
        die();
    }
    echo 'error';
    die();
}
add_action('wp_ajax_mail_before_submit', 'miasan_send_order_notes');


/**
 * Send questionnaire after attendee check-in
 */
function miasan_send_questionnaire(){
    check_ajax_referer('my_email_ajax_nonce');
    if (isset($_POST['action']) && $_POST['action'] =="send_questionnaire" && isset($_POST['attendee_id'])){

        // find attendee email
        $attendee_id = $_POST['attendee_id'];
        $order_id = get_post_meta($attendee_id, '_tribe_wooticket_order');
        $attendees = array();
        // Match number to attendee ticket ID
        global $wpdb;
        $meta = $wpdb->get_results("SELECT * FROM `".$wpdb->postmeta."` WHERE meta_key='_tribe_wooticket_order' AND meta_value='".$order_id[0]."'");
        if (is_array($meta) && !empty($meta) && isset($meta[0])) {
            foreach($meta as $attendee) {
                $attendees[] = $attendee->post_id;
            }
        }   
        $attendee_number = array_search($attendee_id, $attendees);
        $attendee_number++;

        $email = get_post_meta($order_id[0], $attendee_number.' Attendee Email'); 

        //send email  wp_mail( $to, $subject, $message, $headers, $attachments ); ex:
        $headers = array('Content-Type: text/html; charset=UTF-8');

        $html = "感謝您參加米亞桑之系列活動，米亞桑致力於戶外活動的推廣，針對於本次活動十分需要您的寶貴建議與回饋唷！<br/><br/>";

        $html .= "1.本次活動之行前通知是否有不足的地方呢？<br/><br/>";

        $html .= "2.本次活動對於飲食方面有什麼建議呢？<br/><br/>";

        $html .= "3.本次活動對於住宿之環境與空間是否有什麼建議呢？<br/><br/>";

        $html .= "4.本次活動對於課程/活動之安排有什麼建議呢？<br/><br/>";

        $html .= "5.本次活動如果還有可以更好好的地方，邀請您不吝嗇提供給我們唷？<br/><br/>";

        $html .= "您的寶貴建議會是我們前進之動力，感謝您的回饋，米亞桑會虛心接受並且調整。在戶外/山林活動之領域，米亞桑會更加努力、繼續進步！";

        wp_mail($email[0],'您的訂單匯款資訊',$html, $headers);
        echo '您的郵件已寄出';
        die();
    }
    echo 'error';
    die();
}
add_action('wp_ajax_send_questionnaire', 'miasan_send_questionnaire');

/**
 * Send questionnaire on checkin click
 */
function miasan_admin_mailer_javascript()
{    
    ?>
    <script>
    jQuery( document ).ready(function() {
        jQuery('.check_in a').click(function() {
        // send email to client if checking in
        if(jQuery(this).hasClass('tickets_checkin')) {
            var data = {
                action: 'send_questionnaire',
                attendee_id: jQuery(this).data('attendee-id'),
                _ajax_nonce: '<?php echo wp_create_nonce( 'my_email_ajax_nonce' ); ?>',
            };
            jQuery.post("<?php bloginfo( 'wpurl' ); ?>/wp-admin/admin-ajax.php", data, function(response) {
                alert(response);
            });
        }
        return false;

        });
    });
    </script>
    <?php
}
add_action('admin_footer','miasan_admin_mailer_javascript');

/**
 * Send email to client on click
 */
function miasan_client_mailer_javascript()
{    
    ?>
    <script>
    jQuery( document ).ready(function() {
        jQuery('#send-notes').click(function() {

        var data = {
            action: 'mail_before_submit',
            order_id: jQuery(this).data('order-id'),
            _ajax_nonce: '<?php echo wp_create_nonce( 'my_email_ajax_nonce' ); ?>',
        };
        jQuery.post("<?php bloginfo( 'wpurl' ); ?>/wp-admin/admin-ajax.php", data, function(response) {
                alert(response);
        });
        return false;

        });
    });
    </script>
    <?php 
}
add_action('wp_head','miasan_client_mailer_javascript');
