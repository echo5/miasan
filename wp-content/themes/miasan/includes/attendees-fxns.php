<?php

require_once('xlsxwriter.class.php');

/**
 * Create headers for download
 * @param  str $filename
 * @return str
 */
function download_send_headers($filename) {
    // disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download  
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");


    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
}


/**
 * Display attendees table in Events admin
 * @return str
 */
function miasan_display_attendees_table() { ?>

    <div class="clear"></div>
    <br><br>
    <h3><?php echo (isset($event) ? $event->post_title : ''); ?> <?php _e('Attendee List', 'tribe-events-calendar') ?></h3>
    <br>
    <br>

    <div class="attendee-actions">
    <?php        
    $attendees = get_attendees($_GET['event_id']);
    $attendees = array_merge($attendees['completed'], $attendees['incomplete']);
    $orders_array = array();
    // // Completed and paid attendees
    // if(isset($attendees['completed'])) {
    //     echo '<h4>已完成訂單</h4>';
    //     print_attendees($attendees['completed']);
    //     create_csv_export_url($_GET['event_id'], 'completed');
    //     create_xlsx_export_url($_GET['event_id'], 'completed');
    // }
    // // On-hold attendees
    // if(isset($attendees['incomplete'])) {
    //     echo '<h4>未完成訂單</h4>';
    //     print_attendees($attendees['incomplete']);
    //     create_csv_export_url($_GET['event_id'], 'incomplete');
    //     create_xlsx_export_url($_GET['event_id'], 'incomplete');
    // }
    if (isset($attendees)) {
        print_attendees($attendees);
        create_csv_export_url($_GET['event_id'], 'incomplete');
        create_xlsx_export_url($_GET['event_id'], 'incomplete');
    }
    ?>
    </div>
    <?php

}
add_action('tribe_events_tickets_attendees_event_summary_table_after','miasan_display_attendees_table');


/**
 * Create URL for CSV export
 * @param  $event_id
 * @param  $status
 * @return str
 */
function create_csv_export_url($event_id, $status) {
    $exportURL = add_query_arg(array(
        'action' => 'export_attendees_csv',
        'nc' => time(),     // cache buster
        'event_id' => $event_id,
        'status' => $status
    ), admin_url('admin-ajax.php'));
    printf('<a href="%s">匯出CSV檔案</a>', $exportURL);
}

/**
 * Create URL for XLSX export
 * @param  $event_id
 * @param  $status
 * @return str
 */
function create_xlsx_export_url($event_id, $status) {
    $exportURL = add_query_arg(array(
        'action' => 'export_attendees_xlsx',
        'nc' => time(),     // cache buster
        'event_id' => $event_id,
        'status' => $status
    ), admin_url('admin-ajax.php'));
    printf('<a href="%s">匯出XLSX檔案</a>', $exportURL);
}


/**
 * Get attendees by event id
 * @param  $event_id
 * @return array $attendees
 */
function get_attendees($event_id) {

    global $wpdb;
    $products = $wpdb->get_results( "select post_id from $wpdb->postmeta where meta_value=".intval($event_id) );
    foreach ($products as $product) {
        $product_ids[] = $product->post_id;
    }
    $product_id_list = implode(', ', $product_ids);
    $orders = $wpdb->get_results( 'SELECT order_id, order_item_name FROM wp_woocommerce_order_items WHERE order_item_id in (SELECT order_item_id FROM wp_woocommerce_order_itemmeta WHERE meta_key="_product_id" and meta_value IN ('.$product_id_list.'))');
    $product_orders_array = array();
    foreach($orders as $order) {
        $tickets[$order->order_id] = $order->order_item_name;
        $product_orders_array[] = $order->order_id;
    }

    $attendees_query = new WP_Query( array(
        'posts_per_page' => - 1,
        'post_type'      => 'tribe_wooticket',
        'meta_key'       => '_tribe_wooticket_event',
        'meta_value'     => intval( $event_id ),
        'orderby'        => 'ID',
        'order'          => 'DESC'
    ) );
    // Completed orders array
    $completed_orders = array();
    // CSV output array
    $array = array();

    if ( ! $attendees_query->have_posts() ) echo 'No attendees Found';
    $attendees = array();

    foreach ( $attendees_query->posts as $attendee ) {
        $order_id   = get_post_meta( $attendee->ID, '_tribe_wooticket_order', true );
        $checkin    = get_post_meta( $attendee->ID, '_tribe_wooticket_checkedin', true );
        $security   = get_post_meta( $attendee->ID, '_tribe_wooticket_security_code', true );
        $product_id = get_post_meta( $attendee->ID, '_tribe_wooticket_product', true );
        $name       = get_post_meta( $order_id, '_billing_first_name', true ) . ' ' . get_post_meta( $order_id, '_billing_last_name', true );
        $email      = get_post_meta( $order_id, '_billing_email', true );

        if ( empty( $product_id ) ) continue;

        $product = get_post( $product_id );
        $product_title = ( ! empty( $product ) ) ? $product->post_title : get_post_meta( $attendee->ID, '_tribe_deleted_product_name', true ) . ' ' . __( '(deleted)', 'wootickets' );

        $attendees[] = array(
            'order_id'           => $order_id,
            'purchaser_name'     => $name,
            'purchaser_email'    => $email,
            'ticket'             => $product_title,
            'attendee_id'        => $attendee->ID,
            'security'           => $security,
            'product_id'         => $product_id,
            'check_in'           => $checkin,
            'provider'           => __CLASS__ );

        $completed_orders[] = $order_id;
    }

    $incomplete_orders = array_diff($product_orders_array, $completed_orders);
    $incomplete_attendees = array();
    foreach($incomplete_orders as $order_id) {
        $order = get_post_meta( $order_id );
        for($n = 1; isset($order[$n.' Attendee Chinese Name']) || isset($order[$n.' Attendee English Name']); $n++) { 
            $incomplete_attendees[] = array(
                'order_id'          => $order_id,
                'purchaser_name'     => '',
                'purchaser_email'    => '',
                'ticket'             => $tickets[$order_id],
                'attendee_id'        => '',
                'security'           => '',
                'product_id'         => '',
                'check_in'           => '',
                );
        }
    }
    return array('completed' => $attendees, 'incomplete' => $incomplete_attendees);
}


/**
 * [print_attendees description]
 * @param  [type]  $attendees       [description]
 * @param  [type]  $attendee_number [description]
 * @param  boolean $export_csv      [description]
 * @param  boolean $toggle          [description]
 * @return [type]                   [description]
 */
function print_attendees($attendee_orders, $attendee_number = null, $export_csv = true, $toggle = true) {
    $attendees = get_attendees_array($attendee_orders, $attendee_number);

    foreach($attendees as $attendee) { ?>

        <div class="attendee-container">
            <div class="vc_toggle vc_toggle_default">

                <div class="vc_toggle_title">
                    <div class="attendee-info-container">
                        <div class="attendee-label">中文全名</div>
                        <div class="attendee-info"><?php echo $attendee['中文全名']; ?></div>
                    </div>
                    <div class="attendee-info-container">
                        <div class="attendee-label">电子邮件</div>
                        <div class="attendee-info"><?php echo $attendee['电子邮件']; ?></div>
                    </div>
                    <div class="attendee-info-container"> 
                        <div class="attendee-label">联络电话</div>
                        <div class="attendee-info"><?php echo $attendee['联络电话']; ?></div>
                    </div>
                    <div class="attendee-info-container">
                        <div class="attendee-label">身分证字号</div>
                        <div class="attendee-info"><?php echo $attendee['身分证字号']; ?></div>
                    </div>     
                    <div class="attendee-info-container">
                        <div class="attendee-label">出生日期</div>
                        <div class="attendee-info"><?php echo $attendee['出生日期']; ?></div>
                    </div>     
                    <?php if(($attendee['生理病況'] !== '以上皆无' && !empty($attendee['生理病況'])) || ($attendee['其他生理病况'] !== '無' && $attendee['其他生理病况'] !== 'none' && !empty($attendee['其他生理病况']))) { ?>
                        <div class="medical-flag"></div>
                    <?php } ?>
                    <div class="clear"></div>
                </div>

                <?php if ($toggle == true) : ?>
                    <div class="vc_toggle_content" style="display: none;">
                <?php else: ?>
                    <div class="vc_toggle_content">
                <?php endif; ?>
                    <div class="line-separater"></div>


                    <div class="attendee-info-container">
                        <div class="attendee-label">英文全名</div>
                        <div class="attendee-info"><?php echo $attendee['英文全名']; ?></div>
                    </div>
                    <div class="attendee-info-container">
                        <div class="attendee-label">Order #</div>
                        <div class="attendee-info"><?php echo $attendee['Order ID']; ?></div>
                    </div>
                    <div class="attendee-info-container">
                        <div class="attendee-label">Attendee Ticket ID</div>
                        <div class="attendee-info"><?php echo $attendee['Attendee Ticket ID']; ?></div>
                    </div>
                    <div class="attendee-info-container">
                        <div class="attendee-label">Security Code</div>
                        <div class="attendee-info"><?php echo $attendee['Security Code']; ?></div>
                    </div>
                    <div class="attendee-info-container">
                        <div class="attendee-label">Check-In Status</div>
                        <div class="attendee-info"><?php echo $attendee['Check-In Status']; ?></div>
                    </div>
                    
                    <div class="clear"></div>

                    <div class="attendee-info-container">
                        <div class="attendee-label">郵遞區號</div>
                        <div class="attendee-info"><?php echo $attendee['郵遞區號']; ?></div>
                    </div>  
                    <div class="attendee-info-container">
                        <div class="attendee-label">城市</div>
                        <div class="attendee-info"><?php echo $attendee['城市']; ?></div>
                    </div>  
                    <div class="attendee-info-container">
                        <div class="attendee-label">行政區</div>
                        <div class="attendee-info"><?php echo $attendee['行政區']; ?></div>
                    </div>  
                    <div class="attendee-info-container">
                        <div class="attendee-label">路名 / 街道名</div>
                        <div class="attendee-info"><?php echo $attendee['路名 / 街道名']; ?></div>
                    </div>  
                    <div class="attendee-info-container">
                        <div class="attendee-label">巷</div>
                        <div class="attendee-info"><?php echo $attendee['巷']; ?></div>
                    </div>  

                    <div class="clear"></div>

                    <div class="attendee-info-container">
                        <div class="attendee-label">弄</div>
                        <div class="attendee-info"><?php echo $attendee['弄']; ?></div>
                    </div>  
                    <div class="attendee-info-container">
                        <div class="attendee-label">門牌號碼</div>
                        <div class="attendee-info"><?php echo $attendee['門牌號碼']; ?></div>
                    </div>  
                    <div class="attendee-info-container">
                        <div class="attendee-label">樓層</div>
                        <div class="attendee-info"><?php echo $attendee['樓層']; ?></div>
                    </div>  
                    <div class="attendee-info-container">
                        <div class="attendee-label">性别</div>
                        <div class="attendee-info"><?php echo $attendee['性别']; ?></div>
                    </div>     
                    <div class="attendee-info-container">
                        <div class="attendee-label">紧急联络人姓名</div>
                        <div class="attendee-info"><?php echo $attendee['紧急联络人姓名']; ?></div>
                    </div>    

                    <div class="clear"></div>

                    <div class="attendee-info-container">
                        <div class="attendee-label">紧急联络人电话</div>
                        <div class="attendee-info"><?php echo $attendee['紧急联络人姓名']; ?></div>
                    </div>     
                    <div class="attendee-info-container">
                        <div class="attendee-label">监护人姓名</div>
                        <div class="attendee-info"><?php echo $attendee['监护人姓名']; ?></div>
                    </div>     
                    <div class="attendee-info-container">
                        <div class="attendee-label">生理病況</div>
                        <div class="attendee-info"><?php echo $attendee['生理病況']; ?></div>
                    </div>     
                    <div class="attendee-info-container">
                        <div class="attendee-label">血型</div>
                        <div class="attendee-info"><?php echo $attendee['血型']; ?></div>
                    </div>     
                    <div class="attendee-info-container">
                        <div class="attendee-label">其他生理病况</div>
                        <div class="attendee-info"><?php echo $attendee['其他生理病况']; ?></div>
                    </div>     
                    <div class="clear"></div>
                    <div class="attendee-info-container">
                        <div class="attendee-label">用药记录或药物过敏</div>
                        <div class="attendee-info"><?php echo $attendee['用药记录或药物过敏']; ?></div>
                    </div>     
                    <div class="attendee-info-container">
                        <div class="attendee-label">手术或骨折记录</div>
                        <div class="attendee-info"><?php echo $attendee['手术或骨折记录']; ?></div>
                    </div>  
                    <div class="attendee-info-container">
                        <div class="attendee-label">用餐需求</div>
                        <div class="attendee-info"><?php echo $attendee['用餐需求']; ?></div>
                    </div>     
                    <div class="attendee-info-container">
                        <div class="attendee-label">免费借用设备</div>
                        <div class="attendee-info"><?php echo $attendee['免费借用设备']; ?></div>
                    </div>  
                    <div class="attendee-info-container">
                        <div class="attendee-label">Ticket Type</div>
                        <div class="attendee-info"><?php echo $attendee['ticket']; ?></div>
                    </div>          
                </div>
            </div>              
        </div>  

    <?php } 

}

/**
 * Get attendee array and add info
 * @param  array $attendees
 * @return array $attendee_info
 */
function get_attendees_array($attendees, $attendee_number = null) {

    $attendee_info = array();
    foreach($attendees as $attendee) {
        if(!$attendee_number) {
            if(isset($orders_array)) {
                if(in_array($attendee['order_id'], $orders_array)) {
                    $n++;
                }           
                else {
                    $n = 1;
                    $orders_array[] = $attendee['order_id'];
                    $order = get_post_meta( $attendee['order_id'] );
                }             
            }
            else {
                $n = 1;
                $orders_array[] = $attendee['order_id'];
                $order = get_post_meta( $attendee['order_id'] );
            }
        } else {
            $n = $attendee_number;
            $order = get_post_meta($attendee['order_id']);
        }

        if(isset($order[$n.' Attendee Chinese Name']) && isset($order[$n.' Attendee English Name'])) {
            if(empty($attendee['attendee_id'])) {
                $attendee['attendee_id'] = $attendee['order_id'] . '_' . $n;
            }
            $attendee_info[$attendee['attendee_id']]['Order ID'] = ($attendee['order_id'] ? $attendee['order_id'] : '');
            $attendee_info[$attendee['attendee_id']]['Attendee Ticket ID'] = ($attendee['attendee_id'] ? $attendee['attendee_id'] : '');
            $attendee_info[$attendee['attendee_id']]['Security Code'] = ($attendee['security'] ? $attendee['security'] : '');
            $attendee_info[$attendee['attendee_id']]['Check-In Status'] = ($attendee['check_in'] ? 'Checked In' : 'Not Checked In');
            $attendee_info[$attendee['attendee_id']]['ticket'] = ($attendee['ticket'] ? $attendee['ticket'] : '');

            $attendee_info[$attendee['attendee_id']]['中文全名'] = (isset($order[$n.' Attendee Chinese Name'][0]) ? $order[$n.' Attendee Chinese Name'][0] : '');
            $attendee_info[$attendee['attendee_id']]['英文全名'] = (isset($order[$n.' Attendee English Name'][0]) ? $order[$n.' Attendee English Name'][0] : '');
            $attendee_info[$attendee['attendee_id']]['电子邮件'] = (isset($order[$n.' Attendee Email'][0]) ? $order[$n.' Attendee Email'][0] : '');
            $attendee_info[$attendee['attendee_id']]['联络电话'] = (isset($order[$n.' Attendee Phone'][0]) ? $order[$n.' Attendee Phone'][0] : ''); 
            $attendee_info[$attendee['attendee_id']]['身分证字号'] = (isset($order[$n.' Attendee Id'][0]) ? $order[$n.' Attendee Id'][0] : ''); 
            $attendee_info[$attendee['attendee_id']]['出生日期'] = (isset($order[$n.' Attendee DOB'][0]) ? $order[$n.' Attendee DOB'][0] : '');
            
            $attendee_info[$attendee['attendee_id']]['郵遞區號'] = (isset($order[$n.' Attendee Post Code'][0]) ? $order[$n.' Attendee Post Code'][0] : '');
            $attendee_info[$attendee['attendee_id']]['城市'] = (isset($order[$n.' Attendee City'][0]) ? $order[$n.' Attendee City'][0] : '');
            $attendee_info[$attendee['attendee_id']]['行政區'] = (isset($order[$n.' Attendee District'][0]) ? $order[$n.' Attendee District'][0] : '');
            $attendee_info[$attendee['attendee_id']]['路名 / 街道名'] = (isset($order[$n.' Attendee Street'][0]) ? $order[$n.' Attendee Street'][0] : '');
            $attendee_info[$attendee['attendee_id']]['巷'] = (isset($order[$n.' Attendee Alley'][0]) ? $order[$n.' Attendee Alley'][0] : '');
            $attendee_info[$attendee['attendee_id']]['弄'] = (isset($order[$n.' Attendee Lane'][0]) ? $order[$n.' Attendee Lane'][0] : '');
            $attendee_info[$attendee['attendee_id']]['門牌號碼'] = (isset($order[$n.' Attendee Number'][0]) ? $order[$n.' Attendee Number'][0] : '');
            $attendee_info[$attendee['attendee_id']]['樓層'] = (isset($order[$n.' Attendee Floor'][0]) ? $order[$n.' Attendee Floor'][0] : '');


            $attendee_info[$attendee['attendee_id']]['性别'] = (isset($order[$n.' Attendee Gender'][0]) ? $order[$n.' Attendee Gender'][0] : '');
            $attendee_info[$attendee['attendee_id']]['紧急联络人姓名'] = (isset($order[$n.' Attendee Emergency Name'][0]) ? $order[$n.' Attendee Emergency Name'][0] : '');
            $attendee_info[$attendee['attendee_id']]['紧急联络人电话'] = (isset($order[$n.' Attendee Emergency Phone'][0]) ? $order[$n.' Attendee Emergency Phone'][0] : '');
            $attendee_info[$attendee['attendee_id']]['监护人姓名'] = (isset($order[$n.' Attendee Guardian'][0]) ? $order[$n.' Attendee Guardian'][0] : ''); 
            $attendee_info[$attendee['attendee_id']]['用餐需求'] = (isset($order[$n.' Attendee Meal Preference'][0]) ? $order[$n.' Attendee Meal Preference'][0] : ''); 

            $attendee_info[$attendee['attendee_id']]['血型'] = (isset($order[$n.' Attendee Blood Type'][0]) ? $order[$n.' Attendee Blood Type'][0] : '');
            $attendee_info[$attendee['attendee_id']]['其他生理病况'] = (isset($order[$n.' Attendee Medical Other'][0]) ? $order[$n.' Attendee Medical Other'][0] : '');
            $attendee_info[$attendee['attendee_id']]['用药记录或药物过敏'] = (isset($order[$n.' Attendee Drugs'][0]) ? $order[$n.' Attendee Drugs'][0] : '');
            $attendee_info[$attendee['attendee_id']]['手术或骨折记录'] = (isset($order[$n.' Attendee Surgery'][0]) ? $order[$n.' Attendee Surgery'][0] : '');

            $equipment = array();
            $equipment[] = (isset($order[$n.' Attendee Equipment Miasan Special Lightweight Backpack'][0]) ? '米亞桑特製輕量化背包（含鋁條 / 70L）' : '');
            $equipment[] = (isset($order[$n.' Attendee Equipment BD Series Headlights'][0]) ? 'BD系列頭燈' : '');
            $equipment[] = (isset($order[$n.' Attendee Equipment Alpenstock'][0]) ? '登山杖' : '');
            $equipment[] = (isset($order[$n.' Attendee Equipment Sleeping Bags'][0]) ? '睡袋（含睡套內套）' : '');
            $equipment[] = (isset($order[$n.' Attendee Equipment Foam Mattress'][0]) ? '泡綿睡墊' : '');
            $equipment[] = (isset($order[$n.' Attendee Equipment Personal Helmet'][0]) ? '個人安全帽' : '');
            $equipment[] = (isset($order[$n.' Attendee Equipment Climbing Suspenders'][0]) ? '吊帶' : '');
            $equipment[] = (isset($order[$n.' Attendee Equipment Safety Shackles'][0]) ? '保險鉤環' : '');
            $equipment[] = (isset($order[$n.' Attendee Equipment None'][0]) ? '無需借用，自行攜帶所有裝備' : '');
            $equipment_string = implode(' / ', array_filter($equipment));
            $attendee_info[$attendee['attendee_id']]['免费借用设备'] = $equipment_string;

            $medical = array();
            $medical[] = (isset($order[$n.' Attendee Heart Disease'][0]) ? '心脏疾病' : '');
            $medical[] = (isset($order[$n.' Attendee Hypertension'][0]) ? '高血压' : '');
            $medical[] = (isset($order[$n.' Attendee Diabetes'][0]) ? '糖尿病' : '');
            $medical[] = (isset($order[$n.' Attendee Vascular Disease'][0]) ? '心血管疾病' : '');
            $medical[] = (isset($order[$n.' Attendee Asthma'][0]) ? '气喘' : '');
            $medical[] = (isset($order[$n.' Attendee Strenuous Exercise'][0]) ? '无法做剧烈运动' : '');
            $medical[] = (isset($order[$n.' Attendee Physical Disease'][0]) ? '特殊体质或疾病纪录' : '');
            $medical[] = (isset($order[$n.' Attendee Drug Allergy'][0]) ? '药物过敏' : '');
            $medical[] = (isset($order[$n.' Attendee Medication'][0]) ? '服用药物' : '');
            $medical[] = (isset($order[$n.' Attendee Major Surgery'][0]) ? '曾有重大手术' : '');
            $medical[] = (isset($order[$n.' Attendee Fractures'][0]) ? '曾有肢体骨折' : '');
            $medical[] = (isset($order[$n.' Attendee Medical (None)'][0]) ? '以上皆无' : '');
            $medical_string = implode(' / ', array_filter($medical));
            $attendee_info[$attendee['attendee_id']]['生理病況'] = $medical_string;
        }
    }

    return $attendee_info;
}


/**
 * Action for downloading CSV
 */
function export_attendees_csv(){
    if(is_admin() && isset($_REQUEST['event_id'])){
        $event_id = $_REQUEST['event_id'];
        $status = $_REQUEST['status'];
        $attendees = get_attendees($event_id);
        $attendees = array_merge($attendees['completed'], $attendees['incomplete']);
        $attendees_array = get_attendees_array($attendees);
        download_send_headers("data_export_" . date("Y-m-d") . ".csv");
        echo array2csv($attendees_array);
        die();
    }
}
add_action('wp_ajax_export_attendees_csv', 'export_attendees_csv');

/**
 * Action for downloading CSV
 */
function export_attendees_xlsx(){
    if(is_admin() && isset($_REQUEST['event_id'])){
        $event_id = $_REQUEST['event_id'];
        $status = $_REQUEST['status'];
        $attendees = get_attendees($event_id);
        $attendees = array_merge($attendees['completed'], $attendees['incomplete']);
        $attendees_array = get_attendees_array($attendees);
        download_send_headers("data_export_" . date("Y-m-d") . ".xlsx");
        echo array2xlsx($attendees_array);
        die();
    }
}
add_action('wp_ajax_export_attendees_xlsx', 'export_attendees_xlsx');

/**
 * Convert array to XLSX
 * @param  array  &$array
 * @return xlsx
 */
function array2xlsx(array &$array)
{
   if (count($array) == 0) {
     return null;
   }
   array_unshift($array, array_keys(reset($array)));
   ob_start();
   $writer = new XLSXWriter();
   $writer->writeSheet($array);
   echo $writer->writeToString();
   return ob_get_clean();
}

/**
 * Convert array to CSV
 * @param  array  &$array
 * @return csv
 */
function array2csv(array &$array)
{
   if (count($array) == 0) {
     return null;
   }
   ob_start();
   $df = fopen("php://output", 'w');
   fprintf($df, chr(0xEF).chr(0xBB).chr(0xBF));
   // fwrite($df, 'sep=,' . "\r\n");
   fputcsv($df, array_keys(reset($array)));
   foreach ($array as $row) {
      fputcsv($df, preg_replace( "/\r|\n/", "", $row ));
   }
   fclose($df);
   return ob_get_clean();
}