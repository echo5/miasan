<?php 

/**
 * WPML filter for Woocommerce pages
 */
function miasan_wpml_woocommerce_get_cart_page_id($id) {
    return icl_object_id($id, 'page', true, ICL_LANGUAGE_CODE); // translate the page to current language
}
function miasan_wpml_woocommerce_get_checkout_page_id($id) {
    return icl_object_id($id, 'page', true, ICL_LANGUAGE_CODE); // translate the page to current language
}
add_filter('woocommerce_get_checkout_page_id', 'miasan_wpml_woocommerce_get_checkout_page_id');
add_filter('woocommerce_get_cart_page_id', 'miasan_wpml_woocommerce_get_cart_page_id');
remove_action( 'wp_footer', 'wp_func_jquery' );

/**
 * Woocommerce checkout redirect for language
 */
function miasan_woocommerce_redirect() {
    if (! is_user_logged_in() && (is_checkout())) {
        // Get page for "my account"    
        $page_id = '1208'; 
        if (defined('ICL_LANGUAGE_CODE')) { // this is to not break code in case WPML is turned off, etc.
            $_type  = 'page';
            $page = icl_object_id($page_id, $_type, true, ICL_LANGUAGE_CODE);
        }
        wp_redirect(get_permalink($page));
        exit;
    }
}
add_action('template_redirect', 'miasan_woocommerce_redirect');


/**
 * Reorder woocommerce checkout fields 
 */
function miasan_order_fields($fields) {

    $billing_fields = array(
        "billing_country", 
        "billing_last_name", 
        "billing_first_name", 
        "billing_company", 
        "billing_address_1", 
        "billing_address_2", 
        "billing_city", 
        "billing_state", 
        "billing_postcode", 
        "billing_email", 
        "billing_phone",
    );
    foreach($billing_fields as $field)
    {
        if($field == 'billing_last_name') {
            $fields["billing"][$field]['class'] = array('form-row-first');
            $fields["billing"][$field]['clear'] = false;
        }
        elseif($field == 'billing_first_name') {
            $fields["billing"][$field]['class'] = array('form-row-last');
        }
        
        $billing_ordered_fields[$field] = $fields["billing"][$field];
    }

    $fields["billing"] = $billing_ordered_fields;


    $shipping_fields = array(
        "shipping_country", 
        "shipping_last_name", 
        "shipping_first_name", 
        "shipping_company", 
        "shipping_address_1", 
        "shipping_address_2", 
        "shipping_city", 
        "shipping_state", 
        "shipping_postcode", 
    );
    foreach($shipping_fields as $field)
    {
        if($field == 'shipping_last_name') {
            $fields["shipping"][$field]['class'] = array('form-row-first');
            $fields["shipping"][$field]['clear'] = false;
        }
        elseif($field == 'shipping_first_name') {
            $fields["shipping"][$field]['class'] = array('form-row-last');
        }

        $shipping_ordered_fields[$field] = $fields["shipping"][$field];
    }

    $fields["shipping"] = $shipping_ordered_fields;

    return $fields;

}
add_filter("woocommerce_checkout_fields", "miasan_order_fields");


/**
 * Change login errors
 */
add_filter('login_errors', create_function('$a', "return '無效的使用者名稱或密碼';"));

/**
 * Remove woothemes notice
 */
remove_action('admin_notices', 'woothemes_updater_notice');

/**
 * Email admin with customer orders
 */
function miasan_email_headers_filter( $headers, $object ) {
    if ($object == 'customer_completed_order' || $object == 'customer_note') {
        $headers .= 'BCC: Miasan <miasanoutdoortw@gmail.com>' . "\r\n";
    }

    return $headers;
}
add_filter( 'woocommerce_email_headers', 'miasan_email_headers_filter', 10, 2);
