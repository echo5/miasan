<?php
/*
 *Plugin Name: SmilePayC2C(PAY)
 *Description: SmilePay 7-11 c2c Shipping
 * Version: 2.6.2
 * Author:  SmilePay
 * Author URI: http://www.smilepay.net
*/
 
/**
 * Check if WooCommerce is active
 */

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    add_action('plugins_loaded', 'Smilepayc2cp_shipping_method_init', 0);
 
    add_action('woocommerce_checkout_update_order_review','Smilepayc2cp_shipping_method_match');
    add_action('before_woocommerce_pay','Smilepayc2cp_shipping_method_match_orderpay');
    //配送方式過濾付款方式
    function Smilepayc2cp_shipping_method_match()
    {

        $packages = WC()->cart->get_shipping_packages();
         
       

        foreach ( $packages as $package_key => $package ) 
        {
            WC()->shipping()->load_shipping_methods( $package );
           
        }
        $shipping_methods =  WC()->shipping()->shipping_methods;
        $payment_gateways = WC()->payment_gateways();



        foreach($shipping_methods as $shipping_method)
        {
           if( $shipping_method->id == 'smilepay_711_c2c_shippingpay' && $shipping_method->get_instance_id() != 0)
           {
               $smilepay_c2cp_shipping_method =  $shipping_method;
            
               break;
           }
           
        }
        if( isset($smilepay_c2cp_shipping_method) &&  $smilepay_c2cp_shipping_method)
        {
            if($smilepay_c2cp_shipping_method->enabled!="yes")
                return;
            if($smilepay_c2cp_shipping_method->instance_settings['payment_filter_enabled']!="yes")
                return;
           
        }
        
        $selected_shipping_method = $_REQUEST['shipping_method'][0] ;
        
                
        foreach ( $payment_gateways as $payment_gateway ) {
             foreach ( $payment_gateway as $gateway ) {
                    if(strtolower($selected_shipping_method) == 'smilepay_711_c2c_shippingpay')
                    {
               
                        if($gateway->id != 'smilepayc2c')
                            $gateway->enabled = 'no';
                    }
                    else
                    {
                        if($gateway->id == 'smilepayc2c')
                            $gateway->enabled = 'no';
                    }

             }
        }
    
         
      
    }
    //配送方式過濾付款方式 重結帳
    function Smilepayc2cp_shipping_method_match_orderpay()
    {
        global $wp;

        if ( isset( $_GET['pay_for_order']) && $_GET['pay_for_order'] == 'true') 
        {
            $order_id = $wp->query_vars['order-pay'];
           
            if($order_id)
            {
                $order = wc_get_order( $order_id ); 
                if(!$order)
                    return;
                //$packages = WC()->cart->get_shipping_packages();
                
                //make fake package for find shipping region
                $packages = array();
                $packages[0]['destination']['country']   =  $order->shipping_country;
			    $packages[0]['destination']['state']     =  $order->shipping_state;
			    $packages[0]['destination']['postcode']  =  $order->shipping_postcode;
			    $packages[0]['destination']['city']      =  $order->shipping_city;
			    $packages[0]['destination']['address']   =  $order->shipping_address_1;
			    $packages[0]['destination']['address_2'] =  $order->shipping_address_2;

              
               
                //load shipping method
                foreach ( $packages as $package_key => $package ) 
                {
                     WC()->shipping()->load_shipping_methods( $package );
                }

              
                $shipping_methods =  WC()->shipping()->shipping_methods;
                $payment_gateways = WC()->payment_gateways();
                
                    
                foreach($shipping_methods as $shipping_method)
                {

                    if( $shipping_method->id == 'smilepay_711_c2c_shippingpay' && $shipping_method->get_instance_id() != 0)
                    {
                        $smilepay_c2cp_shipping_method =  $shipping_method;
                        break;
                    }
                }

                if( isset($smilepay_c2cp_shipping_method) &&  $smilepay_c2cp_shipping_method)
                {
                    if($smilepay_c2cp_shipping_method->enabled!="yes")
                        return;
                    if($smilepay_c2cp_shipping_method->instance_settings['payment_filter_enabled']!="yes")
                        return;
                }
                $selected_shipping_method =    $order->has_shipping_method('smilepay_711_c2c_shippingpay');

                
                foreach ( $payment_gateways as $payment_gateway ) {
                    foreach ( $payment_gateway as $gateway ) {
                        if($selected_shipping_method)
                        {
               
                            if($gateway->id != 'smilepayc2c')
                                $gateway->enabled = 'no';
                        }
                        else
                        {
                            if($gateway->id == 'smilepayc2c')
                                $gateway->enabled = 'no';
                        }   

                    }
                }


            }
            else
                return;



        }
    }


	function  Smilepayc2cp_shipping_method_init() {


		if ( ! class_exists( 'WC_Smilepayc2cp_Shipping_Method' ) ) {
			class WC_Smilepayc2cp_Shipping_Method extends WC_Shipping_Method {

				public function __construct($instance_id = 0) {
                    include_once("smilepayc2c.php");

                    $this->instance_id 			 = absint( $instance_id );
                    $smilepayc2cdata = new WC_SmilePayc2c();
                    
					$this->id                 = 'smilepay_711_c2c_shippingpay';
					$this->method_title       = __( 'SmilePay 7-11 Shipping(PAY)' ); 
                    $this->method_description    = u2bcp('7-11超商取貨付款');

                    //for woocommerce 2.6 modify
					//$this->enabled            = "yes";
                    $this->supports              = array(
			                                        'shipping-zones',
			                                        'instance-settings',
			                                        'instance-settings-modal',
		                                            );

                    if(isset($smilepayc2cdata->title) && !empty($smilepayc2cdata->title))
					    $this->title              = $smilepayc2cdata->title;//u2bcp("速買配7-11門市取貨付款"); 
                    else
                        $this->title              = u2bcp("SmilePay 7-11超商取貨付款 繳費"); 
					$this->init();
            
				}

				public function init_form_fields() {  //後台設置欄位

					//for woocommerce 2.6 modify
					//$this->form_fields = array(
                    $this->instance_form_fields = array(
					/*	'enabled' => array(
							'title' => __(u2bcp("啟用/關閉"), 'woocommerce'),
							'type' => 'checkbox',
							'default' => 'yes',
                   			'label' =>  $this->title,
						),*/
						'fee' => array(
							'title'       => __(u2bcp('費用'), 'woocommerce' ),
							'type'        => 'price',
							'description' => __( '', 'woocommerce' ),
							'default'     => '',
							'desc_tip'    => true,
							'placeholder' => wc_format_localized_price( 0 )
						),
						'fee_top' => array(
							'title'       => __(u2bcp('免運費門檻'), 'woocommerce' ),
							'type'        => 'price',
							'description' => __(u2bcp("當達到此金額時，免運費，不使用請保持空白"), 'woocommerce'),
							'desc_tip'    => true,
							'placeholder' => wc_format_localized_price( 0 )
						),			
                        'payment_filter_enabled' => array(
							'title' =>  u2bcp("SmilePay 配送方式搭配付款方式規則"),
                            'description' => __(u2bcp("開啟此功能後，在結帳時，當配送方式是取貨付款，付款方式只有取貨付款，若配送方式不是取貨付款，則付款方式不會出現取貨付款"), 'woocommerce'),
							'type' => 'checkbox',
							'default' => 'yes',
                   			'label' => u2bcp("啟用"),
						),			
						'hiddtext' => array(
							'title' => __(u2bcp('超商純取貨注意事項'), 'woocommerce'),
							'type' => 'hidden',
							'description' => __(u2bcp("使用超商純取貨功能，需注意以下事項：
														<br>※<font color='red'>此模組僅支援【SmilePay 超商取貨付款】金流模組，請確認啟用</font>							
														<br>1.請先至SmilePay商家後台開啟取貨功能*<a target='_blank' href='http://www.smilepay.net/RVG.ASP'>商家後台</a>*
														<br>2.帳單資訊中<font color='red'>聯絡電話</font>必須為<font color='red'>手機號碼</font>，商品到門市時會以簡訊通知。
														<br>3.消費者成立訂單後，可至SmilePay商家後台中取得<font color='red'>交貨便代碼</font>。
														<br>4.消費者成立訂單後，可至訂單備註中點選<font color='red'>開啟交貨便服務單</font>。														
														<br>5.請將<font color='red'>交貨便代碼</font>手動新增至商品備註，以利消費者查詢物流狀態。
														<br>6.更多說明請參閱，<font color='red'>SmilePay網站說明</font>與<font color='red'>WooCommerce模組說明文件</font>。
														"), 'woocommerce'),
							'default' => __('', 'woocommerce')
						)         
					);
				}
 
				function init() {
					$this->init_form_fields(); 
					//$this->init_settings();
					$this->fee          = $this->get_option( 'fee' );
					$this->fee_top= $this->get_option( 'fee_top' );	
                    	
                    $this->payment_filter_enabled= $this->get_option( 'payment_filter_enabled' );									
					add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
				}
 
				/**
				 * calculate_shipping function.
				 *
				 * @access public
				 * @param mixed $package
				 * @return void
				 */
				public function calculate_shipping( $package = array() ) {
					$shipping_total=0;
					$shipping_total=$this->fee;
					
					$fee_top=$this->fee_top;
					$total = WC()->cart->cart_contents_total;					
					if($total>=$fee_top AND $fee_top<>0 AND $fee_top<>NULL)
					{$shipping_total=0;}
					
					$rate = array(
						'id' => $this->id,
						'label' => $this->title,
						'cost' => $shipping_total
					);
 
					// Register the rate
					$this->add_rate( $rate );
				}
                // 是否選用C2CP運送
                // $order_id:訂單編號
                public function isSelectedSmilepayC2Cp($order_id)
                {
                 
                    if(!isset($order_id) || empty($order_id))
                    {
                        return false;
                    }
                    
                    $order = new WC_Order($order_id);
          
                    if(!isset($order) || empty($order))
                        return false;
                    if( $order->has_shipping_method($this->id ))
                        return true;
                    else
                        return false;
                 }
			}
		}

	}
 
	add_action( 'woocommerce_shipping_init', 'Smilepayc2cp_shipping_method_init' );
    
 
	function add_Smilepayc2cp_shipping_method( $methods ) {
        //fill this->id for woocommerce2.6 modify
		//$methods[] = 'WC_Smilepayc2cp_Shipping_Method';
        $methods['smilepay_711_c2c_shippingpay'] = 'WC_Smilepayc2cp_Shipping_Method';
		return $methods;
	}
 
	add_filter( 'woocommerce_shipping_methods', 'add_Smilepayc2cp_shipping_method' );
}
function u2bcp($text)//畫面輸出
{	return iconv("big5","UTF-8",$text);}
function b2ucp($text)//寫入資料庫
{	return iconv("UTF-8","big5",$text);}
