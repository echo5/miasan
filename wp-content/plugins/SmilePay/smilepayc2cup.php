<?php
/*
 *Plugin Name: SmilePayC2C(UN-PAY)
 *Description: SmilePay 7-11 c2c Shipping
 * Version: 2.6.2
 * Author:  SmilePay
 * Author URI: http://www.smilepay.net
*/
 
/**
 * Check if WooCommerce is active
 */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    add_action('plugins_loaded', 'smilepayc2cu_shipping_method_init', 0);
	function  smilepayc2cu_shipping_method_init() {
		if ( ! class_exists( 'WC_Smilepayc2cu_Shipping_Method' ) ) {
            
			class WC_Smilepayc2cu_Shipping_Method extends WC_Shipping_Method {
           
				public function __construct($instance_id = 0) {
					$this->id                 = 'smilepay_711_c2c_shippingunpay';
					$this->method_title       = __( 'SmilePay 7-11 Shipping(UN-PAY)' ); 

                    //for woocommerce 2.6 modify
					//$this->enabled            = "yes";
                    $this->instance_id 			 = absint( $instance_id );
                    $this->supports              = array(
			                                        'shipping-zones',
			                                        'instance-settings',
			                                        'instance-settings-modal',
		                                            );
                    $this->method_description    = u2bcu('7-11超商純取貨');


					
                 
					$this->init();
                    if(isset( $this->instance_settings['title']))
                        $this->title = $this->instance_settings['title'];
                    else
                         $this->title = u2bcu('SmilePay 7-11超商純取貨');

				}

				public function init_form_fields() {  //後台設置欄位
					
					//for woocommerce 2.6 modify
					//$this->form_fields = array(
                    $this->instance_form_fields = array(
						/*'enabled' => array(
							'title' => __(u2bcu("啟用/關閉"), 'woocommerce'),
							'type' => 'checkbox',
							'default' => 'yes',
              'label' => __(u2bcu("SmilePay 速買配超商純取貨"), 'woocommerce'),
						),*/
                     'title' => array(
                                'title' => __(u2bcu('標題'), 'woocommerce'),
                                'type' => 'text',
                                'description' => __(u2bcu('顧客在結帳時所顯示的配送方式標題'), 'woocommerce'),
                                'default' => __(u2bcu('SmilePay 7-11超商純取貨'), 'woocommerce')
                            ),
						'fee' => array(
							'title'       => __(u2bcu('費用'), 'woocommerce' ),
							'type'        => 'price',
							'description' => __( '', 'woocommerce' ),
							'default'     => '',
							'desc_tip'    => true,
							'placeholder' => wc_format_localized_price( 0 )
						),
						'fee_top' => array(
							'title'       => __(u2bcu('免運費門檻'), 'woocommerce' ),
							'type'        => 'price',
							'description' => __(u2bcu("當達到此金額時，免運費，不使用請保持空白"), 'woocommerce'),
							'desc_tip'    => true,
							'placeholder' => wc_format_localized_price( 0 )
						),		
                        	//mobile map start
                      /*  'mobile_map' => array(
							'title'       => __(u2bcu('啟用手機板取貨門市'), 'woocommerce' ),
							'type'        => 'checkbox',
							'description' => __(u2bcu("當判斷消費者是使用手機時，選擇手機版本門市地圖"), 'woocommerce'),
							'desc_tip'    => true,
						),	*/
                        //mobile map end
						'hiddtext' => array(
							'title' => __(u2bcu('超商純取貨注意事項'), 'woocommerce'),
							'type' => 'hidden',
							'description' => __(u2bcu("使用超商純取貨功能，需注意以下事項：
														<br>※<font color='red'>此模組僅支援SmilePay速買配金流收款使用</font>							
														<br>1.請先至SmilePay商家後台開啟取貨功能*<a target='_blank' href='http://www.smilepay.net/RVG.ASP'>商家後台</a>*
														<br>2.帳單資訊中<font color='red'>聯絡電話</font>必須為<font color='red'>手機號碼</font>，商品到門市時會以簡訊通知。
														<br>3.消費者成立訂單後，可至SmilePay商家後台中取得<font color='red'>交貨便代碼</font>。
														<br>4.消費者成立訂單後，可至訂單備註中點選<font color='red'>開啟繳費單</font>。														
														<br>5.請將<font color='red'>交貨便代碼</font>手動新增至商品備註，以利消費者查詢物流狀態。
														<br>6.更多說明請參閱，<font color='red'>SmilePay網站說明</font>與<font color='red'>WooCommerce模組說明文件</font>。
														"), 'woocommerce'),
							'default' => __('', 'woocommerce')
						)         
					);
				}
 
				function init() {
					$this->init_form_fields(); 
					$this->init_settings();
					$this->fee= $this->get_option( 'fee' );
					$this->fee_top= $this->get_option( 'fee_top' );					
					add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
					
				}
 
				/**
				 * calculate_shipping function.
				 *
				 * @access public
				 * @param mixed $package
				 * @return void
				 */
				public function calculate_shipping( $package = array() ) {
					$shipping_total=0;
					$shipping_total=$this->fee;
					
					$fee_top=$this->fee_top;
					$total = WC()->cart->cart_contents_total;					
					if($total>=$fee_top AND $fee_top<>0 AND $fee_top<>NULL)
					{$shipping_total=0;}
					
					$rate = array(
						'id' => $this->id,
						'label' => $this->title,
						'cost' => $shipping_total
					);
 
					// Register the rate
					$this->add_rate( $rate );
				}
                // 是否選用C2CUP運送
                // $order_id:訂單編號
                public function isSelectedSmilepayC2Cup($order_id)
                {
                 
                    if(!isset($order_id) || empty($order_id))
                    {
                        return false;
                    }
            
                    $order = new WC_Order($order_id);
          
                    if(!isset($order) || empty($order))
                        return false;
                    if( $order->has_shipping_method($this->id ))
                        return true;
                    else
                        return false;
                 }
 
			}
		}

	}

  
	add_action( 'woocommerce_shipping_init', 'smilepayc2cu_shipping_method_init' );
 
	function add_smilepayc2cu_shipping_method( $methods ) {

        //for woocommerce 2.6 modify
		$methods['smilepay_711_c2c_shippingunpay'] = 'WC_Smilepayc2cu_Shipping_Method';
		return $methods;
	}

 
	add_filter( 'woocommerce_shipping_methods', 'add_smilepayc2cu_shipping_method' );
}
function u2bcu($text)//畫面輸出
{	return iconv("big5","UTF-8",$text);}
function b2ucu($text)//寫入資料庫
{	return iconv("UTF-8","big5",$text);}
