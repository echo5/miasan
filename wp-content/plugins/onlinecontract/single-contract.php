﻿<?php
/*
Template Name: Contract

*/
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
?>
<?php $file = __FILE__; ?>
<?php if(!isset($_GET['pdf'])) { ?>
	<style>
		<?php if(get_option('onlinecontract_styletemplate')) {
		echo get_option('onlinecontract_styletemplate');
		}else{
		?>
		body { font-family: 'Open Sans',arial,sans-serif; margin:20px 0; }
		h1,h2 { font-family: 'Roboto Slab', serif; text-transform:uppercase; }
		.signedby { font-family: 'Dawning of a New Day', cursive; font-size:45px;  font-weight:strong;}
		.entry-content img {
			margin: 0 0 1.5em 0;
			}
		.alignleft, img.alignleft {
			margin-right: 1.5em;
			display: inline;
			float: left;
			}
		.alignright, img.alignright {
			margin-left: 1.5em;
			display: inline;
			float: right;
			}
		.aligncenter, img.aligncenter {
			margin-right: auto;
			margin-left: auto;
			display: block;
			clear: both;
			}
		.alignnone, img.alignnone {
			/* not sure about this one */
			}
		.wp-caption {
			margin-bottom: 1.5em;
			text-align: center;
			padding-top: 5px;
			}
		.wp-caption img {
			border: 0 none;
			padding: 0;
			margin: 0;
			}
		.wp-caption p.wp-caption-text {
			line-height: 1.5;
			font-size: 10px;
			margin: 0;
			}
		.wp-smiley {
			margin: 0 !important;
			max-height: 1em;
			}
		blockquote.left {
			margin-right: 20px;
			text-align: right;
			margin-left: 0;
			width: 33%;
			float: left;
			}
		blockquote.right {
			margin-left: 20px;
			text-align: left;
			margin-right: 0;
			width: 33%;
			float: right;
			}
		.button-primary, .button-secondary {
			display: inline-block;
			text-decoration: none;
			font-size: 18px;
			line-height: 38px;
			height: 38px;
			margin: 0;
			padding: 0 10px 1px;
			cursor: pointer;
			border-width: 1px;
			border-style: solid;
			-webkit-appearance: none;
			-webkit-border-radius: 3px;
			border-radius: 3px;
			white-space: nowrap;
			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
			margin-top:20px;
		}
		.button-primary {
			background: #2ea2cc;
			border-color: #0074a2;
			-webkit-box-shadow: inset 0 1px 0 rgba(120,200,230,.5),0 1px 0 rgba(0,0,0,.15);
			box-shadow: inset 0 1px 0 rgba(120,200,230,.5),0 1px 0 rgba(0,0,0,.15);
			color: #fff;
			text-decoration: none;
		}
		.button-secondary {
			color: #555;
			border-color: #ccc;
			background: #f7f7f7;
			-webkit-box-shadow: inset 0 1px 0 #fff,0 1px 0 rgba(0,0,0,.08);
			box-shadow: inset 0 1px 0 #fff,0 1px 0 rgba(0,0,0,.08);
			vertical-align: top;
		}
		@media screen and (max-width:600px){
			body { font-size:12px; }
			.signedby { font-size:25px; }
			.button-primary, .button-secondary {
				font-size:12px;
			}
			div { line-height:normal !important; }
		}
		@media screen and (max-width:384px){
			body { font-size:12px; }
			.button-primary, .button-secondary {
				display: block;
				text-align: center;
				margin-top: 10px;
				width: 100%;
				font-size: 15px;
			}
			form { display: block !important; }
			div { width:98% !important; text-align:center !important; margin:0 auto; line-height:normal !important; }
			.signedby { font-size:45px; }
			p, ul, li, h1,h2,h3,h4,h5,h6 { text-align:left; }
			h1 { font-size:20px; }
			h2 { font-size:18px; }
		}
		<?php } ?>
		</style>
	<?php global $post; if (!post_password_required($post) ) { ?>
		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<meta name="viewport" content="width=device-width">
		<title>Online Contract | <?php echo get_option('onlinecontract_businessname'); ?></title>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800">
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto+Slab">
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Dawning+of+a+New+Day">
		<div style="width:95%; max-width:1280px; margin:0 auto;">
			<div id="content">
				<?php
				$content = apply_filters( 'the_content', get_the_content() );
				$content = str_replace( ']]>', ']]&gt;', $content );
				echo stripslashes($content);
				$contentextract = explode('" ', get_the_content());
				if(strpos($contentextract[1],'mailingaddress') !== false) {
					$projectname = str_replace('projectname="','',$contentextract[2]);
					$paypalamount = str_replace('paypalamount="','',$contentextract[10]);
					$sendnotification = str_replace('sendnotification="','',$contentextract[11]);
				}else{
					$projectname = str_replace('projectname="','',$contentextract[1]);
					$paypalamount = str_replace('paypalamount="','',$contentextract[9]);
					$sendnotification = str_replace('sendnotification="','',$contentextract[10]);
				}
				$postid = get_the_ID();
				$signed = 0;
				$upload_dir = str_replace('plugins/','uploads/',dirname(__FILE__)).'/';
				 if ($handle = opendir($upload_dir.'signatures/')) {
						while (false !== ($entry = readdir($handle))) {
							if ($entry != "." && $entry != "..") {
								$imgnamearr = explode('_',$entry);
								if ($imgnamearr[0]==$postid) {
									$signed =1;
									$fullimgname = $entry;
									$signeddate = str_replace('.png','',$imgnamearr[1]);
									$ipaddress = str_replace('-','.',str_replace('.png','',$imgnamearr[2]));
								}
							}
						}
						closedir($handle);
				   }
				?>
				<?php if ($signed!=1) { ?>
                <br />
                <strong>請使用您的滑鼠在下方簽名欄位中簽名（按住左鍵拖曳），如您所使用的是平板電腦，也可以使用觸控筆或手指簽名。此電子簽名，表示您已同意以上所列之條約和規定。
在您簽完名後，請點選「完成簽名」按鈕儲存您的電子簽名。完成簽名程序後，您可點選列印同意書以列印，並回到上一個頁面繼續下一步。</strong><br/><br />
				<strong>Please sign using a stylus, your mouse, or your finger below to authorize this contract. By electronically signing this document, you agree to the terms established above.<br>After the document is signed, you can proceed to print it or save it as a PDF.</strong>
				<div id="signature" style="border:1px solid #f4f4f4; margin:10px 0; cursor: url(<?php echo WP_CONTENT_URL; ?>/plugins/onlinecontract/includes/img/contract-icon-large.png), auto;"></div>
				<div style=" float:right; width:30%; text-align:right;"><?php echo date('m-d-Y'); ?></div>
				<div style=" float:left; width:70%;"><button type="button" onclick="$('#signature').jSignature('clear')" class="button-secondary">清除簽名</button> <button type="button" onclick="importImg($('#signature'))" class="button-primary">完成簽名</button></div>
				<?php
					if ($sendnotification == 'true') {
						$message = 'Your contract has been viewed by the client.  Please click the following link to view the contract that was viewed:';
						$message .= "\r\n";
						$message .= get_permalink();
						$headers = 'From: Online Contract <'.get_option('admin_email').'>' . "\r\n";
						$headers .= "MIME-Version: 1.0\r\n";
						$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
						mail(get_option('admin_email'), 'A Contract Has Been Viewed', $message, $headers);
					}
				?>
				<div style="clear:both;"></div>
				<div style="height:75px;"></div>
				<?php }else{ ?>
				<img src="<?php echo WP_CONTENT_URL; ?>/uploads/onlinecontract/signatures/<?php echo $fullimgname; ?>" style="width:100%;">
				<div style=" width:100%; text-align:center;">Signed on <?php echo $signeddate; ?>
				<?php if($ipaddress) { ?>
					<br><strong>IP Address:</strong> <?php echo $ipaddress; ?>
				<?php } ?>
				</div>
			</div>
				<a id="bottom"></a>
				<?php include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); ?>
				<div style="width:100%; text-align:center;"><br><br><button id="printBtn" type="button" class="button-secondary">列印同意書</button>
				<?php if (get_option('onlinecontract_contractpaypalcode') != '' && $paypalamount!='') { ?>
					<?php
						if (get_option('onlinecontract_contractpaypalfee') !='') {
							if (strpos(get_option('onlinecontract_contractpaypalfee'), '%') !== false) {
								$percentage = str_replace('%','',get_option('onlinecontract_contractpaypalfee'));
								$percentage = $paypalamount * ($percentage/100);
								$paypal_fee = $percentage;
								$paypal_payment = $paypalamount + $paypal_fee;
							}else{
								$paypal_fee= get_option('onlinecontract_contractpaypalfee');
								$paypal_payment = $paypalamount + $paypal_fee;
							}
						}else{
							$paypal_payment = $paypalamount;
						}
					?>
					<button type="button" class="button-primary" onclick="window.open('https://www.paypal.com/cgi-bin/webscr?business=<?php echo get_option('onlinecontract_contractpaypalcode'); ?>&cmd=_xclick&currency_code=<?php echo get_option('onlinecontract_contractcurrencytype'); ?>&amount=<?php echo round($paypal_payment,2); ?>&item_name=<?php echo $projectname; ?>','_blank')">Pay with Paypal</button>
				<?php } ?>
				<?php if (is_plugin_active( 'wp-online-contract-skrill-payments/online-contract-skrill-payments.php' ) && get_option('onlinecontractskrill_email')!=''  && $paypalamount!='') { ?>
					<?php
					if (get_option('onlinecontractskrill_swipefee') !='') {
						if (strpos(get_option('onlinecontractskrill_swipefee'), '%') !== false) {
							$percentage = str_replace('%','',get_option('onlinecontractskrill_swipefee'));
							$percentage = $paypalamount * ($percentage/100);
							$skrill_fee = $percentage;
							$skrill_payment = $paypalamount + $skrill_fee;
						}else{
							$skrill_fee = get_option('onlinecontractskrill_swipefee');
							$skrill_payment = $paypalamount + $skrill_fee;
						}
					}else{
						$skrill_payment = $paypalamount;
					}
					?>
					<form action="https://www.moneybookers.com/app/payment.pl" method="post" target="_blank" style="display: inline-block;">
						<input type="hidden" name="pay_to_email" value="<?php echo get_option('onlinecontractskrill_email'); ?>">
						<input type="hidden" name="status_url" value="<?php echo get_option('onlinecontractskrill_email'); ?>">
						<input type="hidden" name="amount" value="<?php echo round($skrill_payment,2); ?>">
						<input type="hidden" name="currency" value="<?php echo get_option('onlinecontract_contractcurrencytype'); ?>">
						<input type="hidden" name="detail1_description" value="<?php echo $projectname; ?>">
						<input type="hidden" name="detail1_text" value="">
						<input type="submit" value="Pay with Skrill" class="button-primary">
					</form>
				<?php } ?>
				<?php if (is_plugin_active( 'wp-online-contract-authorize-payments/online-contract-authorize-payments.php' ) && get_option('onlinecontractauthorizenet_api')!='' && get_option('onlinecontractauthorizenet_transaction')!='' && $paypalamount!='') { ?>
					<?php
					if (get_option('onlinecontractauthorizenet_swipefee') !='') {
						if (strpos(get_option('onlinecontractauthorizenet_swipefee'), '%') !== false) {
							$percentage = str_replace('%','',get_option('onlinecontractauthorizenet_swipefee'));
							$percentage = $paypalamount * ($percentage/100);
							$authorizenet_fee = $percentage;
							$authorizenet_payment = $paypalamount + $authorizenet_fee;
						}else{
							$authorizenet_fee = get_option('onlinecontractauthorizenet_swipefee');
							$authorizenet_payment = $paypalamount + $authorizenet_fee;
						}
					}else{
						$authorizenet_payment = $paypalamount;
					}
					// https://secure.authorize.net/gateway/transact.dll
					?>
					<?php

					$loginID		= get_option('onlinecontractauthorizenet_api');
					$transactionKey = get_option('onlinecontractauthorizenet_transaction');
					$amount 		= $authorizenet_payment;
					$description 	= $projectname;
					$label 			= "Pay with Authorize.net";
					$testMode		= "false";
					if(get_option('onlinecontractauthorizenet_testmode')=='yes') {
						$url			= "https://test.authorize.net/gateway/transact.dll";
					}else{
						$url			= "https://secure.authorize.net/gateway/transact.dll";
					}
					if (array_key_exists("amount",$_REQUEST))
						{ $amount = $_REQUEST["amount"]; }
					if (array_key_exists("description",$_REQUEST))
						{ $description = $_REQUEST["description"]; }

					$invoice	= date('YmdHis');
					$sequence	= rand(1, 1000);
					$timeStamp	= time();
					if( phpversion() >= '5.1.2' )
						{ $fingerprint = hash_hmac("md5", $loginID . "^" . $sequence . "^" . $timeStamp . "^" . $amount . "^", $transactionKey); }
					else
						{ $fingerprint = bin2hex(mhash(MHASH_MD5, $loginID . "^" . $sequence . "^" . $timeStamp . "^" . $amount . "^", $transactionKey)); }
					?>
					<form method="post" action="<?php echo $url; ?>" style="display: inline-block;">
						<input type="hidden" name="x_login" value="<?php echo $loginID; ?>" />
						<input type="hidden" name="x_amount" value="<?php echo $amount; ?>" />
						<input type="hidden" name="x_description" value="<?php echo $description; ?>" />
						<input type="hidden" name="x_invoice_num" value="<?php echo $invoice; ?>" />
						<input type="hidden" name="x_fp_sequence" value="<?php echo $sequence; ?>" />
						<input type="hidden" name="x_fp_timestamp" value="<?php echo $timeStamp; ?>" />
						<input type="hidden" name="x_fp_hash" value="<?php echo $fingerprint; ?>" />
						<input type="hidden" name="x_test_request" value="<?php echo $testMode; ?>" />
						<input type="hidden" name="x_show_form" value="PAYMENT_FORM" />
						<input type="submit" value="<?php echo $label; ?>" class="button-primary"/>
					</form>
				<?php } ?>
				<?php if (is_plugin_active( 'wp-online-contract-dwolla-payments/online-contract-dwolla-payments.php' ) && get_option('onlinecontractdwolla_key')!='' && $paypalamount!='') { ?>
				<?php
				if (get_option('onlinecontractdwolla_swipefee') !='') {
					if (strpos(get_option('onlinecontractdwolla_swipefee'), '%') !== false) {
						$percentage = str_replace('%','',get_option('onlinecontractdwolla_swipefee'));
						$percentage = $paypalamount * ($percentage/100);
						$dwolla_fee = $percentage;
						$dwolla_payment = $paypalamount + $dwolla_fee;
					}else{
						$dwolla_fee = get_option('onlinecontractdwolla_swipefee');
						$dwolla_payment = $paypalamount + $dwolla_fee;
					}
				}else{
					$dwolla_payment = $paypalamount;
				}
				?>
					<script src="<?php echo plugins_url().'/wp-online-contract-dwolla-payments/includes/js/'; ?>button.min.js" class="dwolla_button" type="text/javascript" data-key="<?php echo get_option('onlinecontractdwolla_key'); ?>" data-redirect="<?php echo get_permalink(); ?>" data-label="Pay with Dwolla" data-name="<?php echo $projectname; ?>, <?php echo get_option('onlinecontract_businessname'); ?>" data-description="" data-amount="<?php echo round($dwolla_payment,2); ?>" <?php if (get_option('onlinecontractdwolla_guestcheckout')=='on') { ?> data-guest-checkout="true" <?php }else{ ?> data-guest-checkout="false" <?php } ?> data-type="simple">
					</script>
				<?php } ?>
				<?php if (is_plugin_active( 'wp-online-contract-stripe-payments/online-contract-stripe-payments.php' ) && get_option('onlinecontractstripe_publicapi')!='' && get_option('onlinecontractstripe_privateapi')!='' && $paypalamount!='') { ?>
					<?php
					if (get_option('onlinecontractstripe_swipefee') !='') {
						if (strpos(get_option('onlinecontractstripe_swipefee'), '%') !== false) {
							$percentage = str_replace('%','',get_option('onlinecontractstripe_swipefee'));
							$percentage = $paypalamount * ($percentage/100);
							$stripe_fee = $percentage;
						}else{
							$stripe_fee = get_option('onlinecontractstripe_swipefee');
						}
					}
					?>
					<form action="#bottom" method="post" style="display: inline-block;">
						<input type="submit" id="onlinecontractstripepayment" value="Pay with Stripe" data-key="<?php echo get_option('onlinecontractstripe_publicapi'); ?>" data-amount="<?php echo ($paypalamount + $stripe_fee)*100; ?>" data-currency="<?php echo get_option('onlinecontract_contractcurrencytype');?>" data-name="<?php echo get_option('onlinecontract_businessname'); ?>" data-description="<?php echo $projectname; ?>" class="button-primary">
						<script src="https://checkout.stripe.com/v2/checkout.js"></script>
						<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
						<script>
						var $ = jQuery;
						$(document).ready(function() {
							$('#onlinecontractstripepayment').on('click', function(event) {
								event.preventDefault();
								var button = $(this),
									form = button.parents('form');
								var opts = $.extend({}, button.data(), {
									token: function(result) {
										form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
									}
								});
								StripeCheckout.open(opts);
							});
						});
						</script>
					</form>
					<?php
						if($_POST['stripeToken']) {

							require_once(WP_PLUGIN_DIR.'/wp-online-contract-stripe-payments/includes/lib/Stripe.php');

							Stripe::setApiKey(get_option('onlinecontractstripe_privateapi'));

							$token = $_POST['stripeToken'];

							try {

								$charge = Stripe_Charge::create(array(

								  "amount" => ($paypalamount + $stripe_fee)*100,

								  "currency" => get_option('onlinecontract_contractcurrencytype'),

								  "card" => $token,

								  "description" => $projectname)

								);

							} catch (Stripe_ApiConnectionError $e) {

								// Network problem, perhaps try again.

								 $e_json = $e->getJsonBody();

								 $error = $e_json['error'];

							} catch (Stripe_InvalidRequestError $e) {

								// You screwed up in your programming. Shouldn't happen!

								 $e_json = $e->getJsonBody();

								 $error = $e_json['error'];

							} catch (Stripe_ApiError $e) {

								// Stripe's servers are down!

								 $e_json = $e->getJsonBody();

								 $error = $e_json['error'];

							} catch (Stripe_CardError $e) {

								// Card was declined.

								 $e_json = $e->getJsonBody();

								 $error = $e_json['error'];

							}

						}

					?>
					<?php } ?>
				</div>
				<br>
				<div style="font-size:10px; text-align:center;">
					<?php
					if (get_option('onlinecontract_contractpaypalfee') !='') {
						echo 'There is a '.get_option('onlinecontract_contractcurrency').get_option('onlinecontract_contractpaypalfee').' handling fee to use PayPal to pay your contract online.<br>';
					}
					?>
					<?php if (is_plugin_active( 'wp-online-contract-skrill-payments/online-contract-skrill-payments.php' ) && get_option('onlinecontractskrill_email')!='' && $paypalamount!='') { ?>
						<?php
						if (get_option('onlinecontractskrill_swipefee') !='') {
							echo 'There is a '.get_option('onlinecontract_contractcurrency').get_option('onlinecontractskrill_swipefee').' convenience fee to use Skrill to pay your contract online.<br>';
						}
						?>
					<?php } ?>
					<?php if (is_plugin_active( 'wp-online-contract-stripe-payments/online-contract-stripe-payments.php' ) && get_option('onlinecontractstripe_publicapi')!='' && get_option('onlinecontractstripe_privateapi')!='' && $paypalamount!='') { ?>
						<?php
						if (get_option('onlinecontractstripe_swipefee') !='') {
							echo 'There is a '.get_option('onlinecontract_contractcurrency').get_option('onlinecontractstripe_swipefee').' convenience fee to use Stripe to pay your contract online.<br>';
						}
						?>
					<?php } ?>
					<?php if (is_plugin_active( 'wp-online-contract-authorize-payments/online-contract-authorize-payments.php' ) && get_option('onlinecontractauthorizenet_api')!='' && get_option('onlinecontractauthorizenet_transaction')!='' && $paypalamount!='') { ?>
						<?php
						if (get_option('onlinecontractauthorizenet_swipefee') !='') {
							echo 'There is a '.get_option('onlinecontract_contractcurrency').get_option('onlinecontractauthorizenet_swipefee').' convenience fee to use Authorize.net to pay your contract online.<br>';
						}
						?>
					<?php } ?>
					<?php if (is_plugin_active( 'wp-online-contract-dwolla-payments/online-contract-dwolla-payments.php' ) && get_option('onlinecontractdwolla_key')!='' && $paypalamount!='') { ?>
						<?php
						if (get_option('onlinecontractdwolla_swipefee') !='') {
							echo 'There is a '.get_option('onlinecontract_contractcurrency').get_option('onlinecontractdwolla_swipefee').' convenience fee to use Dwolla to pay your contract online.<br>';
						}
						?>
					<?php } ?>
				</div>
				<div style="clear:both;"></div>
				<?php } ?>
		</div>
		<?php endwhile; // end of the loop. ?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
		<!--[if lt IE 9]>
		<script type="text/javascript" src="<?php echo plugin_dir_url($file); ?>includes/js/flashcanvas.js"></script>
		<![endif]-->
		<script src="<?php echo plugin_dir_url($file); ?>includes/js/jSignature.min.js"></script>

		<script>

			$(document).ready(function() {

				$("#signature").jSignature();

			})

			function importImg(sig)

			{

				var dNow = new Date();

				var hours = dNow.getHours();

				var ampm = '';

				if (hours > 12) {

					hours -= 12;

					ampm = 'PM';

				} else if (hours === 0) {

					hours = 12;

					ampm = 'AM';

				}

				function pad(n) {

					 return (n < 10) ? '0' + n : n;

				}

				var localdate= (dNow.getMonth()+1) + '-' + dNow.getDate() + '-' + dNow.getFullYear() + ' ' + hours + ':' + pad(dNow.getMinutes()) + ampm;

				sig.children("img.imported").remove();

				//$("<img class='imported'></img").attr("src",sig.jSignature('getData')).appendTo(sig);

				$.ajax({

					type: 'POST',

					url: '<?php echo plugin_dir_url($file); ?>signaturesave.php',

					data: 'postid=<?php echo $postid; ?>&signed=1&signdate=' + localdate + '&ipaddress=<?php echo str_replace('.','-',$_SERVER['REMOTE_ADDR']); ?>&url=<?php echo get_permalink(); ?>&adminemail=<?php echo get_option('admin_email'); ?>&image=' + sig.jSignature('getData'),

					success: function(data){

						 //alert(data);

						 location.reload();

					}

				});

			}

			$("#printBtn").click(function(){

				printcontent($("#content").html());

			});

			function printcontent(content)

			{

				var mywindow = window.open('', '', '');

				mywindow.document.write('<html><title>Print</title><body>');

				mywindow.document.write(content);

				mywindow.document.write('</body></html>');

				mywindow.document.close();

				mywindow.print();

				return true;

			}

		</script>

	<?php }else{ ?>

		<div style="text-align:center; margin-top:20%;"><?php echo get_the_password_form(); ?></div>

	<?php } ?>

<?php }else{

	ob_start();

	if ( have_posts() ) while ( have_posts() ) : the_post();

		require_once('includes/pdf/tcpdf.php');

		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		$pdf->setPrintHeader(false);

		$pdf->setPrintFooter(false);

		$pdf->AddPage();

	?>

	<?php

	$contractstr =

		'<div id="content">';
			$content = apply_filters( 'the_content', get_the_content() );
			$content = str_replace( ']]>', ']]&gt;', $content );
			$contractstr .= stripslashes(do_shortcode( $content));
			$contractstr = str_replace(array('<strong>','</strong>'),'',$contractstr);
			$postid = get_the_ID();

			$signed = 0;

			$upload_dir = str_replace('plugins/','uploads/',dirname(__FILE__)).'/';
				if ($handle = opendir(str_replace('includes/functions/','',$upload_dir) . 'signatures/')) {

					while (false !== ($entry = readdir($handle))) {

						if ($entry != "." && $entry != "..") {

							$imgnamearr = explode('_',$entry);

							if ($imgnamearr[0]==$postid) {

								$signed =1;

								$fullimgname = $entry;

								$signeddate = str_replace('.png','',$imgnamearr[1]);

								$ipaddress = str_replace('-','.',str_replace('.png','',$imgnamearr[2]));

							}

						}

					}

					closedir($handle);

			   }

			$protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ||
			 $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
			 $contractstr .= '<img src="'.$protocol.str_replace(array('http://', 'https://'), '',WP_CONTENT_URL).'/uploads/onlinecontract/signatures/'.str_replace(' ','%20',$fullimgname).'">

			<br>

			<div style="text-align:center;">Signed on '.$signeddate;

			if ($ipaddress) {

				$contractstr .= '<br><strong>IP Address:</strong> '.$ipaddress;

			}

			$contractstr .= '</div>

		</div>

		<div style="clear:both;"></div>';
	$contractstr .= '
	<style>
		.signedby {
			font-family: dawningofanewdayi;
			font-size:35px;
			font-weight:strong;
		}
		.entry-content img {
			margin: 0 0 1.5em 0;
		}
		.alignleft, img.alignleft {
			margin-right: 1.5em;
			display: inline;
			float: left;
			}
		.alignright, img.alignright {
			margin-left: 1.5em;
			display: inline;
			float: right;
			}
		.aligncenter, img.aligncenter {
			margin-right: auto;
			margin-left: auto;
			display: block;
			clear: both;
			}
		.wp-caption {
			margin-bottom: 1.5em;
			text-align: center;
			padding-top: 5px;
			}
		.wp-caption img {
			border: 0 none;
			padding: 0;
			margin: 0;
			}
		.wp-caption p.wp-caption-text {
			line-height: 1.5;
			font-size: 10px;
			margin: 0;
			}
		.wp-smiley {
			margin: 0 !important;
			max-height: 1em;
			}
		blockquote.left {
			margin-right: 20px;
			text-align: right;
			margin-left: 0;
			width: 33%;
			float: left;
			}
		blockquote.right {
			margin-left: 20px;
			text-align: left;
			margin-right: 0;
			width: 33%;
			float: right;
			}
		.button-primary, .button-secondary {
			display: inline-block;
			text-decoration: none;
			font-size: 18px;
			line-height: 38px;
			height: 38px;
			margin: 0;
		padding: 0 10px 1px;
			cursor: pointer;
			border-width: 1px;
			border-style: solid;
			-webkit-appearance: none;
			-webkit-border-radius: 3px;
			border-radius: 3px;
			white-space: nowrap;
			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
		}
	</style>';
		$pdf->writeHTMLCell(0, 0, '', '', $contractstr, 0, 1, 0, true, '', true); ob_end_clean(); $pdf->Output('contract.pdf', 'I');
	?>
	<?php endwhile; // end of the loop. ?>
<?php } ?>