<?php
$text = $_POST['contract'];
$title = clean($_POST['title']);
$title = strtolower(str_replace('txt','',$title)).'.txt';
if ($title == '') {
	$date = new DateTime();
	$title = $date->getTimestamp();
}
define('CONTRACTPATH', str_replace('plugins/','uploads/',dirname(__FILE__)).'/'); 
file_put_contents(CONTRACTPATH . 'contracts/'.$title, $text);

function clean($input) {
	$title = stripslashes($input);
	$title = preg_replace("/[^a-zA-Z0-9\s]/", "", $title);
	$title = str_replace(' ', '-', $title);
	return $title;
}
?>
