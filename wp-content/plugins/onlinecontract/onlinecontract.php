<?php
define('onlinecontract_version_num', '2.15');
/*

Plugin Name: WP Online Contract

Plugin URI: http://futuredesigngroup.com/downloads/wp-online-contract

Description: WP Online Contract allows you to create, manage, and save contracts online through Wordpress.  You can customize contracts using flat txt files and shortcodes and view revisions of your existing contracts.  Give your clients a way to view and sign your contracts online in one place.

Version: 2.15

Author: Future Design Group

Author URI: http://futuredesigngroup.com

License: GPL2

*/

/*

Copyright 2014  Future Design Group  (email : olivere@futuredesigngroup.com)

*/
// Grab custom template
include('includes/functions/functions.php');
require('live-update.php');
add_filter( 'template_include','include_template_function', 1 );

function include_template_function($template_path ) {

	if ( get_post_type() == 'contract' ) {

        //Remove WPML from contracts
        define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);

	if ( is_single() ) {

	if ( $theme_file = locate_template( array( 'single-contract.php' ) ) ) {

	$template_path = $theme_file;

	} else {

	$template_path = plugin_dir_path( __FILE__ ) .'single-contract.php';

	}

	}

	}

	return $template_path;

}
add_filter( 'manage_edit-contract_columns', 'my_edit_movie_columns' ) ;

function my_edit_movie_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Contract Name' ),
		'signed' => __( 'Signed?' ),
		'date' => __( 'Date' )
	);

	return $columns;
}

// Contract Post Type

add_action( 'init', 'register_cpt_contract' );

function register_cpt_contract() {
    $labels = array( 

        'name' => _x( 'Online Contract', 'contract' ),

        'singular_name' => _x( 'Contract', 'contract' ),

        'add_new' => _x( 'Add New', 'contract' ),

        'add_new_item' => _x( 'Add New Contract Item', 'contract' ),

        'edit_item' => _x( 'Edit Contract', 'contract' ),

        'new_item' => _x( 'New Contract', 'contract' ),

        'view_item' => _x( 'View Contract', 'contract' ),

        'search_items' => _x( 'Search Contracts', 'contract' ),

        'not_found' => _x( 'No contracts found', 'contract' ),

        'not_found_in_trash' => _x( 'No contracts found in Trash', 'contract' ),

        'parent_item_colon' => _x( 'Parent Contract:', 'contract' ),

        'menu_name' => _x( 'Contracts', 'contract' ),

    );



    $args = array( 

		'menu_icon' => '',

        'labels' => $labels,

        'hierarchical' => false,

        'description' => 'Create a contract and allow your clients to view, sign, and download a PDF version.',

        'supports' => array( 'title', 'editor', 'revisions' ),

        'taxonomies' => array('' ),

        'public' => true,

        'show_ui' => true,

        'show_in_menu' => true,

        'show_in_nav_menus' => true,

        'publicly_queryable' => true,

        'exclude_from_search' => false,

        'has_archive' => false,

        'query_var' => true,

        'can_export' => true,

        'rewrite' => true,

        'capability_type' => 'post'

    );



    register_post_type( 'contract', $args );
	//flush_rewrite_rules();

	

}



// Add contract shortcode

function add_contract( $atts ) {

	extract( shortcode_atts(

		array(

			'clientname' => '',

			'projectname' => '',

			'contractamount' => '',

			'duedate'=>'',

			'paymentschedule'=>'',

			'contracttype' => '',

			'hourlyrate' => '',

			'signedby' => '',

			'signedbydate' => '',

            'clientid' => '',

            'guardian' => '',

		), $atts )

	);

	ob_start();
	$contractcontent = file_get_contents(WP_CONTENT_DIR . '/uploads/onlinecontract/contracts/'.$atts['contracttype'].'.txt');
	echo convertcontractshortcode($contractcontent, $atts); 
	return ob_get_clean();

}
function post_type_script($pagehook) {
    global $post_type;
    $pages = array( 'post.php', 'post-new.php' );
    if ( in_array( $pagehook, $pages ) && $post_type == 'contract' ) {
        add_filter( 'wp_default_editor', create_function('', 'return "html";') );
    }
}

add_action('admin_enqueue_scripts', 'post_type_script', '', array(&$this) );


add_shortcode( 'contract', 'add_contract' );

function onlinecontract_scripts() {
		wp_register_style('contract_allstyle', plugin_dir_url( __FILE__ ).'includes/css/style.css');
		wp_enqueue_style('contract_allstyle');
		
		wp_register_script('validatejs', plugin_dir_url( __FILE__ ).'includes/js/validate.js', array('jquery'));
		wp_enqueue_script('validatejs');

		wp_register_script('shortcodejs', plugin_dir_url( __FILE__ ).'includes/js/shortcode.js', array('jquery'));
		wp_enqueue_script('shortcodejs');
		
		wp_register_script('jqueryvalidation', plugin_dir_url( __FILE__ ).'includes/js/jquery.validate.min.js', array('jquery'));
		wp_enqueue_script('jqueryvalidation');
}
add_action( 'in_admin_footer', 'onlinecontract_scripts' );

add_action('init', 'wptuts_activate_au');
function wptuts_activate_au()
{
	require_once ('live-update.php');
    $wptuts_plugin_current_version = onlinecontract_version_num;
    $wptuts_plugin_remote_path = 'http://futuredesigngroup.com/apps/onlinecontracts/update.php';
    $wptuts_plugin_slug = plugin_basename(__FILE__);
    new wp_auto_update_contract ($wptuts_plugin_current_version, $wptuts_plugin_remote_path, $wptuts_plugin_slug);
}
