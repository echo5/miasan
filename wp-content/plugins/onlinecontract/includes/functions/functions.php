<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
$contractpath = WP_CONTENT_DIR . '/uploads/onlinecontract/';
$contractcontractspath = WP_CONTENT_DIR . '/uploads/onlinecontract/contracts/';
$contractsignaturespath = WP_CONTENT_DIR . '/uploads/onlinecontract/signatures/';
if(!is_dir($contractpath)){
    mkdir($contractpath);
}

if(!is_dir($contractcontractspath)){
    mkdir($contractcontractspath);

	$src = plugin_dir_path(__FILE__).'democontracts/';
	recurse_copy($src,$contractcontractspath);
}

if(!is_dir($contractsignaturespath)){
    mkdir($contractsignaturespath);
}

function recurse_copy($src,$dst) {
    $dir = opendir($src); 
    @mkdir($dst); 
    while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
                recurse_copy($src . '/' . $file,$dst . '/' . $file); 
            } 
            else { 
                copy($src . '/' . $file,$dst . '/' . $file); 
            } 
        } 
    } 
    closedir($dir); 
}

$prefix = '_onlinecontracts'; // Prefix of all meta additions to prevent plugin conflict

$meta_boxes_contract = array();

// Completed Meta Data
$meta_boxes_contract[] = array(
    'id' => 'contractcomplete',
    'title' => 'Contact Signed',
    'pages' => array('contract'),
    'context' => 'side',
    'priority' => 'high',
    'fields' => array(
		array(
            'name' => 'Complete?',
            'id' => 'contractcomplete',
            'type' => 'checkboxsigned'
        )
    )
);

$templates = str_replace('.txt','',showContent(WP_CONTENT_DIR . '/uploads/onlinecontract/contracts'));
$templates = explode('<br>',$templates);
$templatesarr = array();
foreach($templates as $template) {
	if($template !=''){
	$templatesarr[] = array('name'=>ucwords(str_replace('_',' ',str_replace('-',' ',$template))), 'value'=>$template);
	}

}


$contractlengths = explode(',',get_option('onlinecontract_contractterms'));
$sets = array();
foreach ($contractlengths as $value) {
    $sets[] = array('name'=>$value, 'value'=>$value);
}
// Contract Information Meta Data
$meta_boxes_contract[] = array(
    'id' => 'contractdetails',
    'title' => 'Contract Details',
    'pages' => array('contract'),
    'context' => 'normal',
    'priority' => 'high',
    'fields' => array(
		array(
            'name' => 'Due Date',
            'desc' => 'Select the project\'s due date',
            'id' => 'contractdetails_duedate',
            'type' => 'datepicker',
            'std' => ''
       	),
		array(
            'name' => 'Client Name',
            'desc' => 'Enter the name of the client this contract is for',
            'id' => 'contractdetails_clientname',
            'type' => 'text',
            'std' => ''
        ),
        array(
            'name' => 'Guardian',
            'desc' => 'Parent or Guardian',
            'id' => 'contractdetails_guardian',
            'type' => 'text',
            'std' => ''
        ),
		array(
            'name' => 'Client Email',
            'desc' => 'Enter the email address of this client',
            'id' => 'contractdetails_emailaddress',
            'type' => 'text',
            'std' => ''
        ),
		array(
            'name' => 'Client Mailing Address',
            'desc' => 'Enter the mailing address of this client',
            'id' => 'contractdetails_mailingaddress',
            'type' => 'textarea',
            'std' => ''
        ),
		array(
            'name' => 'Project Name',
            'desc' => 'Enter the name of the company this contract is for',
            'id' => 'contractdetails_projectname',
            'type' => 'text',
            'std' => ''
       	),
		array(
            'name' => 'Project Amount / Monthly Payments',
            'desc' => 'Enter the project or maintenance total amount with commas (ex: 5600.00)',
            'id' => 'contractdetails_contractamount',
            'type' => 'text',
            'std' => ''
       	),
		array(
            'name' => 'Payment Schedule',
            'id' => 'contractdetails_paymentschedule',
            'type' => 'select',
			'options' => $sets
       	),
		array(
            'name' => 'Contract type',
            'id' => 'contractdetails_contracttype',
            'type' => 'select',
			'options' => $templatesarr
       	),
		array(
            'name' => 'Your Hourly Rate',
            'desc' => 'Enter the amount you will get paid for additional hourly work outside of the scope (ex: 150.00)',
            'id' => 'contractdetails_hourlyrate',
            'type' => 'text',
            'std' => ''
       	),
		array(
            'name' => 'Signed By',
            'desc' => 'Enter the name of the person from your team that will sign this contract (it will appear as signed on the contract)',
            'id' => 'contractdetails_signedby',
            'type' => 'text',
            'std' => ''
       	),
        array(
            'name' => 'Signed By On',
            'desc' => 'Select the date this contract was signed by your team member',
            'id' => 'contractdetails_signedbydate',
            'type' => 'datepicker',
            'std' => ''
        ),
        array(
            'name' => 'Client ID',
            'desc' => 'ID Number of client',
            'id' => 'contractdetails_clientid',
            'type' => 'text',
            'std' => ''
        ),
        array(
            'name' => 'Payment (optional)',
            'desc' => 'If you are integrating a payment method with this contract, please put the amount that should be paid when your client views the contract (ex: 1500.00)',
            'id' => 'contractdetails_paymentamout',
            'type' => 'text',
            'std' => ''
        ),
		array(
            'name' => 'Send Password to Client? (optional)',
            'desc' => 'If this contract is password protected and you want to automatically send the client the password, add the password you used to protect this contract here',
            'id' => 'contractdetails_contractpassword',
            'type' => 'text',
            'std' => ''
       	),
		array(
            'name' => 'Send Contract to Client?',
            'id' => 'contractdetails_sendcontracttoclient',
            'type' => 'checkbox'
        ),
		array(
            'name' => 'Send Notification When Viewed?',
            'id' => 'contractdetails_sendnotification',
            'type' => 'checkbox'
        ),
    )
);

// Functions and classes to add meta data to custom post types
foreach ($meta_boxes_contract as $meta_box) {
	$my_box = new RW_Meta_Box_Contract($meta_box);
}

// Validate value of meta fields

// Define ALL validation methods inside this class
// and use the names of these methods in the definition of meta boxes (key 'validate_func' of each field)
class RW_Meta_Box_Validate_Contract {
	function check_text($text) {
		if ($text != 'hello') {
			return false;
		}
		return true;
	}
}
 
add_action('wp_ajax_unlink_file', 'unlink_file_callback_Contract');
function unlink_file_callback_Contract() {
	global $wpdb;
	if ($_POST['data']) {
		$data = explode('-', $_POST['data']);
		$att_id = $data[0];
		$post_id = $data[1];
		wp_delete_attachment($att_id);
	}
}

// Create meta boxes
class RW_Meta_Box_Contract {

	protected $_meta_box;

	// create meta box based on given data
	function __construct($meta_box) {
		if (!is_admin()) return;

		$this->_meta_box = $meta_box;
		$current_page = substr(strrchr($_SERVER['PHP_SELF'], '/'), 1, -4);
		if (($current_page == 'page' || $current_page == 'page-new' || $current_page == 'post' || $current_page == 'post-new')) {
			add_action('admin_head', array(&$this, 'add_post_enctype'));
			add_action('admin_head', array(&$this, 'add_unlink_script'));
			add_action('admin_head', array(&$this, 'add_clone_script'));
		}

		add_action('admin_menu', array(&$this, 'add'));

		add_action('save_post', array(&$this, 'save'));
	}

	function add_post_enctype() {
		echo '
		<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery("#post").attr("enctype", "multipart/form-data");
			jQuery("#post").attr("encoding", "multipart/form-data");
		});
		</script>';
	}

	function add_unlink_script(){
		echo '
		<script type="text/javascript">
		jQuery(document).ready(function($){
			$("a.deletefile").click(function () {
				var parent = jQuery(this).parent().parent(),
					data = jQuery(this).attr("rel"),
					_wpnonce = $("input[name=\'_wpnonce\']").val();

				$.post(
					ajaxurl,
					{action: \'unlink_file\', _wpnonce: _wpnonce, data: data},
					function(response){
						//$("#info").html(response).fadeOut(3000);
						//alert(data.post);
					},
					"json"
				);
				parent.fadeOut("slow");
				return false;
			});
		});
		</script>';
	}

	function add_clone_script() {
		echo '
		<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery(".add").click(function() {
				jQuery("#newimages p:first-child").clone().insertAfter("#newimages p:last").show();
				return false;
			});
			jQuery(".remove").click(function() {
				jQuery(this).parent().remove();
			});
		});
		</script>';
	}


	/// Add meta box for multiple post types
	function add() {
		$this->_meta_box['context'] = empty($this->_meta_box['context']) ? 'normal' : $this->_meta_box['context'];
		$this->_meta_box['priority'] = empty($this->_meta_box['priority']) ? 'high' : $this->_meta_box['priority'];
		foreach ($this->_meta_box['pages'] as $page) {
			add_meta_box($this->_meta_box['id'], $this->_meta_box['title'], array(&$this, 'show'), $page, $this->_meta_box['context'], $this->_meta_box['priority']);
		}
	}

	// Callback function to show fields in meta box
	function show() {
		global $post;

		// Use nonce for verification
		echo '<input type="hidden" name="wp_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

		echo '<table class="form-table">';
		$tabid = 0;
		foreach ($this->_meta_box['fields'] as $field) {
			$tabid++;
			// get current post meta data
			$meta = get_post_meta($post->ID, $field['id'], true);
			echo '<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"><script src="//code.jquery.com/jquery-1.9.1.js"></script><script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>';
			echo '<tr>',
					'<th style="width:20%"><label for="', $field['id'], '">', $field['name'], '</label></th>',
					'<td>';
			switch ($field['type']) {
				case 'text':
					if($field['id']=='contractdetails_paymentamout' || $field['id']=='contractdetails_contractpassword') {
						echo '<input type="text" tabindex='.$tabid.' name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />',
						'<br />', $field['desc'];
					}else{
						echo '<input type="text" tabindex='.$tabid.' name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" required />',
						'<br />', $field['desc'];
					}
					break;
				case 'textarea':
					echo '<textarea tabindex='.$tabid.' name="', $field['id'], '" id="', $field['id'], '" cols="60" rows="15" style="width:97%">', $meta ? $meta : $field['std'], '</textarea>',
						'<br />', $field['desc'];
					break;
				case 'select':
					echo '<select tabindex='.$tabid.' name="', $field['id'], '" id="', $field['id'], '" required>';
					echo '<option selected disabled value="">Please Select</option>';
					foreach ($field['options'] as $option) {
						echo '<option value="', $option['value'], '"', $meta == $option['value'] ? ' selected="selected"' : '', '>', $option['name'], '</option>';
					}
					echo '</select>';
					break;
				case 'radio':
					foreach ($field['options'] as $option) {
						echo '<input tabindex='.$tabid.' type="radio" name="', $field['id'], '" value="', $option['value'], '"', $meta == $option['value'] ? ' checked="checked"' : '', ' />', $option['name'];
					}
					break;
				case 'checkbox':
					echo '<input tabindex='.$tabid.' type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />';
					break;
				case 'checkboxsigned':
					$postid = $_GET['post'];
					$signed = 0;
					$fullimgname = '';
					$upload_dir = str_replace('plugins/','uploads/',dirname(__FILE__)).'/'; 
					if ($handle = opendir(str_replace('includes/functions/','',$upload_dir) . 'signatures/')) {
							while (false !== ($entry = readdir($handle))) {
								if ($entry != "." && $entry != "..") {
									$imgnamearr = explode('_',$entry);
									if ($imgnamearr[0]==$postid) {
										$fullimgname = $entry;
										echo '<div style="text-align:center;"><img src="'.WP_CONTENT_URL . '/uploads/onlinecontract/signatures/'.$fullimgname.'" style="width:100%; height:auto;">';
										if($meta=='') {
											echo '<input type="checkbox" name="', $field['id'], '" id="', $field['id'], '" checked="checked" />';
										}else{
											echo '<input type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />';
										}
										echo '<br><small style="color:red;">Uncheck to remove signature and open contract.</small>';
										echo '<input type="hidden" name="contractsignature" id="contractsignature"  value="'.WP_CONTENT_DIR . '/uploads/onlinecontract/signatures/'.$fullimgname.'">';
										$signed = 1;
										break;
									}
								}
								
							}
							if ($signed!=1) {
								echo '<small style="color:red;">Waiting on client to sign contract.</small>';
								echo '<input type="checkbox" name="'.$field['id'].'" id="'.$field['id'].'" checked="checked" style="display:none;" />';
							}
							closedir($handle);
					   }
					
					break;
				case 'datepicker':
					echo '<script>
						  $(function() {
							$( "#', $field['id'], '" ).datepicker();
						  });
						  </script>';
					echo '<input tabindex='.$tabid.' type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:50%" required>';
					break;
				case 'wysiwyg':
					echo '<textarea tabindex='.$tabid.' name="', $field['id'], '" id="', $field['id'], '" class="theEditor" cols="60" rows="4" style="width:97%">', $meta ? $meta : $field['std'], '</textarea>',
						'<br />', $field['desc'];
					break;
			}
			echo 	'<td>',
				'</tr>';
		}

		echo '</table>';
	}

	// Save data from meta box
	function save($post_id) {
		// verify nonce
		if (!wp_verify_nonce($_POST['wp_meta_box_nonce'], basename(__FILE__))) {
			return $post_id;
		}

		// check autosave
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return $post_id;
		}

		// check permissions
		if ('page' == $_POST['post_type']) {
			if (!current_user_can('edit_page', $post_id)) {
				return $post_id;
			}
		} elseif (!current_user_can('edit_post', $post_id)) {
			return $post_id;
		}
		foreach ($this->_meta_box['fields'] as $field) {
			$name = $field['id'];

			$old = get_post_meta($post_id, $name, true);
			$new = $_POST[$field['id']];
			if ($name == 'contractcomplete' && $new != 'on' ) {
				unlink($_POST['contractsignature']);
			}
			if ($name == 'contractdetails_contractpassword' && $new != '') {
				$sendpassword = '<br><br><strong>NOTE:</strong> This contract is password protected.  You can view it by using the following password: '.$new;
			}
			if (get_post_status($post_id) == 'publish' || get_post_status($post_id) == 'private' || get_post_status($post_id) == 'inherit') {
				if ($name == 'contractdetails_sendcontracttoclient' && $new == 'on') {
					//$message = 'A new contract has been created for you.  Can you view and sign this contract online using the following link:';
					if(get_option('onlinecontract_emailtemplate')) {
						$emailtemplate = get_option('onlinecontract_emailtemplate');
						$contentextract = explode('" ', get_post_field('post_content', $post_id));
						$clientname = str_replace('[contract clientname="','',$contentextract[0]);
						if(strpos($contentextract[1],'mailingaddress') !== false) {
							$clientmailingaddress = str_replace('clientmailingaddress="','',$contentextract[1]);
							$projectname = str_replace('projectname="','',$contentextract[2]);
							$duedate = str_replace('duedate="','',$contentextract[3]);
							$contractamount = str_replace('contractamount="','',$contentextract[4]);
							$paymentschedule = str_replace('paymentschedule="','',$contentextract[5]);
							$hourlyrate = str_replace('hourlyrate="','',$contentextract[7]);
							$signedby = str_replace('signedby="','',$contentextract[8]);
                            $signedbydate = str_replace('signedbydate="','',$contentextract[9]);
                            $clientid = str_replace('clientid="','',$contentextract[10]);
							$guardian = str_replace('guardian="','',$contentextract[11]);
						}else{
							$projectname = str_replace('projectname="','',$contentextract[1]);
							$duedate = str_replace('duedate="','',$contentextract[2]);
							$contractamount = str_replace('contractamount="','',$contentextract[3]);
							$paymentschedule = str_replace('paymentschedule="','',$contentextract[4]);
							$hourlyrate = str_replace('hourlyrate="','',$contentextract[6]);
							$signedby = str_replace('signedby="','',$contentextract[7]);
							$signedbydate = str_replace('signedbydate="','',$contentextract[8]);
                            $clientid = str_replace('clientid="','',$contentextract[9]);
                            $guardian = str_replace('guardian="','',$contentextract[10]);

						}
					}else{
						$emailtemplate = 'A new contract has been created for you.  Can you view and sign this contract online using the following link:<br>[contract-link]';
					}
					if (strpos($emailtemplate,'[contract-link]') !== false) {
						$emailtemplate = str_replace('[contract-link]','<a href="'.post_permalink($post_id).'">'.post_permalink($post_id).'</a>',$emailtemplate);
					}
					if (strpos($emailtemplate,'[clientname]') !== false) {
						$emailtemplate = str_replace('[clientname]',$clientname,$emailtemplate);
					}
					if (strpos($emailtemplate,'[clientmailingaddress]') !== false) {
						$emailtemplate = str_replace('[clientmailingaddress]',$clientmailingaddress,$emailtemplate);
					}
					if (strpos($emailtemplate,'[projectname]') !== false) {
						$emailtemplate = str_replace('[projectname]',$projectname,$emailtemplate);
					}
					if (strpos($emailtemplate,'[duedate]') !== false) {
						$emailtemplate = str_replace('[duedate]',$duedate,$emailtemplate);
					}
					if (strpos($emailtemplate,'[contractamount]') !== false) {
						$emailtemplate = str_replace('[contractamount]',$contractamount,$emailtemplate);
					}
					if (strpos($emailtemplate,'[paymentschedule]') !== false) {
						$emailtemplate = str_replace('[paymentschedule]',$paymentschedule,$emailtemplate);
					}
					if (strpos($emailtemplate,'[hourlyrate]') !== false) {
						$emailtemplate = str_replace('[hourlyrate]',$hourlyrate,$emailtemplate);
					}
					if (strpos($emailtemplate,'[signedby]') !== false) {
						$emailtemplate = str_replace('[signedby]',$signedby,$emailtemplate);
					}
                    if (strpos($emailtemplate,'[signedbydate]') !== false) {
                        $emailtemplate = str_replace('[signedbydate]',$signedbydate,$emailtemplate);
                    }
                    if (strpos($emailtemplate,'[clientid]') !== false) {
                        $emailtemplate = str_replace('[clientid]',$clientid,$emailtemplate);
                    }
                    if (strpos($emailtemplate,'[guardian]') !== false) {
                        $emailtemplate = str_replace('[guardian]',$guardian,$emailtemplate);
                    }
					if (strpos($emailtemplate,'[businessname]') !== false) {
						$emailtemplate = str_replace('[businessname]',get_option('onlinecontract_businessname'),$emailtemplate);
					}
					if (strpos($emailtemplate,'[contractcurrency]') !== false) {
						$emailtemplate = str_replace('[contractcurrency]',get_option('onlinecontract_contractcurrencytype'),$emailtemplate);
					}
					if(get_option('onlinecontract_emailtemplatesubject')) {
						$emailtemplatesubject = get_option('onlinecontract_emailtemplatesubject');
					}else{
						$emailtemplatesubject = 'A New Contract Has Been Created For You';
					}
					//get_option('onlinecontract_businessname')
					$message = $emailtemplate.$sendpassword;
					$headers = "From: ".get_option('admin_email')."\r\n";
					$headers .= "MIME-Version: 1.0\n";
					$headers .= "Content-Type: text/html; charset=ISO-8859-1\n";
					mail(get_post_meta($post_id, 'contractdetails_emailaddress', true), $emailtemplatesubject, $message, $headers);
				}
			}
			/*
			// changed to using WP gallery
			if ($field['type'] == 'file' || $field['type'] == 'image') {
				$file = wp_handle_upload($_FILES[$name], array('test_form' => false));
				$new = $file['url'];
			}
			*/

			if ($field['type'] == 'file' || $field['type'] == 'image') {
				if (!empty($_FILES[$name])) {
					$this->fix_file_array($_FILES[$name]);
					foreach ($_FILES[$name] as $position => $fileitem) {
						$file = wp_handle_upload($fileitem, array('test_form' => false));
						$filename = $file['url'];
						if (!empty($filename)) {
							$wp_filetype = wp_check_filetype(basename($filename), null);
							$attachment = array(
								'post_mime_type' => $wp_filetype['type'],
								'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
								'post_status' => 'inherit'
							);
							$attach_id = wp_insert_attachment($attachment, $filename, $post_id);
							// you must first include the image.php file
							// for the function wp_generate_attachment_metadata() to work
							require_once(ABSPATH . 'wp-admin/includes/image.php');
							$attach_data = wp_generate_attachment_metadata($attach_id, $filename);
							wp_update_attachment_metadata($attach_id, $attach_data);
						}
					}
				}
			}

			if ($field['type'] == 'wysiwyg') {
				$new = wpautop($new);
			}

			if ($field['type'] == 'textarea') {
				$new = htmlspecialchars($new);
			}

			// validate meta value
			if (isset($field['validate_func'])) {
				$ok = call_user_func(array('RW_Meta_Box_Validate_Contract', $field['validate_func']), $new);
				if ($ok === false) { // pass away when meta value is invalid
					continue;
				}
			}
			if ($new && $new != $old) {
				update_post_meta($post_id, $name, $new);
			} elseif ('' == $new && $old && $field['type'] != 'file' && $field['type'] != 'image') {
				delete_post_meta($post_id, $name, $old);
			}
		}
	}

	function fix_file_array(&$files) {
		$names = array(
			'name' => 1,
			'type' => 1,
			'tmp_name' => 1,
			'error' => 1,
			'size' => 1
		);

		foreach ($files as $key => $part) {
			// only deal with valid keys and multiple files
			$key = (string) $key;
			if (isset($names[$key]) && is_array($part)) {
				foreach ($part as $position => $value) {
					$files[$position][$key] = $value;
				}
				// remove old key reference
				unset($files[$key]);
			}
		}
	}
}

function showContent($path){
$dirlisting = '';
   if ($handle = opendir($path))
   {
	   while (false !== ($file = readdir($handle)))
	   {
		   if ($file != "." && $file != "..")
		   {
			   $fName  = $file;
			   $file   = $path.'/'.$file;
			   $ext = pathinfo($file, PATHINFO_EXTENSION);
			   if(is_file($file) && $ext =='txt') {
				   $dirlisting .= $fName.'<br>';
			   }
		   }
	   }

	   closedir($handle);
   }  

return $dirlisting;
}

function showContentEditSelect($path){
$dirlisting = '';
global $contractcontractspath;
$dirlisting = '<div id="onlinecontract-infotext"><h3>Need To Edit An Existing Contract...</h3><p>Choose from your existing contracts below.  Remember, this choice will refresh the page and you will lose changes to what you are working on.  Save your current edits or new editions using the "Save Changes" button on the top right.</p>
<div style="width:98%; float:left; line-height:28px; background-color:#f8f8f8; padding:1%; margin-bottom:5px; text-align:right;"><input type="button" class="button-primary" onclick="if (makesureSave()) { window.open(\'edit.php?post_type=contract&page=edit_contracts\', \'_self\')};" value="Create New"></div>';
   if ($handle = opendir($path))
   {
	   while (false !== ($file = readdir($handle)))
	   {
		   if ($file != "." && $file != "..")
		   {
			   $fName  = $file;
			   $file   = $path.'/'.$file;
			   $ext = pathinfo($file, PATHINFO_EXTENSION);
			   
			   if(is_file($file) && $ext =='txt') {
				   $dirlisting .= '<div style="width:48%; float:left; line-height:28px; background-color:#f8f8f8; padding:1%; margin-bottom:5px;"><input type="checkbox" name="file[]" value="'.$file.'" />'.$fName.'</div><div style="width:48%; float:left; text-align:right; background-color:#f4f4f4; padding:1%; margin-bottom:5px;"><input type="button" class="button-primary" onclick="if (makesureSave()) { window.open(\'edit.php?post_type=contract&page=edit_contracts&contract='.str_replace('.txt','',$fName).'\', \'_self\')};" value="Edit This Contract"> <input type="button" class="button-secondary" onclick="window.open(\''.content_url().'/uploads/onlinecontract/contracts/'.$fName.'\',\'_blank\')" value="View Source Code"></div> ';
			   }
			   
		   }
	   }

	   closedir($handle);
   }  
return $dirlisting;
}

add_action( 'manage_contract_posts_custom_column', 'my_manage_contracts_columns', 10, 2 );

function my_manage_contracts_columns( $column, $post_id ) {
	global $post;

	switch( $column ) {
		case 'signed' :
			$signed = 0;
			$upload_dir = wp_upload_dir();
			if ($handle = opendir($upload_dir['basedir'] . '/onlinecontract/signatures/')) {
							while (false !== ($entry = readdir($handle))) {
								if ($entry != "." && $entry != "..") {
									$imgnamearr = explode('_',$entry);
									if ($imgnamearr[0]==$post_id) {
										$fullimgname = $entry;
										echo '<span style="color: white; background-color: green; border-radius: 3px; padding: 2px 5px; font-size:10px;">Signed</span>';
										$signed = 1;
										break;
									}
								}
								
							}
							if ($signed!=1) {
								echo '<span style="color: white; background-color: red; border-radius: 3px; padding: 2px 5px; font-size:10px;">Unsigned</span>';
							}
							closedir($handle);
					   }
		default :
			break;
	}
}


add_action('admin_menu', 'register_my_custom_submenu_page');

function sortable_columns() {
  return array(
    'signed'      => 'signed',
	'title'       => 'title'
  );
}

add_filter( "manage_edit-contract_sortable_columns", "sortable_columns" );

function register_my_custom_submenu_page() {
	add_submenu_page( 'edit.php?post_type=contract', 'Contract Options', 'Contract Options', 'manage_options', 'contract_options', 'contract_settings' ); 
}

function contract_settings() {
	$contractbusinessname = get_option('onlinecontract_businessname');
	$contractterms = get_option('onlinecontract_contractterms');
	$contractcurrency = get_option('onlinecontract_contractcurrency');
	$contractcurrencytype = get_option('onlinecontract_contractcurrencytype');
	$contractpaypalcode = get_option('onlinecontract_contractpaypalcode');
	$contractpaypalfee = get_option('onlinecontract_contractpaypalfee');
	$contractemailtemplate = get_option('onlinecontract_emailtemplate');
	$contractemailtemplatesubject = get_option('onlinecontract_emailtemplatesubject');
	$contractstyletemplate = get_option('onlinecontract_styletemplate');
	$html = '
	<div id="onlinecontract-settings">
		<form action="options.php" method="post" name="options" enctype="multipart/form-data">
		<h3>Contract Settings</h3>
		'. wp_nonce_field('update-options') .'
		<table class="form-table" width="100%" cellpadding="10">
		<tbody>
		<tr>
		<th style="width:15%"><label for="onlinecontract_businessname">Your Agency Name</label></th>
		<td><input type="text" name="onlinecontract_businessname" id="onlinecontract_businessname" value="' . $contractbusinessname . '" size="30" style="width:97%;" required><br>Enter the name of your company that will display on the contract.</td>
		</tr>
		<tr>
		<th style="width:15%"><label for="onlinecontract_contractterms">Contract Terms</label></th>
		<td><input type="text" name="onlinecontract_contractterms" id="onlinecontract_contractterms" value="' . $contractterms . '" size="30" style="width:50%;" required><br>Enter your payment schedule terms. (ex: 1/6,1/3,1/4,1/2,1)</td>
		</tr>
		<tr>
		<th style="width:15%"><label for="onlinecontract_contractcurrency">Currency Symbol</label></th>
		<td><input type="text" name="onlinecontract_contractcurrency" id="onlinecontract_contractcurrency" value="' . $contractcurrency. '" size="30" style="width:50%;" required><br>Enter your current symbol (ex: $ or &pound;, etc)</td>
		</tr>
		<tr>
		<th style="width:15%"><label for="onlinecontract_contractcurrencytype">Currency Type</label></th>
		<td><input type="text" name="onlinecontract_contractcurrencytype" id="onlinecontract_contractcurrencytype" value="' . $contractcurrencytype. '" size="30" style="width:50%;" required><br>Enter your currency type (ex: USD or EUR, etc)<br><small>A complete list can be found <a href="https://developer.paypal.com/docs/classic/api/currency_codes/" target="_blank" onclick="return makesureSave();">here</a></small></td>
		</tr>
		<tr>
		<th style="width:15%"><label for="onlinecontract_emailtemplatesubject">Notification Email Subject Template</label></th>
		<td><input name="onlinecontract_emailtemplatesubject" id="onlinecontract_emailtemplatesubject" style="width:50%;" required value="';
		if($contractemailtemplatesubject) { $html .= $contractemailtemplatesubject; }else{ $html .= 'A New Contract Has Been Created For You'; }
		$html .='"><br>Enter a subject for your contract.</td>
		</tr>
		<tr>
		<th style="width:15%"><label for="onlinecontract_emailtemplate">Notification Email Template</label></th>
		<td><textarea name="onlinecontract_emailtemplate" id="onlinecontract_emailtemplate" style="width:100%; height:250px;" required>';
		if($contractemailtemplate) { $html .= $contractemailtemplate; }else{ $html .= 'A new contract has been created for you.  Can you view and sign this contract online using the following link:&lt;br&gt;
		[contract-link]'; }
		$html .='</textarea><br>If you wish to change the notification email, please enter it here using HTML code. Use the shortcode <strong>[contract-link]</strong> to add a link back to the contract. You can also add other shortcodes used in your contracts (<a href="?post_type=contract&page=edit_contracts" target="_blank" onclick="return makesureSave();">view list</a>).<br>
		<small><em>The following shortcodes are <strong>NOT</strong> transferable: [clientemailaddress], [monthly], [remainingbalance]</em></small>
		<br>
		<br>
		<small>You can use the following HTML code for common elements:<br>&lt;br&gt; single link break, &lt;p&gt;paragraph&lt;/p&gt;, <strong>&lt;strong&gt;bold&lt;strong&gt;</strong>, <em>&lt;em&gt;italic&lt;em&gt;</em></small></td>
		</tr>
		<tr>
		<th style="width:15%"><label for="onlinecontract_styletemplate">Style Template</label></th>
		<td><textarea name="onlinecontract_styletemplate" id="onlinecontract_styletemplate" style="width:100%; height:250px;">';
		if($contractstyletemplate) { $html .= $contractstyletemplate; }else{ $html .= '
		body { font-family: \'Open Sans\',arial,sans-serif; margin:20px 0; }
		h1,h2 { font-family: \'Roboto Slab\', serif; text-transform:uppercase; } 
		.signedby { font-family: \'Dawning of a New Day\', cursive; font-size:45px;  font-weight:strong;}
		.entry-content img {
			margin: 0 0 1.5em 0;
			}
		.alignleft, img.alignleft {
			margin-right: 1.5em;
			display: inline;
			float: left;
			}
		.alignright, img.alignright {
			margin-left: 1.5em;
			display: inline;
			float: right;
			}
		.aligncenter, img.aligncenter {
			margin-right: auto;
			margin-left: auto;
			display: block;
			clear: both;
			}
		.alignnone, img.alignnone {
			/* not sure about this one */
			}
		.wp-caption {
			margin-bottom: 1.5em;
			text-align: center;
			padding-top: 5px;
			}
		.wp-caption img {
			border: 0 none;
			padding: 0;
			margin: 0;
			}
		.wp-caption p.wp-caption-text {
			line-height: 1.5;
			font-size: 10px;
			margin: 0;
			}
		.wp-smiley {
			margin: 0 !important;
			max-height: 1em;
			}
		blockquote.left {
			margin-right: 20px;
			text-align: right;
			margin-left: 0;
			width: 33%;
			float: left;
			}
		blockquote.right {
			margin-left: 20px;
			text-align: left;
			margin-right: 0;
			width: 33%;
			float: right;
			}
		.button-primary, .button-secondary {
			display: inline-block;
			text-decoration: none;
			font-size: 18px;
			line-height: 38px;
			height: 38px;
			margin: 0;
			padding: 0 10px 1px;
			cursor: pointer;
			border-width: 1px;
			border-style: solid;
			-webkit-appearance: none;
			-webkit-border-radius: 3px;
			border-radius: 3px;
			white-space: nowrap;
			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
			margin-top:20px;
		}
		.button-primary { 
			background: #2ea2cc;
			border-color: #0074a2;
			-webkit-box-shadow: inset 0 1px 0 rgba(120,200,230,.5),0 1px 0 rgba(0,0,0,.15);
			box-shadow: inset 0 1px 0 rgba(120,200,230,.5),0 1px 0 rgba(0,0,0,.15);
			color: #fff;
			text-decoration: none; 
		}
		.button-secondary {
			color: #555;
			border-color: #ccc;
			background: #f7f7f7;
			-webkit-box-shadow: inset 0 1px 0 #fff,0 1px 0 rgba(0,0,0,.08);
			box-shadow: inset 0 1px 0 #fff,0 1px 0 rgba(0,0,0,.08);
			vertical-align: top;
		}
		@media screen and (max-width:600px){
			body { font-size:12px; }
			.signedby { font-size:25px; }
			.button-primary, .button-secondary {
				font-size:12px;
			}
			div { line-height:normal !important; }
		}
		@media screen and (max-width:384px){
			body { font-size:12px; }
			.button-primary, .button-secondary {
				display: block;
				text-align: center;
				margin-top: 10px;
				width: 100%;
				font-size: 15px;
			}
			form { display: block !important; }
			div { width:98% !important; text-align:center !important; margin:0 auto; line-height:normal !important; }
			.signedby { font-size:45px; }
			p, ul, li, h1,h2,h3,h4,h5,h6 { text-align:left; }
			h1 { font-size:20px; }
			h2 { font-size:18px; }
		}'; }
		$html .= '</textarea><br>You can change the default style of the plugin by modifying the stylesheet above.  Leave as is if you want to use the default style.<br><br><small>Only change what you need, as most of this is WP specific and it helps the style of your contract and is responsive. <strong>Modify at your own risk.</strong></small></td>
		</tr>
		<tr><td colspan="2"><hr></td></tr>
		<tr><td colspan="2"><p>If you are wanting to use Paypal as a integrated method of Payment after a contract is signed, use the following section to provide a bit of information for our plugin to use, otherwise leave this section blank.</p></td></tr>
		<tr>
		<th style="width:15%"><label for="onlinecontract_contractpaypalcode">Paypal Account</label></th>
		<td><input type="text" name="onlinecontract_contractpaypalcode" id="onlinecontract_contractpaypalcode" value="' . $contractpaypalcode. '" size="30" style="width:50%;"><br>Enter your Paypal email or business ID</td>
		</tr>
		<tr>
		<th style="width:15%"><label for="onlinecontract_contractpaypalfee">Charge A Fee</label></th>
		<td><input type="text" name="onlinecontract_contractpaypalfee" id="onlinecontract_contractpaypalfee" value="' . $contractpaypalfee. '" size="30" style="width:50%;"><br>You can charge a fee to use Paypal (ex: 2.50 or 2.9%), however, please <a href="https://www.paypal.com/webapps/mpp/ua/useragreement-full#4" target="_blank" onclick="return makesureSave();">verify</a> with your country\'s TOS to make sure this is legal in your country.</td>
		</tr>		<tr><td colspan="2"><h3>Need Another Payment Method?</h2><p>Check out the addons we have created for other payment integrations.</p><br><p><strong>Stripe Payment:</strong> Stripe is a developer-friendly way to accept payments online and in mobile apps. We process billions of dollars a year for thousands of companies of all sizes.<br><a href="http://codecanyon.net/item/wp-online-contract-stripe-payments/8001391" target="_blank" onclick="return makesureSave();">View Our Stripe Payment Addon</a></p>
		<br><br><p><strong>Dwolla Payment:</strong> Dwolla payments gives you an alternative way to collect payment on your contracts instantly as soon as your clients sign. Get paid and secured quicker with Dwolla payments.<br><a href="http://codecanyon.net/item/wp-online-contract-dwolla-payments/8128849" target="_blank" onclick="return makesureSave();">View Our Dwolla Payment Addon</a></p><br><br><p><strong>Skrill Payment:</strong> Skrill has been moving money digitally since 2001 and is now one of world\'s leading digital payments companies, with 560 people from 30 nationalities working in our London headquarters and our offices throughout Europe and the USA.<br><a href="http://codecanyon.net/item/wp-online-contract-skrill-payments/8198739" target="_blank" onclick="return makesureSave();">View Our Skrill Payment Addon</a></p><br><br><p><strong>Authorize.net Payment:</strong> Authorize.Net enables merchants to authorize, settle and manage credit card and electronic check transactions via Web sites, retail stores, mail order/telephone order (MOTO) call centers and mobile devices.<br><a href="http://codecanyon.net/item/wp-online-contract-authorizenet-payments/8325553" target="_blank" onclick="return makesureSave();">View Our Authorize.net Payment Addon</a></p></td></tr>
		</tbody>
		</table>
		 <input type="hidden" name="action" value="update" />
		 <input type="hidden" name="page_options" value="onlinecontract_businessname,onlinecontract_contractterms,onlinecontract_contractcurrency,onlinecontract_contractcurrencytype,onlinecontract_emailtemplatesubject,onlinecontract_emailtemplate,onlinecontract_styletemplate,onlinecontract_contractpaypalcode,onlinecontract_contractpaypalfee" />
		<div style="float:right;">Version '.onlinecontract_version_num.'</div>
		 <input type="submit" name="btnupdatecontracts" id="btnupdatecontracts" value="Update Contract Settings" class="button-primary" />
		 </form>
	 </div>
	 <div id="onlinecontract-infotext">
		<a href="http://codecanyon.net/item/wp-online-contract/7698011" target="_blank" onclick="return makesureSave();"><img src="'.WP_CONTENT_URL.'/plugins/onlinecontract/includes/img/wp-online-contractlogo-small.png" style="max-width:512px; width:100%; height:auto; border:0px;"></a>
		<p>Thank you for using our Online Contract plugin. We hope our plugin gives you as much use as we have found in it.  Please rate this plugin if you like how it functions and if it helps you create unique, usable contracts that help you keep track of signatures and clients.</p>
		<p>You can rate our plugin and view other reviews using the following link through CodeCanyon in your downloads:<br><a href="http://codecanyon.net/downloads" target="_blank" onclick="return makesureSave();">http://codecanyon.com/downloads</a></p>
		<p>Interested in more of our plugins?  Please take a look at our CodeCanyon portfolio for all of our plugins and code:<br><a href="http://codecanyon.net/user/futuredesigngrp/portfolio" target="_blank" onclick="return makesureSave();">http://codecanyon.net/user/futuredesigngrp/portfolio</a></p>
		<h3>Note</h3>
		<p>If you would like to give your clients the opportunity to initiate a contract, you can easily add the following shortcode to any page or theme file:<br>
		<strong>[clientadd]</strong></p>
		<p>This will add a short form that can be styled by you that will allow a client to create a pending contract and send your administrative contact an email notification.</p>
		<p>See this plugins full documentation online using the following URL:<br>
		<a href="http://onlinecontract.futuredesigngroup.com/documentation/" target="_blank">http://onlinecontract.futuredesigngroup.com/documentation/</a>
		</p>
	</div>
	';
	echo $html;
}

add_action('admin_menu', 'register_my_custom_submenu_contracts_page');

function register_my_custom_submenu_contracts_page() {
	add_submenu_page( 'edit.php?post_type=contract', 'Edit Contracts', 'Edit Contracts', 'manage_options', 'edit_contracts', 'edit_contracts' ); 
}

function edit_contracts() {
	if(isset($_GET['contract'])) {
		$file = file_get_contents(WP_CONTENT_DIR . '/uploads/onlinecontract/contracts/'.$_GET['contract'] . ".txt", "r");
		$contractname = ucwords(str_replace('-',' ', $_GET['contract']));
	}
	echo '<form action="options.php" method="post" name="options" enctype="multipart/form-data">';
	echo '
	<div id="onlinecontract-settings">
		'. wp_nonce_field('update-options') .'
		<h3>Create or Edit A Contract</h3>
		<p>Use this page to created or edit contracts for your WP Online Contract usage. You can modify or create a contract using WordPress\'s built in editor or if you know how to use HTML code, you can use the code view.  If you need help with the shortcode usage, please see our guide at the bottom of the page.</p>';
		if ($file!= '' || !isset($_GET['contract'])) {
			echo '<table class="form-table" width="100%" cellpadding="10">
			<tbody>
			<tr>
			<th style="width:15%"><label for="onlinecontract_contractname">Contract Name</label></th>';
			if($contractname) {
			echo '<td><input type="text" name="onlinecontract_contractname" id="onlinecontract_contractname" value="' . stripslashes($contractname). '" size="30" style="width:90%;" required disabled><br>Current name (this is not editable because changing it will alter all contracts using this it)</td>';
			}else{
			echo '<td><input type="text" name="onlinecontract_contractname" id="onlinecontract_contractname" value="' . stripslashes($contractname). '" size="30" style="width:90%;"><br>Enter the name of this contract</td>';
		}
		echo '</tr>
		</tbody>
		</table>';
		}
	echo '</div>';
	echo showContentEditSelect(WP_CONTENT_DIR . '/uploads/onlinecontract/contracts');
	if(showContentEditSelect(WP_CONTENT_DIR . '/uploads/onlinecontract/contracts')) {
		echo '<br><label>Remove contract templates by checking their box (existing contracts with this template will no longer be usable).<br>You can view their source code using the button to the right to copy and create your own contracts.</label><br><br>';
	}else{
		echo '<label>There are no contracts.  Use our [shortcodes] to the right and create your own.</label><br><br>';
	}
	echo '<h3>...Or Upload A New Contract?</h3><label for="file">Select a file:</label> <input type="file" name="userfile" id="userfile"><br><div class="uploadtext">Upload your contract template file (only txt files can be uploaded)</div>';
	echo '<input type="submit" name="btnupdatecontracts" id="btnupdatecontracts" value="Update Contracts" class="button-primary" onclick="return makesureDelete();" style="margin-top:30px;" /></div>';
    $html = '';
	if ($file!= '' || !isset($_GET['contract'])) {
		$content = $file;
		wp_editor( wpautop(stripslashes($content)), 'editcontract' );
	}else{
		$html = '<div id="onlinecontract-settings" style="line-height: 400px; text-align: center;">The contract you selected, does not exist.  Please select another contract from the list below to continue.</div>';
	}
	$html .= '
	<div id="onlinecontract-infotext">
		<h3>Contract Shortcodes</h3>
		<hr>
		<div style="overflow-y:scroll; height:400px;">
			<strong>[clientname]</strong><br><br>
			<strong>[clientmailingaddress]</strong><br><br>
			<strong>[clientemailaddress]</strong><br><br>
			<strong>[projectname]</strong><br><br>
			<strong>[contractamount]</strong><br>
			This shortcode is used to display the contract amount.  Whether it is by month or in total.  Use this code to display your amount, our plugin will handle the rest.<br>
			<br>
			<strong>[hourlyrate]</strong><br><br>
			<strong>[duedate]</strong><br><br>
			<strong>[paymentschedule]</strong><br>
			This shortcode displays your payment schedule and helps the plugin decipher the payment amounts split per your schedule.
			<br>
			<br>
			<strong>[signedby]</strong><br>
			This shortcode will display a signature generated by our plugin with the name you set in the contract. If you wrap it in the &lt;span class="signedby"&gt;&lt;/span&gt; class, it will be automatically sign using a script font with the notation that the contract was electronically signed.
			<br>
			<br>
			<strong>[signedbydate]</strong><br><br>
			<strong>[monthly]</strong><br>
			This shortcode will display a monthly payment option that will automatically divide your total payment by the payment schedule you set up.
			<br>
			<br>
			<strong>[contractcurrency]</strong><br>
			This displays the currency symbol that you set up on the left.
			<br>
			<br>
			<strong>[businessname]</strong><br><br>
			<strong>[remainingbalance]</strong><br>
			This shortcode displays the remaining balance and how it should be paid if you are using the monthly option. It will tell your client what they need to pay now and how they should pay it over your schedule.
		</div>
	</div>';
	$html .= ' 
	<div id="onlinecontract-settings">
		 <input type="hidden" name="action" value="update" />
		 <input type="hidden" name="page_options" value="" />
		<div style="float:right;">Version '.onlinecontract_version_num.'</div>';
		if ($file!= '' || !isset($_GET['contract'])) {
		$html .= ' <input type="button" name="btneditcontracts" id="btneditcontracts" value="Save Changes" class="button-primary" />';
		}
	$html .= '</div>
	<script>
	var $ = jQuery;
	$(document).ready(function(){     
			$("#btneditcontracts").click(function(e){ 
				var dataString = "";
				var content = tinyMCE.activeEditor.getContent();
				var title = $("#onlinecontract_contractname").val();
				dataString = "title=" + title + "&contract=" + encodeURIComponent(content);
	 
				$.ajax({
					type: "POST",
					url: "'.plugins_url().'/onlinecontract/editcontract.php",
					data: dataString,
					success: function() {
						window.location.reload(true);
					}
				});
				return false;
			console.log(datastring);
			});
	});
	</script>';
	echo $html;
	echo '</form>';
}


function convertfractiontodecimal($input) {
	$fraction = array('whole' => 0);
	preg_match('/^((?P<whole>\d+)(?=\s))?(\s*)?(?P<numerator>\d+)\/(?P<denominator>\d+)$/', $input, $fraction);
	$result = $fraction['whole'] + $fraction['numerator']/$fraction['denominator'];
	return $result;

}

function monthlymaintenance($schedule) {
	$schedule = explode('/',$schedule);
	$months = $schedule[1];
	return $months;

}

if(isset($_POST['btnupdatecontracts'])){
	$newcontract  = $_FILES['userfile']['name'];
	$upload_dir = wp_upload_dir();
	if ($_FILES['userfile']['type'] == 'text/plain'){
		$title = clean($newcontract);
		$title = strtolower(str_replace('txt','',$title)).'.txt';
		$file = $upload_dir['basedir'] . '/onlinecontract/contracts/'.$title;
		
		$success = move_uploaded_file($_FILES['userfile']['tmp_name'],$file);
	}
	if(isset($_POST['file'])) {
		foreach ($_POST['file'] as $contractname) {
			unlink($contractname);
		}
	}
}

function contract_add_shortcode() {
$html = '
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.9.1.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>';
$html .= wp_register_style('contract_datepickercss', '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css');
$html .= wp_enqueue_style('contract_datepickercss');

$html .= wp_register_script('contract_datepickerjs', '//code.jquery.com/ui/1.10.4/jquery-ui.js', array('jquery'));
$html .= wp_enqueue_script('contract_datepickerjs');
$html .= '<script>
$(function() {
	$( "#duedate" ).datepicker();
});
</script>';
if ($_SESSION['onlinecontractsentmessage'] != '') {
$html .= '<div class="onlinecontractsentmessage"><p>'.$_SESSION['onlinecontractsentmessage'].'</p></div>';
$_SESSION['onlinecontractsentmessage'] = '';
}
$html .= '<form name="new_post" method="post" action="'.get_the_permalink().'" class="onlinecontractusersubmit">
	<p><label>Your Name *</label><input type="text" name="clientname" required/></p>
	<p><label>Your Mailing Address *</label><textarea name="clientmailingaddress" required/></textarea></p>
	<p><label>Your Email *</label><input type="text" name="clientemailaddress" required/></p>
	<p><label>Project Title (or Company Name) *</label><input type="text" name="projecttitle" required/></p>
	<p><label>Preferred Due Date *</label><input type="text" name="duedate" id="duedate" size="30" required>
	<p><label>Preferred Payment Schedule *</label>
	<select name="paymentschedule" required>';
		$contractlengths = explode(',',get_option('onlinecontract_contractterms'));
		foreach ($contractlengths as $value) {
			$html .= '<option value="'.$value.'">'.$value.'</option>';
		}
	$html .= '</select>
	<button id="submit" type="submit"/>Send</button>					
	<input type="hidden" name="action" value="post" />
	<input type="hidden" name="empty-description" id="empty-description" value="1"/>';
	wp_nonce_field( 'new-post' );
$html .= '</form>';
return $html;
}
add_shortcode( 'clientadd', 'contract_add_shortcode' );

function onlinecontract_clientadd(){
	if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == 'post' ){
		if ( !is_user_logged_in() )
			return;
		global $current_user;
 
		$user_id = $current_user->ID;
		if ($_POST['clientname']!='' && $_POST['clientemailaddress']!='' && $_POST['projecttitle']!='' && $_POST['duedate']!='') {
			$clientname = sanitize_text_field($_POST['clientname']);
			$clientmailingaddress = sanitize_text_field($_POST['clientmailingaddress']);
			$clientemailaddress	= sanitize_text_field($_POST['clientemailaddress']);
			$projectname = sanitize_text_field($_POST['projecttitle']);
			$paymentschedule = sanitize_text_field($_POST['paymentschedule']);
			$duedate = sanitize_text_field($_POST['duedate']);
	 
			$post_id = wp_insert_post( array(
				'post_author'	=> $user_id,
				'post_title'	=> $projectname,
				'post_content' => '[contract clientname="'.$clientname.'" clientmailingaddress="'.$clientmailingaddress.'" projectname="'.$projectname.'" duedate="'.$duedate.'" contractamount="" paymentschedule="'.$paymentschedule.'" contracttype="null" hourlyrate="" signedby="" signedbydate="" clientid="" guardian="" paypalamount="" sendnotification="false" clientemailaddress="'.$clientemailaddress.'" ]',
				'post_type'     => 'contract',
				'post_status'	=> 'pending'
				));			
			update_post_meta ($post_id, 'contractdetails_duedate', $duedate);
			update_post_meta ($post_id, 'contractdetails_clientname', $clientname);
			update_post_meta ($post_id, 'contractdetails_mailingaddress', $clientmailingaddress);
			update_post_meta ($post_id, 'contractdetails_emailaddress', $clientemailaddress);
			update_post_meta ($post_id, 'contractdetails_projectname', $projectname);
			update_post_meta ($post_id, 'contractdetails_paymentschedule', $paymentschedule);
			$message = 'You have a new client generated contract.  Please login to your administration to view and approve this contract.';
			$message .= "<br>";
			$message .= 'Client Name: '.$clientname;
			$message .= "<br>";
			$message .= 'Client Mailing Address: '.$clientmailingaddress;
			$message .= "<br>";
			$message .= 'Client Email Address: '.$clientemailaddress;
			$message .= "<br>";
			$message .= 'Project or Company Name: '.$projectname;
			$message .= "<br>";
			$message .= 'Preferred Completion Date: '.$duedate;
			$message .= "<br>";
			$message .= 'Preferred Payment Schedule: '.$paymentschedule;
			$message .= "<br>";
			$headers = "From: Online Contract <".get_option('admin_email').">"."\r\n";
			$headers .= "MIME-Version: 1.0\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\n";
			mail(get_option('admin_email'), 'A Client Generated Contract Is Pending', $message, $headers);
			$_SESSION['onlinecontractsentmessage'] = 'Your contract was successfully sent.  Once approved, you will be able to view and sign your contract online.';
		}else{
			$_SESSION['onlinecontractsentmessage'] = 'All fields are required';
		}
	}
}
 
add_action('init','onlinecontract_clientadd');

function clean($input) {
	$title = stripslashes($input);
	$title = preg_replace("/[^a-zA-Z0-9\s]/", "", $title);
	$title = str_replace(' ', '-', $title);
	return $title;
}

function convertcontractshortcode($contractinput, $atts) {
		$contract = $contractinput;
		if (strpos($contractinput,'[clientname]') !== false) {
			$contract = str_replace('[clientname]',$atts['clientname'],$contract);
		}
		if (strpos($contractinput,'[clientemailaddress]') !== false) {
			$contract = str_replace('[clientemailaddress]',$atts['clientemailaddress'],$contract);
		}
		if (strpos($contractinput,'[clientmailingaddress]') !== false) {
			$contract = str_replace('[clientmailingaddress]',$atts['clientmailingaddress'],$contract);
		}
		if (strpos($contractinput,'[projectname]') !== false) {
			$contract = str_replace('[projectname]',$atts['projectname'],$contract);
		}
		if (strpos($contractinput,'[contractamount]') !== false) {
			$contractamount = str_replace(',','',$atts['contractamount']);
			$contractamount = preg_replace('@[^0-9\.]+@i', '', $contractamount);
			$contract = str_replace('[contractamount]',number_format($contractamount, 2),$contract);
		}
		if (strpos($contractinput,'[hourlyrate]') !== false) {
			$hourlyrate = str_replace(',','',$atts['hourlyrate']);
			$hourlyrate = preg_replace('@[^0-9\.]+@i', '', $hourlyrate);
			$contract = str_replace('[hourlyrate]',$hourlyrate,$contract);
		}
		if (strpos($contractinput,'[duedate]') !== false) {
			$contract = str_replace('[duedate]',$atts['duedate'],$contract);
		}
		if (strpos($contractinput,'[paymentschedule]') !== false) {
			$contract = str_replace('[paymentschedule]',$atts['paymentschedule'],$contract);
		}
		if (strpos($contractinput,'[signedby]') !== false) {
			$contract = str_replace('[signedby]',$atts['signedby'],$contract);
		}
        if (strpos($contractinput,'[signedbydate]') !== false) {
            $contract = str_replace('[signedbydate]',$atts['signedbydate'],$contract);
        }
        if (strpos($contractinput,'[clientid]') !== false) {
            $contract = str_replace('[clientid]',$atts['clientid'],$contract);
        }
        if (strpos($contractinput,'[guardian]') !== false) {
            $contract = str_replace('[guardian]',$atts['guardian'],$contract);
        }
		if (strpos($contractinput,'[monthly]') !== false) {
			$contractmonthly = monthlymaintenance($atts['paymentschedule']);
			$contract = str_replace('[monthly]',$contractmonthly,$contract);
		}
		if (strpos($contractinput,'[contractcurrency]') !== false) {
			$contractcurrency = get_option('onlinecontract_contractcurrency');
			$contract = str_replace('[contractcurrency]',$contractcurrency,$contract);
		}
		if (strpos($contractinput,'[businessname]') !== false) {
			$businessname = get_option('onlinecontract_businessname');
			$contract = str_replace('[businessname]',$businessname,$contract);
		}
		if (strpos($contractinput,'[remainingbalance]') !== false) {
			if(!is_numeric($atts['paymentschedule'])) {
				$decimal = convertfractiontodecimal($atts['paymentschedule']); 
			}else{ 
				$decimal = $atts['paymentschedule'];
			}
			$contractamount = str_replace(',','',$atts['contractamount']);
			$contractamount = preg_replace('@[^0-9\.]+@i', '', $contractamount);
			$remainingbalance = number_format($contractamount * $decimal, 2);
			$contract = str_replace('[remainingbalance]',$remainingbalance,$contract);
		}
	return $contract;
}
//add_action( 'admin_menu', 'my_remove_meta_boxes' );

add_filter('plugin_action_links', 'onlinecontract_plugin_action_links', 10, 2);

function onlinecontract_plugin_action_links($links, $file) {
    if ($file == 'onlinecontract/onlinecontract.php') {
        // The "page" query string value must be equal to the slug
        // of the Settings admin page we defined earlier, which in
        // this case equals "myplugin-settings".
        $settings_link = '<a href="' . get_bloginfo('wpurl') . '/wp-admin/edit.php?post_type=contract&page=contract_options">Settings</a>';
        array_unshift($links, $settings_link);
    }

    return $links;
}

add_action( 'wp_dashboard_setup', 'onlinecontract_dashboard_setup' );
function onlinecontract_dashboard_setup() {
    wp_add_dashboard_widget(
        'onlinecontract-dashboard-widget',
        'Online Contracts Summary',
        'onlinecontract_dashboard_content',
        $control_callback = null
    );
}

function onlinecontract_dashboard_content() {
			global $post;
			$query = new WP_Query( array( 'post_type'=>'contract', 'posts_per_page'=>10) );
			if ( $query->have_posts() ) {
				echo '<table>'; 
				while ( $query->have_posts() ) { 
				$query->the_post();
				echo '<tr><td style="width:85%;"><a href="'.get_edit_post_link().'">'.get_the_title().'</a></td>';
				$signed = 0;
				$upload_dir = wp_upload_dir();
				if ($handle = opendir($upload_dir['basedir'] . '/onlinecontract/signatures/')) {
								while (false !== ($entry = readdir($handle))) {
									if ($entry != "." && $entry != "..") {
										$imgnamearr = explode('_',$entry);
										if ($imgnamearr[0]==$post->ID) {
											$fullimgname = $entry;
											echo '<td style="width:15%; text-align:right;"><span style="color: white; background-color: green; border-radius: 3px; padding: 2px 5px; font-size:10px;">Signed</span><td>';
											$signed = 1;
											break;
										}
									}
									
								}
								if ($signed!=1) {
									echo '<td style="width:15%; text-align:right;"><span style="color: white; background-color: red; border-radius: 3px; padding: 2px 5px; font-size:10px;">Unsigned</span></td>';
								}
								closedir($handle);
						   }
				echo '</tr>';
				}
				echo '</table>';
				echo '<div style="text-align:center;margin:20px 0;"><a href="edit.php?post_type=contract">View All Contracts</a></div>';
			}else{
				echo '<div style="text-align:center;margin:20px 0;">There are no contracts available.</div>';
			}
			
}

add_action( 'dashboard_glance_items', 'cpad_at_glance_content_table_end' );
function cpad_at_glance_content_table_end() {
    $args = array(
        'public' => true,
        '_builtin' => false
    );
    $output = 'object';
    $operator = 'and';
    
    $post_types = get_post_types( $args, $output, $operator );
    foreach ( $post_types as $post_type ) {
        $num_posts = wp_count_posts( $post_type->name );
        $num = number_format_i18n( $num_posts->publish );
        $text = _n( $post_type->labels->singular_name, $post_type->labels->name, intval( $num_posts->publish ) );
        if ( current_user_can( 'edit_posts' ) ) {
			if($text == 'Contract' || $text == 'Contracts' || $text == 'Online Contract') {
			if ($num > 1) {
				$text = 'Online Contracts';
			}
            $output = '<a href="edit.php?post_type=' . $post_type->name . '">' . $num . ' ' . $text . '</a>';
            echo '<li class="post-count ' . $post_type->name . '-count">' . $output . '</li>';
			}
        }
    }
}
 
// Add Some CSS to At a Glance Widget
function cpad_at_glance_icons() {
	$contracticonurl = plugins_url();
    echo '<style type="text/css">
        .contract-count a:before {
		content: "\f464" !important;
		}
        </style>';
}
add_action('admin_head', 'cpad_at_glance_icons');

// Admin menu icon styles
function add_menu_icons_styles_contract(){
// Icon from http://melchoyce.github.io/dashicons/	
	$contracticonurl = plugins_url();
?>
 
<style>
#adminmenu .menu-icon-contract div.wp-menu-image:before {
  content: url('<?php echo $contracticonurl; ?>/onlinecontract/includes/img/contract-icon.png');
  color:#f0be45;
}
#adminmenu .menu-icon-contract.wp-menu-open div.wp-menu-image:before {
  content: url('<?php echo $contracticonurl; ?>/onlinecontract/includes/img/contract-icon-selected.png');
  color:#f0be45;
}
#onlinecontract-settings, #wp-editcontract-wrap {
	float:left;
	border: 1px solid #e5e5e5;
	-webkit-box-shadow: 0 1px 1px rgba(0,0,0,.04);
	box-shadow: 0 1px 1px rgba(0,0,0,.04);
	background: #fff;
	width: 58%;
	padding: 1%;
	margin-top:20px;
}
#onlinecontract-infotext {
	float: right;
	width: 35%;
	margin-right:1%;
	border: 1px solid #e5e5e5;
	-webkit-box-shadow: 0 1px 1px rgba(0,0,0,.04);
	box-shadow: 0 1px 1px rgba(0,0,0,.04);
	background: #fff;
	padding: 1%;
	margin-top:20px;
}
@media screen and (max-width:1024px){
	.form-table th { width:100% !important; }
	#onlinecontract-infotext img { width:80% !important; margin:0 auto; display:block; }
	#onlinecontract-settings, #wp-editcontract-wrap {
		float:none;
		width: 94%;
		padding: 2%;
		margin-bottom:20px;
	}
	#onlinecontract-infotext  {
		float: none;
		padding: 2%;
		width: 94%;
		margin-top:30px;
	}
}
@media screen and (max-width:600px) {
	#onlinecontract-settings .button-primary { display:none; }
	#onlinecontract-settings td div { float:none !important; width:100% !important; }
}
</style>
 
<?php
}
add_action( 'admin_head', 'add_menu_icons_styles_contract' );
