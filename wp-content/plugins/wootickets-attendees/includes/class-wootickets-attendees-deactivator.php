<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://echo5webdesign.com
 * @since      1.0.0
 *
 * @package    Wootickets_Attendees
 * @subpackage Wootickets_Attendees/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wootickets_Attendees
 * @subpackage Wootickets_Attendees/includes
 * @author     Joshua Flowers <joshua@echo5webdesign.com>
 */
class Wootickets_Attendees_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
