<?php

/**
 * Provide a dashboard view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://echo5webdesign.com
 * @since      1.0.0
 *
 * @package    Wootickets_Attendees
 * @subpackage Wootickets_Attendees/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
