<?php

/**
 * The dashboard-specific functionality of the plugin.
 *
 * @link       http://echo5webdesign.com
 * @since      1.0.0
 *
 * @package    Wootickets_Attendees
 * @subpackage Wootickets_Attendees/admin
 */

/**
 * The dashboard-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    Wootickets_Attendees
 * @subpackage Wootickets_Attendees/admin
 * @author     Joshua Flowers <joshua@echo5webdesign.com>
 */
class Wootickets_Attendees_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $wootickets_attendees    The ID of this plugin.
	 */
	private $wootickets_attendees;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $wootickets_attendees       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $wootickets_attendees, $version ) {

		$this->wootickets_attendees = $wootickets_attendees;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the Dashboard.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wootickets_Attendees_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wootickets_Attendees_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->wootickets_attendees, plugin_dir_url( __FILE__ ) . 'css/wootickets-attendees-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the dashboard.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wootickets_Attendees_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wootickets_Attendees_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->wootickets_attendees, plugin_dir_url( __FILE__ ) . 'js/wootickets-attendees-admin.js', array( 'jquery' ), $this->version, false );

	}

}
