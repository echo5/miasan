<?php
namespace Wootickets_Attendees_Public;
use Wootickets_Attendees_Public as Wootickets_Attendees_Public;

    /**
     * Add the field to the checkout
     **/
    function attendee_details( $checkout ) {

        // force toggle scripts to load
        echo do_shortcode('<div style="display:none;">[vc_row][vc_column width="1/1"][vc_toggle title="Toggle title" style="default" color="Default" size="md" open="false"]Toggle content goes here, click edit button to change this text.[/vc_toggle][/vc_column][/vc_row]</div>' );

        $attendee_count = $this->count_attendees();
        if($attendee_count > 0) {
            echo "</div></div>"; //close the Billing Address section to create new group of fields
            //echo do_shortcode('[contract clientname="Testy Testerson" projectname="Climbing Course"]' );

            // @TODO @ECHO5 This will need to loop over an array of ticket products if multiple events are in cart
            // @TODO @ECHO5 Use function wt_get_events
            // $events = wt_get_events();
            // foreach ($events as $event) {
            //     echo $event['title'];
            //     echo $event['duedate'];
            // }
            echo "<div id='attendee_details'><div>"; //automatically be closed from 2 Billing Address's div - </div></div>
            echo '<h3>'.__('Event Attendees').'</h3>';

            echo 'Event Details:';
            $events = $this->get_events();
            foreach ($events as $event) {
                echo $event['title'];
                echo $event['startdate'];
            }

            for($n = 1; $n <= $attendee_count; $n++ ) {
                //echo '<div id="attende_details_'.$n.'">';
                // Open toggle if first
                if($n == 1) {
                    $toggle_title_class = 'wpb_toggle_title_active';
                    $toggle_content_class = 'wpb_toggle_open';
                }
                else {
                    $toggle_title_class = '';
                    $toggle_content_class = '';                    
                }
                echo '<h4 class="wpb_toggle '.$toggle_title_class.'">'.__('Attendee').' '.$n.'</h4>';
                echo '<div class="wpb_toggle_content '.$toggle_content_class.'">';
                woocommerce_form_field( 'attendee_chinese_name_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row form-row-first'),
                    'label'         => __('Chinese Name'),
                    'placeholder'   => __(''),
                    'required'      => true,
                ), $checkout->get_value( 'attendee_chinese_name_'.$n ));
                woocommerce_form_field( 'attendee_english_name_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row'),
                    'label'         => __('English Name'),
                    'placeholder'   => __(''),
                    'required'      => true,
                ), $checkout->get_value( 'attendee_english_name_'.$n ));
                woocommerce_form_field( 'attendee_email_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row validate-email'),
                    'label'         => __('Email'),
                    'placeholder'       => __(''),
                    'required'      => true,
                ), $checkout->get_value( 'attendee_email_'.$n ));
                woocommerce_form_field( 'attendee_phone_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row validate-phone'),
                    'label'         => __('Phone'),
                    'placeholder'   => __(''),
                    'required'      => true,
                ), $checkout->get_value( 'attendee_phone_'.$n ));
                woocommerce_form_field( 'attendee_dob_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row datepicker'),
                    'label'         => __('Date of Birth'),
                    'placeholder'   => __(''),
                    'required'      => true,
                ), $checkout->get_value( 'attendee_dob_'.$n ));
                woocommerce_form_field( 'attendee_gender_'.$n, array(
                    'type'          => 'select',
                    'class'         => array('form-row form-row-last'),
                    'label'         => __('Gender'),
                    'placeholder'   => __(''),
                    'required'      => true,
                    'options' => array(
                              'male' => __('Male'),
                              'female'=> __('Female')
                    ),
                ), $checkout->get_value( 'attendee_gender_'.$n ));
                woocommerce_form_field( 'attendee_id_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row'),
                    'label'         => __('ID Number'),
                    'placeholder'   => __(''),
                    'required'      => true,
                ), $checkout->get_value( 'attendee_id_'.$n ));
                woocommerce_form_field( 'attendee_emergency_name_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row'),
                    'label'         => __('Emergency Contact Name'),
                    'placeholder'   => __(''),
                    'required'      => true,
                ), $checkout->get_value( 'attendee_emergency_name_'.$n ));
                woocommerce_form_field( 'attendee_emergency_phone_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row validate-phone'),
                    'label'         => __('Emergency Contact Phone'),
                    'placeholder'   => __(''),
                    'required'      => true,
                ), $checkout->get_value( 'attendee_emergency_phone_'.$n ));
                echo "<div class='clear'></div><h6>Medical Conditions</h6>";
                woocommerce_form_field( 'attendee_medical_conditions_heart_disease_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Heart disease'),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $checkout->get_value( 'attendee_medical_conditions_heart_disease_'.$n ));
                woocommerce_form_field( 'attendee_medical_conditions_hypertension_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Hypertension'),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $checkout->get_value( 'attendee_medical_conditions_hypertension_'.$n ));
                woocommerce_form_field( 'attendee_medical_conditions_diabetes_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Diabetes'),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $checkout->get_value( 'attendee_medical_conditions_diabetes_'.$n ));
                woocommerce_form_field( 'attendee_medical_conditions_vascular_disease_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Vascular Disease'),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $checkout->get_value( 'attendee_medical_conditions_vascular_disease_'.$n ));
                woocommerce_form_field( 'attendee_medical_conditions_asthma_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Asthma'),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $checkout->get_value( 'attendee_medical_conditions_asthma_'.$n ));
                woocommerce_form_field( 'attendee_medical_conditions_strenuous_exercise_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Unable to do strenuous exercise'),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $checkout->get_value( 'attendee_medical_conditions_strenuous_exercise_'.$n ));      
                woocommerce_form_field( 'attendee_medical_conditions_physical_disease_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('History of physical disease'),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $checkout->get_value( 'attendee_medical_conditions_physical_disease_'.$n ));
                woocommerce_form_field( 'attendee_medical_conditions_drug_allergy_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Drug Allergy'),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $checkout->get_value( 'attendee_medical_conditions_drug_allergy_'.$n ));
                woocommerce_form_field( 'attendee_medical_conditions_medication_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Medication'),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $checkout->get_value( 'attendee_medical_conditions_medication_'.$n ));
                woocommerce_form_field( 'attendee_medical_conditions_major_surgery_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Major Surgery'),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $checkout->get_value( 'attendee_medical_conditions_major_surgery_'.$n ));
                woocommerce_form_field( 'attendee_medical_conditions_fractures_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Limb Fractures'),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $checkout->get_value( 'attendee_medical_conditions_fractures_'.$n ));
                woocommerce_form_field( 'attendee_medical_conditions_none_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('None of the above'),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $checkout->get_value( 'attendee_medical_conditions_none_'.$n ));
                woocommerce_form_field( 'attendee_medical_other_'.$n, array(
                    'type'          => 'textarea',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Other Medical Conditions'),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $checkout->get_value( 'attendee_medical_other_'.$n ));                
                woocommerce_form_field( 'attendee_medical_drugs_'.$n, array(
                    'type'          => 'textarea',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Medication Details or Drug Allergies'),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $checkout->get_value( 'attendee_medical_drugs_'.$n ));                
                woocommerce_form_field( 'attendee_medical_surgery_'.$n, array(
                    'type'          => 'textarea',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('History of Surgery or Fractures'),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $checkout->get_value( 'attendee_medical_surgery_'.$n ));

                echo '<a class="next-attendee vc_btn vc_btn_sm vc_btn_square" href="#" title="" target="" style="color: rgb(255, 255, 255); -webkit-transition: padding 0.3s ease 0s, min-width 0.3s ease 0s, opacity 0.3s ease 0s; transition: padding 0.3s ease 0s, min-width 0.3s ease 0s, opacity 0.3s ease 0s; min-height: 0px; min-width: 170px; line-height: 20px; border-width: 0px; margin: 0px; padding: 15px 24px 13px; letter-spacing: 0px; font-size: 13px; background-color: rgb(87, 194, 126);">
                <span class="button_text" style="-webkit-transition: all 0s ease 0s; transition: all 0s ease 0s; min-height: 0px; min-width: 0px; line-height: 20px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-size: 13px;">Next</span>    </a>';

                echo '</div>';

            }
            echo "<style type='text/css'>
                        #attendee_details .form-row {
                            float: left;
                            margin-right: 2%;
                            width: 31%;
                        }
                        #attendee_details .form-row-full {
                            float: left;
                            margin-right: 0%;
                            width: 100%;
                        }
                        #attendee_details .form-row-last {
                            margin-right: 0;
                        }
                        {
                            font-size: 90%;
                        }

                        .ui-widget-header {
                            border:0px;
                            background: #fff;
                        }
                        .ui-datepicker .ui-datepicker-prev, .ui-datepicker .ui-datepicker-next {
                            display:none;
                        }
                        .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
                            border: 1px solid transparent;
                            background: #fff;
                        }
                    </style>";
            echo '<script>
                        jQuery(function($) {
                            jQuery( ".datepicker input" ).datepicker({
                                changeMonth: true,
                                changeYear: true,
                                yearRange: "-70:-13", 
                            });

                            jQuery(".next-attendee").click(function(e) {
                                e.preventDefault();
                                var nextToggle = $(this).parent().next();
                                $(this).parent().prev().click();  
                                if(!nextToggle.hasClass("wpb_toggle_title_active")) {
                                    nextToggle.click();
                                }  
                                setTimeout(
                                  function() 
                                  {
                                    $("html, body").animate({
                                        scrollTop: nextToggle.offset().top - 100
                                    }, 1000);
                                  }, 500);

                            });
                        });
                    </script>';
        }
        //echo "Attendees: " . $attendee_count;
    }

    /**
     * Return number of attendees based on tickets
     **/        
    function count_attendees() {
        
        global $woocommerce;
        $attendee_count = 0;
        
        if (sizeof($woocommerce->cart->get_cart())>0) :
            foreach ($woocommerce->cart->get_cart() as $item_id => $values) :
                $_product = $values['data'];
                if ($_product->exists() && $values['quantity']>0) :
                    if (!empty(get_post_meta($_product->id, '_tribe_wooticket_for_event')))
                        $attendee_count += $values['quantity'];
                endif;
            endforeach;
        endif;
        
        return $attendee_count;
        
    }

    function get_events() {
        
        global $woocommerce;
        $events = array();
        
        if (sizeof($woocommerce->cart->get_cart())>0) :
            foreach ($woocommerce->cart->get_cart() as $item_id => $values) :
                $_product = $values['data'];
                if ($_product->exists() && $values['quantity']>0) :
                    if (!empty($event_id = get_post_meta($_product->id, '_tribe_wooticket_for_event', true))) {
                        $events[$_product->id]['title'] = get_the_title($_product->id);
                        $events[$_product->id]['startdate'] = get_post_meta($event_id, '_EventStartDate', true);
                    }
                endif;
            endforeach;
        endif;
        
        return $events;
        
    }