<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://echo5webdesign.com
 * @since      1.0.0
 *
 * @package    Wootickets_Attendees
 * @subpackage Wootickets_Attendees/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    Wootickets_Attendees
 * @subpackage Wootickets_Attendees/public
 * @author     Joshua Flowers <joshua@echo5webdesign.com>
 */
class Wootickets_Attendees_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $wootickets_attendees    The ID of this plugin.
	 */
	private $wootickets_attendees;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $wootickets_attendees       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $wootickets_attendees, $version ) {

		$this->wootickets_attendees = $wootickets_attendees;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wootickets_Attendees_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wootickets_Attendees_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

        if(is_checkout()) {
            wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
    		wp_enqueue_style( $this->wootickets_attendees, plugin_dir_url( __FILE__ ) . 'css/wootickets-attendees-public.css', array(), $this->version, 'all' );
        }
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wootickets_Attendees_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wootickets_Attendees_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
        $settings = [];
        $ajax_vars = array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'isAuthorizedUser' => get_current_user_id(),
            'nextButton' => __( 'Next', $this->wootickets_attendees ),
            'previousButton' => __( 'Previous', $this->wootickets_attendees ),
            'finishButton' => __( 'Finish', $this->wootickets_attendees ),
            // 'noAccountButton' => $settings['noAccountButtonName'],
            // 'titleTemplate' => $this->getTabsTitleTemplate(),
            // 'termsLocation' => $settings['termsAndConditionsLocation']
        );
        $attendee_ajax_vars = array( 
            'ajaxurl' => admin_url( 'admin-ajax.php'),
            'security' => wp_create_nonce( "attendee-nonce" ),
        );

        if(is_checkout()) {
            wp_enqueue_script('jquery-ui-datepicker');
            wp_enqueue_script('jquery-steps', plugin_dir_url( __FILE__ ) . 'js/jquery.steps.min.js', array( 'jquery' ), $this->version, false );
            wp_enqueue_script('collect-wizard', plugin_dir_url( __FILE__ ) . 'js/collect_wizard.js', array( 'jquery' ), $this->version, false );
    		wp_enqueue_script( $this->wootickets_attendees, plugin_dir_url( __FILE__ ) . 'js/wootickets-attendees-public.js', array( 'jquery' ), $this->version, true );
            wp_localize_script( $this->wootickets_attendees, 'attendeeAjax', $attendee_ajax_vars);
            wp_localize_script( 'collect-wizard', 'fesiCheckoutSteps', $ajax_vars);
        }
	}

    /**
     * Add the field to the checkout
     **/
    public function show_steps() {

        $steps = array();
        $events = $this->get_events();

        if(!get_current_user_id() && false) {
            $steps['login'] = array(
                        'name' => __('Login', $this->wootickets_attendees),
                        'class' => 'festi-wizard-step-login'
                    );
        }
        $steps['billing'] = array(
                    'name' => __('Billing', $this->wootickets_attendees),
                    'class' => 'festi-wizard-step-billing'
                );
        $steps['shipping'] = array(
                    'name' => __('Shipping', $this->wootickets_attendees),
                    'class' => 'festi-wizard-step-shipping'
                );
        if(!empty($events)) {
        $steps['attendees'] = array(
                    'name' => __('Attendees', $this->wootickets_attendees),
                    'class' => 'festi-wizard-step-attendees',
                );
        $steps['attendee_contracts'] = array(
                    'name' => __('Consent', $this->wootickets_attendees),
                    'class' => 'festi-wizard-step-contracts',
                );
        }
        $steps['reviewOrder'] = array(
                    'name' => __('Payment', $this->wootickets_attendees),
                    'class' => 'festi-wizard-step-view-order',
                );
        // $steps['payment'] = array(
        //             'name' => __('Payment', $this->wootickets_attendees),
        //             'class' => 'festi-wizard-step-payment',
        //         );

        $css_class = (!empty($events) ? 'wizard-events' : 'wizard-no-events');
        echo '<div data-steps-count="" style="display: none;" id="festi-checkout-steps-wizard" class="wizard '.$css_class.'">';
            foreach ($steps as $step) {
                echo '<h1>'.$step["name"].'</h1>';
                echo '<div class="'.$step["class"].'"></div>';
            }
        echo '</div>';
    }

    /**
     * Add the field to the checkout
     **/
    public function show_attendees( $checkout ) {

        // force toggle scripts to load
        echo do_shortcode('<div style="display:none;">[vc_row][vc_column width="1/1"][vc_toggle title="Toggle title" style="default" color="Default" size="md" open="false"]Toggle content goes here, click edit button to change this text.[/vc_toggle][/vc_column][/vc_row]</div>' );

        $attendee_count = $this->count_attendees();
        if($attendee_count > 0) {
            echo "</div></div>"; //close the Billing Address section to create new group of fields
            //echo do_shortcode('[contract clientname="Testy Testerson" projectname="Climbing Course"]' );

            // @TODO @ECHO5 This will need to loop over an array of ticket products if multiple events are in cart
            // @TODO @ECHO5 Use function wt_get_events
            // $events = wt_get_events();
            // foreach ($events as $event) {
            //     echo $event['title'];
            //     echo $event['duedate'];
            // }
            echo "<div id='attendee_details'><div>"; //automatically be closed from 2 Billing Address's div - </div></div>
            echo '<h3>'.__('Event Attendees', $this->wootickets_attendees).'</h3>';

            // Get attendees from last order
            $previous_attendees = $this->get_previous_attendees();
            $latest = max(array_keys($previous_attendees));

            for($n = 1; $n <= $attendee_count; $n++ ) {
                //echo '<div id="attende_details_'.$n.'">';
                // Open toggle if first
                if($n == 1) {
                    $toggle_title_class = 'wpb_toggle_title_active';
                    $toggle_content_class = 'wpb_toggle_open';
                }
                else {
                    $toggle_title_class = '';
                    $toggle_content_class = '';                    
                }
                echo '<h4 class="wpb_toggle '.$toggle_title_class.'">'.__('Attendee', $this->wootickets_attendees).' '.$n.'</h4>';
                echo '<div class="wpb_toggle_content '.$toggle_content_class.'">';
                woocommerce_form_field( 'attendee_chinese_name_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row form-row-first'),
                    'label'         => __('Chinese Name', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => true,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Chinese Name']);
                woocommerce_form_field( 'attendee_english_name_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row'),
                    'label'         => __('English Name', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => true,
                ), $previous_attendees[$latest][$n][$n . ' Attendee English Name']);
                woocommerce_form_field( 'attendee_id_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row'),
                    'label'         => __('ID Number', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => true,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Id']);
                woocommerce_form_field( 'attendee_email_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row validate-email'),
                    'label'         => __('Email', $this->wootickets_attendees),
                    'placeholder'       => __(''),
                    'required'      => true,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Email']);
                woocommerce_form_field( 'attendee_phone_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row validate-phone'),
                    'label'         => __('Phone', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => true,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Phone']);
                woocommerce_form_field( 'attendee_dob_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row datepicker'),
                    'label'         => __('Date of Birth', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => true,
                ), $previous_attendees[$latest][$n][$n . ' Attendee DOB']);


                woocommerce_form_field( 'attendee_post_code_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row form-row-address'),
                    'label'         => __('Post Code', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => true,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Post Code']);    
                woocommerce_form_field( 'attendee_city_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row form-row-address'),
                    'label'         => __('City', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => true,
                ), $previous_attendees[$latest][$n][$n . ' Attendee City']);    
                woocommerce_form_field( 'attendee_district_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row form-row-address'),
                    'label'         => __('District', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee District']);    
                woocommerce_form_field( 'attendee_street_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row'),
                    'label'         => __('Street', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => true,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Street']);    
                woocommerce_form_field( 'attendee_alley_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row form-row-address-small'),
                    'label'         => __('Alley', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Alley']);    
                echo '<div class="address-char"><label for="">&nbsp;</label>巷</div>';
                woocommerce_form_field( 'attendee_lane_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row form-row-address-small'),
                    'label'         => __('Lane', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Lane']);    
                echo '<div class="address-char"><label for="">&nbsp;</label>弄</div>';
                woocommerce_form_field( 'attendee_number_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row form-row-address-small'),
                    'label'         => __('Number', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => true,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Number']);    
                echo '<div class="address-char"><label for="">&nbsp;</label>號</div>';
                woocommerce_form_field( 'attendee_floor_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row form-row-address-small'),
                    'label'         => __('Floor', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Floor']);    
                echo '<div class="address-char"><label for="">&nbsp;</label>樓</div>';

                woocommerce_form_field( 'attendee_gender_'.$n, array(
                    'type'          => 'select',
                    'class'         => array('form-row'),
                    'label'         => __('Gender', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => true,
                    'options' => array(
                              'male' => __('Male', $this->wootickets_attendees),
                              'female'=> __('Female', $this->wootickets_attendees)
                    ),
                ), $previous_attendees[$latest][$n][$n . ' Attendee Gender']);
                woocommerce_form_field( 'attendee_emergency_name_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row'),
                    'label'         => __('Emergency Contact Name', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => true,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Emergency Name']);
                woocommerce_form_field( 'attendee_emergency_phone_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row validate-phone'),
                    'label'         => __('Emergency Contact Phone', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => true,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Emergency Phone']);
                woocommerce_form_field( 'attendee_guardian_'.$n, array(
                    'type'          => 'text',
                    'class'         => array('form-row form-row-guardian'),
                    'label'         => __('Guardian Name', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Guardian']);
                echo "<div class='clear'></div><h6>".__('Medical Conditions', $this->wootickets_attendees)."</h6>";
                woocommerce_form_field( 'attendee_medical_conditions_heart_disease_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row'),
                    'label'         => __('Heart disease', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Guardian']);
                woocommerce_form_field( 'attendee_medical_conditions_hypertension_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row'),
                    'label'         => __('Hypertension', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Hypertension']);
                woocommerce_form_field( 'attendee_medical_conditions_diabetes_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row'),
                    'label'         => __('Diabetes', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Diabetes']);
                woocommerce_form_field( 'attendee_medical_conditions_vascular_disease_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row'),
                    'label'         => __('Vascular Disease', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Vascular Disease']);
                woocommerce_form_field( 'attendee_medical_conditions_asthma_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row', $this->wootickets_attendees),
                    'label'         => __('Asthma', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Asthma']);
                woocommerce_form_field( 'attendee_medical_conditions_strenuous_exercise_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row'),
                    'label'         => __('Unable to do strenuous exercise', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Strenuous Exercise']);      
                woocommerce_form_field( 'attendee_medical_conditions_physical_disease_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row'),
                    'label'         => __('History of physical disease', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Physical Disease']);
                woocommerce_form_field( 'attendee_medical_conditions_drug_allergy_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row'),
                    'label'         => __('Drug Allergy', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Drug Allergy']);
                woocommerce_form_field( 'attendee_medical_conditions_medication_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row'),
                    'label'         => __('Medication', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Medication']);
                woocommerce_form_field( 'attendee_medical_conditions_major_surgery_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row'),
                    'label'         => __('Major Surgery', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Major Surgery']);
                woocommerce_form_field( 'attendee_medical_conditions_fractures_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row'),
                    'label'         => __('Limb Fractures', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Fractures']);
                woocommerce_form_field( 'attendee_medical_conditions_none_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row'),
                    'label'         => __('None of the above', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Medical (None)']);
                woocommerce_form_field( 'attendee_blood_type_'.$n, array(
                    'type'          => 'radio',
                    'class'         => array('form-row form-row-full form-row-radio'),
                    'label'         => __('Blood Type', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => true,
                    'options' => array(
                              'a' => 'A',
                              'b'=> 'B',
                              'o' => 'O',
                              'ab' => 'AB'
                    ),
                ), $previous_attendees[$latest][$n][$n . ' Attendee Blood Type']);
                woocommerce_form_field( 'attendee_medical_other_'.$n, array(
                    'type'          => 'textarea',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Other Medical Conditions', $this->wootickets_attendees),
                    'placeholder'   => __('Please type \'None\' if no medical conditions exist.', $this->wootickets_attendees),
                    'required'      => true,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Medical Other']);     
                woocommerce_form_field( 'attendee_medical_drugs_'.$n, array(
                    'type'          => 'textarea',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Medication Details or Drug Allergies', $this->wootickets_attendees),
                    'placeholder'   => __('Please type \'None\' if no drugs are taken or allergies exist.', $this->wootickets_attendees),
                    'required'      => true,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Drugs']);     
                woocommerce_form_field( 'attendee_medical_surgery_'.$n, array(
                    'type'          => 'textarea',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('History of Surgery or Fractures', $this->wootickets_attendees),
                    'placeholder'   => __('Please type \'None\' if no history of surgery or fractures exists.', $this->wootickets_attendees),
                    'required'      => true,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Surgery']);
                // woocommerce_form_field( 'attendee_equipment_'.$n, array(
                //     'type'          => 'radio',
                //     'class'         => array('form-row form-row-full form-row-checkbox form-row-list'),
                //     'label'         => __('Equipment', $this->wootickets_attendees),
                //     'placeholder'   => __(''),
                //     'required'      => false,
                //     'options' => array(
                //         'miasan_special_lightweight_backpack' => __('Miasan Special Lightweight Backpack', $this->wootickets_attendees),
                //         'bd_series_headlights' => __('BD Series Headlights',$this->wootickets_attendees),
                //         'alpenstock' => __('Alpenstock',$this->wootickets_attendees),
                //         'sleeping_bags' => __('Sleeping Bags',$this->wootickets_attendees),
                //         'foam_mattress' => __('Foam Mattress',$this->wootickets_attendees),
                //         'personal_helmet' => __('Personal Helmet',$this->wootickets_attendees),
                //         'climbing_suspenders' => __('Climbing Suspenders',$this->wootickets_attendees),
                //         'safety_shackles' => __('Safety Shackles (2)',$this->wootickets_attendees),
                //         'none' => __('None',$this->wootickets_attendees),
                //     ),
                // ), $previous_attendees[$latest][$n][$n . ' Attendee Equipment']);

                woocommerce_form_field( 'attendee_meal_preference_'.$n, array(
                    'type'          => 'select',
                    'class'         => array('form-row'),
                    'label'         => __('Meal Preference', $this->wootickets_attendees),
                    'options'       => array(
                                        'non-vegetarian'=> __('Non-vegetarian', $this->wootickets_attendees),
                                        'vegetarian'    => __('Vegetarian', $this->wootickets_attendees),
                                        ),
                    'required'      => true,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Meal Preference']);    

                echo "<div class='clear'></div><h6>".__('Equipment', $this->wootickets_attendees)."</h6>";

                woocommerce_form_field( 'attendee_equipment_miasan_special_lightweight_backpack_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Miasan Special Lightweight Backpack <a href="/shop/accessories/米亞桑特製輕量化背包-含鋁條70l/" target="_blank">(More Info)</a>', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Equipment Miasan Special Lightweight Backpack']);
                woocommerce_form_field( 'attendee_equipment_bd_series_headlights_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row form-row-full form-row-checkbox form-row-list'),
                    'label'         => __('BD Series Headlights', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Equipment BD Series Headlights']);

                woocommerce_form_field( 'attendee_equipment_alpenstock_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Alpenstock', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Equipment Alpenstock']);
                woocommerce_form_field( 'attendee_equipment_sleeping_bags_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Sleeping Bags', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Equipment Sleeping Bags']);
                woocommerce_form_field( 'attendee_equipment_foam_mattress_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Foam Mattress', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Equipment Foam Mattress']);
                woocommerce_form_field( 'attendee_equipment_personal_helmet_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Personal Helmet', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Equipment Personal Helmet']);
                woocommerce_form_field( 'attendee_equipment_climbing_suspenders_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Climbing Suspenders', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Equipment Climbing Suspenders']);
                woocommerce_form_field( 'attendee_equipment_safety_shackles_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('Safety Shackles (2)', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Equipment Safety Shackles']);
                woocommerce_form_field( 'attendee_equipment_none_'.$n, array(
                    'type'          => 'checkbox',
                    'class'         => array('form-row-full form-row-last'),
                    'label'         => __('None', $this->wootickets_attendees),
                    'placeholder'   => __(''),
                    'required'      => false,
                ), $previous_attendees[$latest][$n][$n . ' Attendee Equipment None']);

                if($attendee_count > 1 && $attendee_count != $n) {
                    echo '<a class="next-attendee vc_btn vc_btn_sm vc_btn_square" href="#" title="" target="" style="color: rgb(255, 255, 255); -webkit-transition: padding 0.3s ease 0s, min-width 0.3s ease 0s, opacity 0.3s ease 0s; transition: padding 0.3s ease 0s, min-width 0.3s ease 0s, opacity 0.3s ease 0s; min-height: 0px; min-width: 170px; line-height: 20px; border-width: 0px; padding: 15px 24px 13px; letter-spacing: 0px; font-size: 13px; background-color: rgb(87, 194, 126);">
                            <span class="button_text" style="-webkit-transition: all 0s ease 0s; transition: all 0s ease 0s; min-height: 0px; min-width: 0px; line-height: 20px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-size: 13px;">'.__("Next Attendee",$this->wootickets_attendees).'</span>    </a>';
                }

                echo '</div>';

            }

        }
    }

    /**
     * Return array of previous attendees to fill form
     **/
    public function get_previous_attendees() {

        //$attendee_count = $this->count_attendees();
        global $wpdb;

        // Get attendee info based on previous order ID
        $attendees = array();
        $user_id = get_current_user_id();
        $orders = $wpdb->get_results( "SELECT a.post_id AS post_id, a.meta_key as meta_key, a.meta_value as meta_value  FROM wp_postmeta AS a, wp_postmeta as b WHERE a.meta_key LIKE '% Attendee %' AND a.post_id = b.post_id AND b.meta_key = '_customer_user' AND b.meta_value = '{$user_id}' ORDER BY a.post_id ASC" );
        foreach($orders as $order) {
            $attendee_n = (substr($order->meta_key, 0, 1));
            $attendees[$order->post_id][$attendee_n][$order->meta_key] = $order->meta_value;
        }       
        return $attendees; 
    }

    /**
     * Add area for AJAX contracts to load
     **/
    public function show_contracts( $checkout ) {

        $attendee_count = $this->count_attendees();
        if($attendee_count > 0) {
            echo "</div></div>";
            echo "<div id='attendee_contracts'><div>"; 
        }
    }

    /**
     * Update the order meta with field value
     **/
    public function update_order_meta( $order_id ) {
        
        $events = $this->get_events();
        if(!empty($events)) {
            $attendee_count = $this->count_attendees();
            for($n = 1; $n <= $attendee_count; $n++ ) {
                //@TODO add all info
                if ($_POST['attendee_chinese_name_'.$n]) update_post_meta( $order_id, $n.' Attendee Chinese Name', esc_attr($_POST['attendee_chinese_name_'.$n]));
                if ($_POST['attendee_english_name_'.$n]) update_post_meta( $order_id, $n.' Attendee English Name', esc_attr($_POST['attendee_english_name_'.$n]));
                if ($_POST['attendee_email_'.$n]) update_post_meta( $order_id, $n.' Attendee Email', esc_attr($_POST['attendee_email_'.$n]));
                if ($_POST['attendee_phone_'.$n]) update_post_meta( $order_id, $n.' Attendee Phone', esc_attr($_POST['attendee_phone_'.$n]));
                if ($_POST['attendee_id_'.$n]) update_post_meta( $order_id, $n.' Attendee Id', esc_attr($_POST['attendee_id_'.$n]));
                if ($_POST['attendee_dob_'.$n]) update_post_meta( $order_id, $n.' Attendee DOB', esc_attr($_POST['attendee_dob_'.$n]));
                if ($_POST['attendee_post_code_'.$n]) update_post_meta( $order_id, $n.' Attendee Post Code', esc_attr($_POST['attendee_post_code_'.$n]));
                if ($_POST['attendee_city_'.$n]) update_post_meta( $order_id, $n.' Attendee City', esc_attr($_POST['attendee_city_'.$n]));
                if ($_POST['attendee_district_'.$n]) update_post_meta( $order_id, $n.' Attendee District', esc_attr($_POST['attendee_district_'.$n]));
                if ($_POST['attendee_street_'.$n]) update_post_meta( $order_id, $n.' Attendee Street', esc_attr($_POST['attendee_street_'.$n]));
                if ($_POST['attendee_alley_'.$n]) update_post_meta( $order_id, $n.' Attendee Alley', esc_attr($_POST['attendee_alley_'.$n]));
                if ($_POST['attendee_lane_'.$n]) update_post_meta( $order_id, $n.' Attendee Lane', esc_attr($_POST['attendee_lane_'.$n]));
                if ($_POST['attendee_number_'.$n]) update_post_meta( $order_id, $n.' Attendee Number', esc_attr($_POST['attendee_number_'.$n]));
                if ($_POST['attendee_floor_'.$n]) update_post_meta( $order_id, $n.' Attendee Floor', esc_attr($_POST['attendee_floor_'.$n]));
                if ($_POST['attendee_gender_'.$n]) update_post_meta( $order_id, $n.' Attendee Gender', esc_attr($_POST['attendee_gender_'.$n]));
                if ($_POST['attendee_emergency_name_'.$n]) update_post_meta( $order_id, $n.' Attendee Emergency Name', esc_attr($_POST['attendee_emergency_name_'.$n]));
                if ($_POST['attendee_emergency_phone_'.$n]) update_post_meta( $order_id, $n.' Attendee Emergency Phone', esc_attr($_POST['attendee_emergency_phone_'.$n]));
                if ($_POST['attendee_guardian_'.$n]) update_post_meta( $order_id, $n.' Attendee Guardian', esc_attr($_POST['attendee_guardian_'.$n]));
                if ($_POST['attendee_medical_conditions_heart_disease_'.$n]) update_post_meta( $order_id, $n.' Attendee Heart Disease', esc_attr($_POST['attendee_medical_conditions_heart_disease_'.$n]));
                if ($_POST['attendee_medical_conditions_hypertension_'.$n]) update_post_meta( $order_id, $n.' Attendee Hypertension', esc_attr($_POST['attendee_medical_conditions_hypertension_'.$n]));
                if ($_POST['attendee_medical_conditions_diabetes_'.$n]) update_post_meta( $order_id, $n.' Attendee Diabetes', esc_attr($_POST['attendee_medical_conditions_diabetes_'.$n]));
                if ($_POST['attendee_medical_conditions_vascular_disease_'.$n]) update_post_meta( $order_id, $n.' Attendee Vascular Disease', esc_attr($_POST['attendee_medical_conditions_vascular_disease_'.$n]));
                if ($_POST['attendee_medical_conditions_asthma_'.$n]) update_post_meta( $order_id, $n.' Attendee Asthma', esc_attr($_POST['attendee_medical_conditions_asthma_'.$n]));
                if ($_POST['attendee_medical_conditions_strenuous_exercise_'.$n]) update_post_meta( $order_id, $n.' Attendee Strenuous Exercise', esc_attr($_POST['attendee_medical_conditions_strenuous_exercise_'.$n]));
                if ($_POST['attendee_medical_conditions_physical_disease_'.$n]) update_post_meta( $order_id, $n.' Attendee Physical Disease', esc_attr($_POST['attendee_medical_conditions_physical_disease_'.$n]));
                if ($_POST['attendee_medical_conditions_drug_allergy_'.$n]) update_post_meta( $order_id, $n.' Attendee Drug Allergy', esc_attr($_POST['attendee_medical_conditions_drug_allergy_'.$n]));
                if ($_POST['attendee_medical_conditions_medication_'.$n]) update_post_meta( $order_id, $n.' Attendee Medication', esc_attr($_POST['attendee_medical_conditions_medication_'.$n]));
                if ($_POST['attendee_medical_conditions_major_surgery_'.$n]) update_post_meta( $order_id, $n.' Attendee Major Surgery', esc_attr($_POST['attendee_medical_conditions_major_surgery_'.$n]));
                if ($_POST['attendee_medical_conditions_fractures_'.$n]) update_post_meta( $order_id, $n.' Attendee Fractures', esc_attr($_POST['attendee_medical_conditions_fractures_'.$n]));
                if ($_POST['attendee_medical_conditions_none_'.$n]) update_post_meta( $order_id, $n.' Attendee Medical (None)', esc_attr($_POST['attendee_medical_conditions_none_'.$n]));
                if ($_POST['attendee_blood_type_'.$n]) update_post_meta( $order_id, $n.' Attendee Blood Type', esc_attr($_POST['attendee_blood_type_'.$n]));
                if ($_POST['attendee_medical_other_'.$n]) update_post_meta( $order_id, $n.' Attendee Medical Other', esc_attr($_POST['attendee_medical_other_'.$n]));
                if ($_POST['attendee_medical_drugs_'.$n]) update_post_meta( $order_id, $n.' Attendee Drugs', esc_attr($_POST['attendee_medical_drugs_'.$n]));
                if ($_POST['attendee_medical_surgery_'.$n]) update_post_meta( $order_id, $n.' Attendee Surgery', esc_attr($_POST['attendee_medical_surgery_'.$n]));
                if ($_POST['attendee_meal_preference_'.$n]) update_post_meta( $order_id, $n.' Attendee Meal Preference', esc_attr($_POST['attendee_meal_preference_'.$n]));
                if ($_POST['attendee_equipment_miasan_special_lightweight_backpack_'.$n]) update_post_meta( $order_id, $n.' Attendee Equipment Miasan Special Lightweight Backpack', esc_attr($_POST['attendee_equipment_miasan_special_lightweight_backpack_'.$n]));
                if ($_POST['attendee_equipment_bd_series_headlights_'.$n]) update_post_meta( $order_id, $n.' Attendee Equipment BD Series Headlights', esc_attr($_POST['attendee_equipment_bd_series_headlights_'.$n]));
                if ($_POST['attendee_equipment_alpenstock_'.$n]) update_post_meta( $order_id, $n.' Attendee Equipment Alpenstock', esc_attr($_POST['attendee_equipment_alpenstock_'.$n]));
                if ($_POST['attendee_equipment_sleeping_bags_'.$n]) update_post_meta( $order_id, $n.' Attendee Equipment Sleeping Bags', esc_attr($_POST['attendee_equipment_sleeping_bags_'.$n]));
                if ($_POST['attendee_equipment_foam_mattress_'.$n]) update_post_meta( $order_id, $n.' Attendee Equipment Foam Mattress', esc_attr($_POST['attendee_equipment_foam_mattress_'.$n]));
                if ($_POST['attendee_equipment_personal_helmet_'.$n]) update_post_meta( $order_id, $n.' Attendee Equipment Personal Helmet', esc_attr($_POST['attendee_equipment_personal_helmet_'.$n]));
                if ($_POST['attendee_equipment_climbing_suspenders_'.$n]) update_post_meta( $order_id, $n.' Attendee Equipment Climbing Suspenders', esc_attr($_POST['attendee_equipment_climbing_suspenders_'.$n]));
                if ($_POST['attendee_equipment_safety_shackles_'.$n]) update_post_meta( $order_id, $n.' Attendee Equipment Safety Shackles', esc_attr($_POST['attendee_equipment_safety_shackles_'.$n]));
                if ($_POST['attendee_equipment_none_'.$n]) update_post_meta( $order_id, $n.' Attendee Equipment None', esc_attr($_POST['attendee_equipment_none_'.$n]));
            }
        }
        
    }

    /**
     * Process checkout and validate
     **/
    function fields_process() {
        global $woocommerce;
        $attendee_count = $this->count_attendees();
        
        for($n = 1; $n <= $attendee_count; $n++ ) {

            //@TODO add all required fields
            if (!$_POST['attendee_email_'.$n] || !$_POST['attendee_chinese_name_'.$n])
                $error = true;
                break;
        }
        if($error) {
            $woocommerce->add_error( __('Please complete all attendee details.') );
        }

    }

    /**
     * Return number of attendees based on tickets
     **/        
    public function count_attendees() {
        
        global $woocommerce;
        $attendee_count = 0;
        
        if (sizeof($woocommerce->cart->get_cart())>0) :
            foreach ($woocommerce->cart->get_cart() as $item_id => $values) :
                $_product = $values['data'];
                if ($_product->exists() && $values['quantity']>0) :
                    $has_event = get_post_meta($_product->id, '_tribe_wooticket_for_event');
                    if (!empty($has_event))
                        $attendee_count += $values['quantity'];
                endif;
            endforeach;
        endif;
        
        return $attendee_count;
        
    }

    /**
     * Returns events and event vars based on cart contents
     **/   
    public function get_events() {
        
        global $woocommerce;
        $events = array();
        
        if (sizeof($woocommerce->cart->get_cart())>0) :
            foreach ($woocommerce->cart->get_cart() as $item_id => $values) :
                $_product = $values['data'];
                if ($_product->exists() && $values['quantity']>0) :
                    $event_id = get_post_meta($_product->id, '_tribe_wooticket_for_event', true);
                    if (!empty($event_id)) {
                        $events[$_product->id]['title'] = get_the_title($_product->id);
                        $events[$_product->id]['startdate'] = date_format(date_create(get_post_meta($event_id, '_EventStartDate', true)), 'm/d/y');
                        // $startdate = date_format($duedate, 'm/d/y');
                        // $events[$_product->id]['startdate'] = ;
                    }
                endif;
            endforeach;
        endif;
        
        return $events;
        
    }

    /**
     * Add contracts for each attendee
     **/        
    public function add_contracts() {

        //@TODO check if contract plugin is active
        check_ajax_referer( 'attendee-nonce', 'security' );

        $response = array();
        $attendee_count = $this->count_attendees();
        for($n = 1; $n <= $attendee_count; $n++ ) {
            //Make sure we have events
            $events = $this->get_events();
            if (!empty($events)) {
                foreach ($events as $event) {

                    // Make sure we have attendee info                
                    if ($_POST['attendee_chinese_name_'.$n] != '' && $_POST['attendee_english_name_'.$n] !== '' && $_POST['attendee_id_'.$n] !== '') {

                        // Determine if attendee is under 18
                        $dob = strtotime($_POST['attendee_dob_'.$n]);
                        $min = strtotime('+18 years', $dob);
                        if( time() < $min) {
                            $contracttype = 'miasan-minor';
                        }
                        else {
                            $contracttype = 'miasan';
                        }
                        // Get form fields for input
                        $clientname = sanitize_text_field($_POST['attendee_chinese_name_'.$n] .' ('. $_POST['attendee_english_name_'.$n] .')');
                        // $clientmailingaddress = sanitize_text_field($_POST['clientmailingaddress']);
                        $clientemailaddress = sanitize_text_field($_POST['attendee_email_'.$n]);
                        $clientid = sanitize_text_field($_POST['attendee_id_'.$n]);
                        $guardian = sanitize_text_field($_POST['attendee_guardian_'.$n]);
                        $projectname = $event['title'];
                        $paymentschedule = '1';
                        $contractamount = '1';
                        $hourlyrate = '1';
                        $signedby = 'John';
                        $signedbydate = date("m/d/Y");
                        $contracttype = $contracttype; 
                        $duedate = $event['startdate'];
                        $sendnotification = 'false';

                        // Check if post already exists
                        global $post;
                        $query = new WP_Query( array(
                            'author'=> get_current_user_id(), 
                            'post_type'=>'contract', 
                            'posts_per_page'=>100,
                            'meta_query' => array(
                                array(
                                    'key'     => 'contractdetails_projectname',
                                    'value'   => $projectname,
                                    'compare' => '=',
                                ),
                                array(
                                    'key'     => 'contractdetails_attendee',
                                    'value'   => $n,
                                    'compare' => '=',
                                ),
                                array(
                                    'key'     => 'contractdetails_duedate',
                                    'value'   => $duedate,
                                    'compare' => '=',
                                ),
                            ),
                            ));

                        // Update old post
                        if ( $query->have_posts() ) {
                            while ( $query->have_posts() ) { 
                                $query->the_post();
                                $post_id = wp_update_post ( array(
                                    'ID'            => $post->ID,
                                    'post_content' => '[contract clientname="'.$clientname.'" clientid="'.$clientid.'" projectname="'.$projectname.'" duedate="'.$duedate.'" contractamount="'.$contractamount.'" paymentschedule="'.$paymentschedule.'" contracttype="'.$contracttype.'" hourlyrate="'.$hourlyrate.'" signedby="'.$signedby.'" signedbydate="'.$signedbydate.'" paypalamount="" sendnotification="'.$sendnotification.'" clientemailaddress="'.$clientemailaddress.'" guardian="'.$guardian.'" ]',
                                ));

                            }     
                        }
                        else {
                        // Create new post if not already created
                        $post_id = wp_insert_post( array(
                            'post_author'   => get_current_user_id(),
                            'post_title'    => $projectname . ' '.$duedate. ' User #'. get_current_user_id() .' Attendee #'. $n,
                            // 'post_content' => '[contract clientname="'.$clientname.'" clientmailingaddress="'.$clientmailingaddress.'" projectname="'.$projectname.'" duedate="'.$duedate.'" contractamount="" paymentschedule="'.$paymentschedule.'" contracttype="null" hourlyrate="" signedby="" signedbydate="" paypalamount="" sendnotification="false" clientemailaddress="'.$clientemailaddress.'" ]',
                            'post_content' => '[contract clientname="'.$clientname.'" clientid="'.$clientid.'" projectname="'.$projectname.'" duedate="'.$duedate.'" contractamount="'.$contractamount.'" paymentschedule="'.$paymentschedule.'" contracttype="'.$contracttype.'" hourlyrate="'.$hourlyrate.'" signedby="'.$signedby.'" signedbydate="'.$signedbydate.'" paypalamount="" sendnotification="'.$sendnotification.'" clientemailaddress="'.$clientemailaddress.'" guardian="'.$guardian.'"]',
                            'post_type'     => 'contract',
                            'post_status'   => 'publish'
                            ));  
                        }

                        update_post_meta ($post_id, 'contractdetails_attendee', $n);
                        update_post_meta ($post_id, 'contractdetails_duedate', $duedate);
                        update_post_meta ($post_id, 'contractdetails_clientname', $clientname);
                        // update_post_meta ($post_id, 'contractdetails_mailingaddress', $clientmailingaddress);
                        update_post_meta ($post_id, 'contractdetails_emailaddress', $clientemailaddress);
                        update_post_meta ($post_id, 'contractdetails_projectname', $projectname);
                        update_post_meta ($post_id, 'contractdetails_paymentschedule', $paymentschedule);
                    }
                }
            } 
            else{
                $response['error'] = 'No tickets were found in your cart.'; 
            }
        }

        echo json_encode($response);
        die();
        
    }


    /**
     * Get contracts for current user based on product
     **/        
    public function get_contracts() {

        //@TODO check if contract plugin is active

        check_ajax_referer( 'attendee-nonce', 'security' );

        $response = '';
        $events = $this->get_events();

        if (!empty($events)) {
            foreach ($events as $event) {

                // Get project details to verify
                $projectname = $event['title'];
                $paymentschedule = '1';
                $duedate = $event['startdate'];
                $signatures = array();

                // Loop through contracts for event
                global $post;
                $query = new WP_Query( array(
                    'author'=> get_current_user_id(), 
                    'post_type'=>'contract', 
                    'posts_per_page'=>$this->count_attendees(),
                    'orderby'=>'title',
                    'order'=>'asc',
                    'meta_query' => array(
                        array(
                            'key'     => 'contractdetails_projectname',
                            'value'   => $projectname,
                            'compare' => '=',
                        ),
                        array(
                            'key'     => 'contractdetails_duedate',
                            'value'   => $duedate,
                            'compare' => '=',
                        ),
                    ),
                    ));
                if ( $query->have_posts() ) {
                    echo '<h6>'.__('Please open and sign all forms before continuing', $this->wootickets_attendees).'</h6>';
                    echo '<table>'; 
                    while ( $query->have_posts() ) { 
                        $query->the_post();
                        echo '<tr class="form-row validate-required"><td><a href="'.get_permalink().'" target="_blank">'.get_the_title();
                        $signed = 0;
                        $upload_dir = wp_upload_dir();
                        if ($handle = opendir($upload_dir['basedir'] . '/onlinecontract/signatures/')) {
                            while (false !== ($entry = readdir($handle))) {
                                if ($entry != "." && $entry != "..") {
                                    $imgnamearr = explode('_',$entry);
                                    if ($imgnamearr[0]==$post->ID) {
                                        $fullimgname = $entry;
                                        echo '<input type="hidden" class="input-text " name="attendee_contract_'.$post->ID.'" id="attendee_contract_'.$post->ID.'" placeholder="" value="1">'
                                        .'<span class="signed-chip signed">'.__('Signed',$this->wootickets_attendees).'</span></a></td>';
                                        $signed = 1;
                                        $signatures[] = 1;
                                        break;
                                    }
                                }
                                
                            }
                            if ($signed!=1) {
                                $signatures[] = 0;
                                echo '<input type="hidden" class="input-text " name="attendee_contract_'.$post->ID.'" id="attendee_contract_'.$post->ID.'" placeholder="" >'
                                .'<span class="signed-chip unsigned">'.__('Unsigned',$this->wootickets_attendees).'</span></td>';
                            }
                            closedir($handle);
                        }
                        echo '</tr>';
                    }
                    echo '</table>';        
                }
                else {
                    echo 'No contracts found.  Please go back and ensure you\'ve filled in on attendee fields or contact an administrator for support.';
                }
            }
        }
        else {
            echo 'No events found.';
        }

        echo '<div class="contracts-refresh"><a href="#" class="get-contracts"><span class="contracts-confirm">確認</a></a></div>';

        echo '<div class="contracts-guide"><a href="'.plugin_dir_url( __FILE__ ) . 'images/form-guide.jpg" target="_blank">如何簽署線上同意書？</a></div>';

        die();
        
    }

}
