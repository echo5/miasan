(function( $ ) {
	'use strict';

    /**
     * Spinning Loader
     **/
     $.fn.showLoader = function(){ 
     	$(this).html('<div id="bowlG"><div id="bowl_ringG"><div class="ball_holderG"><div class="ballG"></div></div></div></div>')
     }
     $.fn.hideLoader = function(){ 
     	$(this).find('#bowlG').remove();
     }

    /**
     * AJAX add contracts
     **/

	jQuery('.add-contracts').click(addContracts);

	function addContracts(callback){

		var attendeesForm = jQuery('.checkout').serialize();
		var data = attendeesForm + '&action=add_contracts&security=' + attendeeAjax.security;

		$.post(attendeeAjax.ajaxurl, data, function(response) {
			callback && callback();
		});

		return false;
	}


    /**
     * AJAX get contracts
     **/

	jQuery('.get-contracts').live('click', getContracts);

	function getContracts(callback){

		var data = '&action=get_contracts&security=' + attendeeAjax.security;
		var target = $('#attendee_contracts');

		$.ajax({
		  type: 'POST',
		  url: attendeeAjax.ajaxurl,
		  data: data,
		  success: function(response) {
				//console.log( response );
				target.showLoader();
				target.html(response);
				target.hideLoader();
				callback && callback();
			},
		  async:false
		});
		
		return false;
	}

	/**
	 * Datepicker
	 **/
    jQuery(function($) {
        jQuery( ".datepicker input" ).datepicker({
        	onSelect: function(value, ui) {
        	    var today = new Date().getTime(), 
        	        dob = new Date(value).getTime(),
        	        age = today - dob,
        	        yoa = Math.floor(age / 1000 / 60 / 60 / 24 / 365.25); // age / ms / sec / min / hour / days in a year
        	    if(yoa < 18){
        	        $(this).parent().nextAll('.form-row-guardian').first().show();
        	    }else{
        	        $(this).parent().nextAll('.form-row-guardian').first().hide();
        	    }
        	},
            changeMonth: true,
            changeYear: true,
            yearRange: "-70:-13", 
            defaultDate: '-18yr',
        });

        jQuery(".next-attendee").click(function(e) {
            e.preventDefault();
            var nextToggle = $(this).parent().next();
            $(this).parent().prev().click();  
            if(!nextToggle.hasClass("wpb_toggle_title_active")) {
                nextToggle.click();
            }  
            setTimeout(
              function() 
              {
                $("html, body").animate({
                    scrollTop: nextToggle.offset().top - 100
                }, 1000);
              }, 500);

        });
    });



    if (fesiCheckoutSteps.isAuthorizedUser == false && jQuery.inArray('login', fesiCheckoutSteps.disableSteps) < 0) {
       var nextButtonTitle = fesiCheckoutSteps.noAccountButton
    } else{
       var nextButtonTitle = fesiCheckoutSteps.nextButton
    }
    var nextButtonTitle

    var form = jQuery("#festi-checkout-steps-wizard")
    form.steps(
        {
            transitionEffectSpeed: 500,
            startIndex: 0,
            autoFocus: true,
            enableAllSteps: false,
            transitionEffect: 'fade',
            titleTemplate: fesiCheckoutSteps.titleTemplate,
            cssClass: "festi-wizard",
            labels: {
                cancel: "Cancel",
                pagination: "Pagination",
                finish: fesiCheckoutSteps.finishButton,
                next: nextButtonTitle,
                previous: fesiCheckoutSteps.previousButton,
                loading: "Loading ..."
            },
            onStepChanging: function (event, currentIndex, newIndex)
            {
            	var errors = false;
            	var validated;
                // Allways allow previous action even if the current form is not valid!
                if (currentIndex > newIndex)
                {
                    return true;
                }
                // Pass Shipping if shipped to same adddress
                if (currentIndex == 1) {
            		if (!$('#ship-to-different-address-checkbox').is(":checked"))
            		{
            		  return true;
            		}
                }
                // Needed in some cases if the user went back (clean up)
                if (currentIndex < newIndex)
                {
                    // To remove error styles
                    form.find(".body:eq(" + newIndex + ") label.error").remove();
                    form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
                }
                // Get and verify signatures
                if (currentIndex == 3) {
                	 getContracts(function() {
                		$('#festi-checkout-steps-wizard-p-' + currentIndex + ' .input-text').each(function() {
                			validated = $(this).validate();
                			if(validated == false) {
                				errors = true;
                			}
                		});
                	});
                }

                // Validate input and select fields but skip AJAX consent form step
                if (currentIndex != 3) {
	                $('#festi-checkout-steps-wizard-p-' + currentIndex + ' .input-text, #festi-checkout-steps-wizard-p-' + currentIndex + ' .input-text select').each(function() {
	                	validated = $(this).validate();
	                	if(validated == false) {
	                		errors = true;
	                	}
	                });
            	}

                // If errors were present, don't go to next step
                console.log(errors);
                if(errors) {
                	return false;
                }
                return true;

            },
            onFinished: function (event, currentIndex) {
                jQuery("#place_order").click();
            },
            onStepChanged: function (event, currentIndex, priorIndex)
            {
            	// Scroll to top & animate div content height
            	$("html, body").animate({
            	    scrollTop: $('#primary').offset().top - 100
            	}, 1000);

                if (currentIndex == 0 && fesiCheckoutSteps.isAuthorizedUser == false && jQuery.inArray('login', fesiCheckoutSteps.disableSteps) < 0) {
                    jQuery('#festi-checkout-steps-wizard a[href="#next"]').html(fesiCheckoutSteps.noAccountButton);
                    jQuery('#festi-checkout-steps-wizard a[href="#previous"]').hide();
                }
                else if (currentIndex == 3 ) {
                	addContracts(getContracts);
                }
                else {
                    jQuery('#festi-checkout-steps-wizard a[href="#next"]').html(fesiCheckoutSteps.nextButton);
                    jQuery('#festi-checkout-steps-wizard a[href="#previous"]').show();
                }
            },
        }
    );
    
    var height = jQuery('#festi-checkout-steps-wizard-p-0').css('position', 'relative');
    jQuery('#festi-checkout-steps-wizard.festi-wizard .content').css('height', height + 'px');
    
    jQuery("#festi-checkout-steps-wizard").show();
    jQuery('form[name="checkout"]').css('visibility', 'visible');


    // Woocommerce validation classes
	$.fn.validate = function(){

		var $this = $( this ),
			$parent = $this.closest( '.form-row' ),
			validated = true;


		if ( $parent.is( '.validate-required' ) ) {
			if ( $this.val() === '' ) {
				$parent.removeClass( 'woocommerce-validated' ).addClass( 'woocommerce-invalid woocommerce-invalid-required-field' );
				validated = false;
			}
		}

		if ( $parent.hasClass( '.validate-email' ) ) {
			if ( $this.val() ) {

				/* http://stackoverflow.com/questions/2855865/jquery-validate-e-mail-address-regex */
				var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

				if ( ! pattern.test( $this.val()  ) ) {
					$parent.removeClass( 'woocommerce-validated' ).addClass( 'woocommerce-invalid woocommerce-invalid-email' );
					validated = false;
				}
			}
		}

		// Make valid if account new account created
		// if ( $this.id == 'account_password') {
			
		// }

		if ( validated ) {
			$parent.removeClass( 'woocommerce-invalid woocommerce-invalid-required-field' ).addClass( 'woocommerce-validated' );
		}

		return validated;
	} 


    // User Login
    function appendErrorRequiredClasses(selector)
    {
        jQuery('form.login input#' + selector).parent().removeClass("woocommerce-validated");
        jQuery('form.login input#' + selector).parent().addClass("woocommerce-invalid woocommerce-invalid-required-field");
        jQuery('form.login input#' + selector).parent().addClass("validate-required");
    }
    
    jQuery('form.login').submit(function() 
    {
        var form = 'form.login';
        var error = false;
        
        if (jQuery(form + ' input#username').val() == false) {
            error = true;
            appendErrorRequiredClasses('username');
        }
        
        if (jQuery(form + ' input#password').val() == false) {
           error = true;
           appendErrorRequiredClasses('password');
        }
        
        if (error != false)
        {
            return false;
        }
        

        if (jQuery(form + ' input#rememberme').is(':checked') == false) {
            rememberme = false;
        } else {
            rememberme = true; 
        }

        var data = {
            action: 'login_user_wizard_step',
            username: jQuery(form + ' input#username').val(),
            password: jQuery(form + ' input#password').val(),
            rememberme: rememberme
        };
        
         jQuery.post(fesiCheckoutSteps.ajaxurl, data, function(response) {
            if (response == 'successfully') {
                location.reload();
            } else {
                jQuery('div.festi-wizard-login-error').remove();
                jQuery('form.login').prepend(response);
            }
        })
        return false;
    });

    jQuery('form.login').submit(function() 
    {
        var form = 'form.login';
        var error = false;
        
        if (jQuery(form + ' input#username').val() == false) {
            error = true;
            appendErrorRequiredClasses('username');
        }
        
        if (jQuery(form + ' input#password').val() == false) {
           error = true;
           appendErrorRequiredClasses('password');
        }
        
        if (error != false)
        {
            return false;
        }
        

        if (jQuery(form + ' input#rememberme').is(':checked') == false) {
            rememberme = false;
        } else {
            rememberme = true; 
        }

        var data = {
            action: 'login_user_wizard_step',
            username: jQuery(form + ' input#username').val(),
            password: jQuery(form + ' input#password').val(),
            rememberme: rememberme
        };
        
         jQuery.post(fesiCheckoutSteps.ajaxurl, data, function(response) {
            if (response == 'successfully') {
                location.reload();
            } else {
                jQuery('div.festi-wizard-login-error').remove();
                jQuery('form.login').prepend(response);
            }
        })
        return false;
    });

})( jQuery );
