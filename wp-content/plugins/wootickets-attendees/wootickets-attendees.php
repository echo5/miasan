<?php

/**
 *
 * @link              http://echo5webdesign.com
 * @since             1.0.0
 * @package           Wootickets_Attendees
 *
 * @wordpress-plugin
 * Plugin Name:       Wootickets Attendees
 * Plugin URI:        http://echo5webdesign.com
 * Description:       Inserts appropriate number of fields for event attendee names and emails in Woocommerce checkout page and saves it as meta in order details. WooCommerce & WooTickets plugin are required.
 * Version:           1.0.0
 * Author:            Joshua Flowers
 * Author URI:        http://echo5webdesign.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wootickets-attendees
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wootickets-attendees-activator.php
 */
function activate_wootickets_attendees() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wootickets-attendees-activator.php';
	Wootickets_Attendees_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wootickets-attendees-deactivator.php
 */
function deactivate_wootickets_attendees() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wootickets-attendees-deactivator.php';
	Wootickets_Attendees_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wootickets_attendees' );
register_deactivation_hook( __FILE__, 'deactivate_wootickets_attendees' );

/**
 * The core plugin class that is used to define internationalization,
 * dashboard-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wootickets-attendees.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wootickets_attendees() {

	$plugin = new Wootickets_Attendees();
	$plugin->run();

}
run_wootickets_attendees();
